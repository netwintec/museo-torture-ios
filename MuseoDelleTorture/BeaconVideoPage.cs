using AVFoundation;
using CoreGraphics;
using Foundation;
using MediaPlayer;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MuseoDelleTorture
{
	partial class BeaconVideoPage : UIViewController
	{
        BeaconInfo oggetto;

        MPMoviePlayerController player;

        bool isPlay = false;

        public BeaconVideoPage (IntPtr handle) : base (handle)
		{
		}

        public override void ViewDidLoad()
        {

            base.ViewDidLoad();

            oggetto = BeaconStanzaHome.Instance.ObjectSelected;

            int yy = 0;

            var size = UIStringDrawing.StringSize(oggetto.titolo, UIFont.FromName("DaunPenh", 25), new CGSize(ContentView.Frame.Width, 2000));

            UILabel TitoloLabel = new UILabel(new CGRect(0, 12, ContentView.Frame.Width, size.Height));
            TitoloLabel.Font = UIFont.FromName("DaunPenh", 25);
            TitoloLabel.TextAlignment = UITextAlignment.Center;
            TitoloLabel.TextColor = UIColor.White;
            TitoloLabel.Text = oggetto.titolo;
            TitoloLabel.Lines = 0;

            yy += (int)(size.Height + 15);

            float VideoHeight = (float)(((ContentView.Frame.Height - (size.Height + 45)) / 2) - 30);
            float ScrollHeight = (float)(((ContentView.Frame.Height - (size.Height + 45)) / 2) + 30);

            player = new MPMoviePlayerController(new NSUrl(oggetto.videoUrl))
            {
                AllowsAirPlay = true,
                Fullscreen = true,
                ScalingMode = MPMovieScalingMode.AspectFit,
                RepeatMode = MPMovieRepeatMode.One
            };
            player.ControlStyle = MPMovieControlStyle.None;
            player.View.Frame = new CGRect(0,yy+10,ContentView.Frame.Width,VideoHeight);
            player.PrepareToPlay();
            player.ShouldAutoplay = false;

            NSNotificationCenter.DefaultCenter.AddObserver(
                new NSString("MPMoviePlayerWillEnterFullscreenNotification"),
                (notify) => {
                    Console.WriteLine("Will enter full screen");
                    player.Play();
                    isPlay = true;
                    player.ControlStyle = MPMovieControlStyle.Fullscreen;
                    
                }
            );
            NSNotificationCenter.DefaultCenter.AddObserver(
                new NSString("MPMoviePlayerWillExitFullscreenNotification"),
                (notify) => {
                    player.ControlStyle = MPMovieControlStyle.None;
                    player.Pause();
                    isPlay = false;
                }
            );

            UIView TrasparentClickView = new UIView(new CGRect(0, yy + 10, ContentView.Frame.Width, VideoHeight));
            TrasparentClickView.BackgroundColor = UIColor.Clear;

            UITapGestureRecognizer VideoTap = new UITapGestureRecognizer((s) =>
            {
                if (!isPlay)
                {
                    if (isHeadsetPluggedIn())
                    {
                        player.SetFullscreen(true, true);
                        //player.ControlStyle = MPMovieControlStyle.Embedded;
                    }
                    else
                    {
                        var alert = new UIAlertView("Errore", "Attaccare le cuffie per usufruire dell'audioguida", null, "Ok");
                        alert.Show();
                    }
                }
            });
            TrasparentClickView.UserInteractionEnabled = true;
            TrasparentClickView.AddGestureRecognizer(VideoTap);

            //player.Play();

            yy += (int)(VideoHeight + 10);

            UIView SeparatorView = new UIView(new CGRect(0, yy, View.Frame.Width, 4));
            SeparatorView.BackgroundColor = UIColor.FromRGB(153, 19, 27);

            yy += 4;


            UIScrollView TextScroll = new UIScrollView(new CGRect(30, yy + 11, ContentView.Frame.Width - 60, ScrollHeight));

            string a = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            NSAttributedStringDocumentAttributes opz = new NSAttributedStringDocumentAttributes();
            NSError err = new NSError();
            opz.DocumentType = NSDocumentType.HTML;
            opz.StringEncoding = NSStringEncoding.UTF8;

            NSAttributedString text = new NSAttributedString("<span style='color:white;font-size:15px'>" + oggetto.descrizione + "</span>", opz, ref err);

            size = UIStringDrawing.StringSize(oggetto.descrizione, UIFont.FromName("MyriadPro-Regular", 17), new CGSize(ContentView.Frame.Width - 60, 2000));

            UILabel Text = new UILabel(new CGRect(0, 0, ContentView.Frame.Width - 60, size.Height));
            Text.Font = UIFont.FromName("MyriadPro-Regular", 17);
            Text.TextAlignment = UITextAlignment.Justified;
            Text.TextColor = UIColor.White;
            Text.AttributedText = text;
            Text.Lines = 0;

            TextScroll.ContentSize = new CGSize(ContentView.Frame.Width - 60, size.Height);

            TextScroll.Add(Text);

            ContentView.Add(TitoloLabel);
            ContentView.Add(player.View);
            ContentView.Add(TrasparentClickView);
            ContentView.Add(SeparatorView);
            ContentView.Add(TextScroll);

        }

        public bool isHeadsetPluggedIn()
        {
            AVAudioSessionRouteDescription route = AVAudioSession.SharedInstance().CurrentRoute;
            foreach (AVAudioSessionPortDescription descr in route.Outputs)
            {
                Console.WriteLine(descr.PortType);
                if (descr.PortType.IsEqual(new NSString("Headphones")))
                    return true;
            }
            return true;
        }

        public override void ViewWillDisappear(bool animated)
        {

            //player.Dispose();

            base.ViewWillDisappear(animated);
        }
    }
}
