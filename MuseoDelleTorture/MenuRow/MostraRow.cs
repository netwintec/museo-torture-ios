using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MuseoDelleTorture
{
	partial class MostraRow : UITableViewCell
	{
		public MostraRow (IntPtr handle) : base (handle)
		{
		}
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            UILabel Label = new UILabel(new CGRect(75, 20, Frame.Width - 85, 26));
            Label.Text = NSBundle.MainBundle.LocalizedString("Menu_Mostra", "THE EXHIBITION", null);
            Label.TextColor = UIColor.White;
            //Label.Font = UIFont.FromName("BubblegumSans-Regular", 20);
            //Label.TextAlignment = UITextAlignment.Center;
            Label.Font = UIFont.FromName("DaunPenh", 20);
            Label.TextAlignment = UITextAlignment.Left;

            //ViewForBaselineLayout.
            Add(Label);

        }
    }
}
