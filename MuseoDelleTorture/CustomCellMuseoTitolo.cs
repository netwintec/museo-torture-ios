﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace MuseoDelleTorture
{
    class CustomCellMuseoTitolo : UITableViewCell
    {
        UILabel titolo;
        UIView separator;
        UIImageView freccia,marker;

        float HeightCell;
        public CustomCellMuseoTitolo(NSString cellId, bool selected, string tit, float h) : base(UITableViewCellStyle.Default, cellId)
        {
            HeightCell = h;

            SelectionStyle = UITableViewCellSelectionStyle.None;
            freccia = new UIImageView();
            marker = new UIImageView();
            separator = new UIView();
            separator.BackgroundColor = UIColor.White;
            titolo = new UILabel()
            {
                Font = UIFont.FromName("DaunPenh", 20f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                //Lines = 2,
                BackgroundColor = UIColor.Clear
            };

            //Console.WriteLine("/***** "+(ContentView.Frame.Width - 95)+"!"+tit+" *****\\");
            if (HeightCell > 45)
            {
                titolo.Lines = 2;
            }
            else
            {
                titolo.Lines = 1;
            }

           
            if (selected)
            {
                titolo.TextColor = UIColor.FromRGB(152, 20, 27);
            }
            else
            {
                titolo.TextColor = UIColor.White;
            }

            ContentView.BackgroundColor = UIColor.Black;

            ContentView.AddSubviews(new UIView[] { marker, titolo, freccia, separator });

        }
        public void UpdateCell(string tit, UIImage imm,UIImage mark)
        {
            titolo.Text = tit;
            freccia.Image = imm;
            marker.Image = mark;
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            if (HeightCell > 45)
            {
                marker.Frame = new CGRect(2, 15, 20, 28);
                titolo.Frame = new CGRect(25, 7, ContentView.Frame.Width - 50, 60);
                freccia.Frame = new CGRect(ContentView.Frame.Width - 21, 23, 20, 13);
                separator.Frame = new CGRect(0, 68, ContentView.Frame.Width, 1);
            }
            else
            {
                marker.Frame = new CGRect(1, 7, 14, 25);
                titolo.Frame = new CGRect(20, 7, ContentView.Frame.Width - 45, 30);
                freccia.Frame = new CGRect(ContentView.Frame.Width - 21, 13, 20, 13);
                separator.Frame = new CGRect(0, 38, ContentView.Frame.Width, 1);
            }

        }
    }

}
