﻿
using System;
using System.Collections.Generic;

namespace MuseoDelleTorture
{		
	public class MarkerInfo
	{
		public string nome,nomeE,via,titolo,descrizione,descrizioneC;
		public float lat,lon;
		public List<string> imgurl = new List<String>();
		public bool isHeader;
	}
}

