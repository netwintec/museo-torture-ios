﻿using System;
namespace MuseoDelleTorture
{
    public class Utility
    {
        public static DateTime FromUnixTime(long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        public static long FromDateTime(DateTime time)
        {

            DateTime local = new DateTime(time.Year, time.Month, time.Day,
                                          time.Hour, time.Minute, time.Second,
                                          DateTimeKind.Local);
            DateTime utc = local.ToUniversalTime();

            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            long sec = (long)(utc - epoch).TotalSeconds;

            return sec;

        }
    }
}
