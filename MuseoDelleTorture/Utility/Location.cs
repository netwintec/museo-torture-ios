﻿using System;

namespace MuseoDelleTorture
{
	public class Location
	{
		private double lt;
		private double lg;

		public double Lg
		{
			get { return lg; }
			set { lg = value; }
		}

		public double Lt
		{
			get { return lt; }
			set { lt = value; }
		}

		public Location(double lt, double lg)
		{
			this.lt = lt;
			this.lg = lg;
		}
	}
}

