﻿using CoreBluetooth;
using Foundation;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace MuseoDelleTorture
{
    class MyCentralManagerDelegate : CBCentralManagerDelegate
    {
        const string uuid = "ACFD065E-C3C0-11E3-9BBE-1A514932AC01";
        bool scanned = false;
        int porta = 82;

        public override void UpdatedState(CBCentralManager central)
        {
            var state = central.State;
            Console.WriteLine("aa +"+ state);
            //central.DiscoveredPeripheral += 
            if (state == CBCentralManagerState.PoweredOn && !scanned)
            {
                Console.WriteLine("aabb");
                var spp = new CBUUID[] {
                    //CBUUID.FromString(uuid)
                    };
                var dict = new NSDictionary();
                //central.RetrievePeripherals(spp);
                central.ScanForPeripherals(spp);
                scanned = true;

                Console.WriteLine("Scaning = "+central.IsScanning);
            }
            if (state == CBCentralManagerState.PoweredOff)
            {
                //AppDelegate.Instance.BluetoothOff = true;
            }
        }

        public override void DiscoveredPeripheral(CBCentralManager central, CBPeripheral peripheral, NSDictionary advertisementData, NSNumber RSSI)
        {

            Console.WriteLine("DiscoveredPeripheral");

            try {

                NSDictionary service = (NSDictionary)advertisementData.ObjectForKey(CBAdvertisement.DataServiceDataKey);
                //service. = e.AdvertisementData.ObjectForKey(CBAdvertisement.DataServiceDataKey);
                Console.WriteLine("Name:"+ peripheral.Name);
                if (peripheral.Name.Substring(0, 6) == "BlueUp")
                {
                    
                    if (service != null)
                    {
                        Console.WriteLine("2");
                        NSData data1 = (NSData)service.ObjectForKey(CBUUID.FromString("ACBE"));

                        Console.WriteLine("data1:"+data1);

                        IntPtr rawBeaconData = data1.Bytes;
                        byte[] rawByte = new byte[data1.Length];
                        System.Runtime.InteropServices.Marshal.Copy(rawBeaconData, rawByte, (int)0, (int)data1.Length);
                        if (rawByte != null && rawByte.Length == 27)
                        {

                            //NSUuid proximityUUID = new NSUuid(rawByte[4].ToString());
                            //proximityUUID.
                            /*NSString a = new NSString((rawByte[4] << 120 | rawByte[5] << 112
                                | rawByte[6] << 104 | rawByte[7] << 96
                                | rawByte[8] << 88 | rawByte[9] << 80
                                | rawByte[10] << 72 | rawByte[11] << 64
                                | rawByte[12] << 56 | rawByte[13] << 48
                                | rawByte[14] << 40 | rawByte[15] << 32
                                | rawByte[16] << 24 | rawByte[17] << 16
                                | rawByte[18] << 8 | rawByte[19]).ToString());*/
                            //byte[] app = new byte[16];                       
                            //System.Runtime.InteropServices.Marshal.Copy(rawBeaconData, app, 4, 16);
                            //NSData a = NSData.FromArray(app);
                            //NSUuid proximityUUID = new NSUuid(app);

                            string aa = BitConverter.ToString(rawByte, 4, 16);
                            string b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12;
                            b1 = aa.Substring(0, 2);
                            b2 = aa.Substring(3, 2);
                            b3 = aa.Substring(6, 2);
                            b4 = aa.Substring(9, 5);
                            b5 = aa.Substring(15, 5);
                            b6 = aa.Substring(21, 5);
                            b7 = aa.Substring(27, 5);
                            b8 = aa.Substring(33, 2);
                            b9 = aa.Substring(36, 2);
                            b10 = aa.Substring(39, 2);
                            b11 = aa.Substring(42, 2);
                            b12 = aa.Substring(45, 2);
                            string bb = b1 + b2 + b3 + b4 + b5 + b6 + b7 + b8 + b9 + b10 + b11 + b12;
                            NSUuid proximityUUID = new NSUuid(bb);


                            NSNumber majorNumber = new NSNumber((rawByte[20] << 8) | rawByte[21]);
                            NSNumber minorNumber = new NSNumber((rawByte[22] << 8) | rawByte[23]);
                            NSNumber batteryLevel = new NSNumber(rawByte[26]);
                            Console.WriteLine("beacon data:" + proximityUUID.AsString() + "|" + majorNumber + "|" + minorNumber + "|" + batteryLevel.ToString());

                            var client = new RestClient("http://api.netwintec.com:" + porta + "/");
                            //client.Authenticator = new HttpBasicAuthenticator(username, password);

                            //var requestN4U = new RestRequest("login/facebook?access_token=" + TokenFB, Method.GET);
                            var requestN4U = new RestRequest("battery", Method.POST);
                            requestN4U.AddHeader("content-type", "application/json");
                            requestN4U.AddHeader("Net4U-Company", "museotortura");

                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("uuid", proximityUUID.AsString());
                            oJsonObject.Add("major", int.Parse(majorNumber.ToString()));
                            oJsonObject.Add("minor", int.Parse(minorNumber.ToString()));
                            oJsonObject.Add("battery", int.Parse(batteryLevel.ToString()));

                            Console.WriteLine("JSON:" + oJsonObject.ToString());

                            requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

                            //requestN4U.Timeout = 120000;
                            client.ExecuteAsync(requestN4U, (s, e) => {

                                Console.WriteLine("battery"+s.StatusCode + "|" + s.Content);

                            });

                            /*IRestResponse aaa = client.Execute(requestN4U);
                            Console.WriteLine("battery:" + aaa.StatusCode + "|" + aaa.Content);*/
                        }
                    }
                }
            }catch(Exception e)
            {
                Console.WriteLine("battery:" + e.ToString());
            }
        }


        public override void RetrievedPeripherals(CBCentralManager central, CBPeripheral[] peripherals)
        {
            Console.WriteLine("RetrievedPeripherals");
            base.RetrievedPeripherals(central, peripherals);
        }

        public override void RetrievedConnectedPeripherals(CBCentralManager central, CBPeripheral[] peripherals)
        {
            Console.WriteLine("RetrievedConnectedPeripherals");
            base.RetrievedConnectedPeripherals(central, peripherals);
        }
    }
}
