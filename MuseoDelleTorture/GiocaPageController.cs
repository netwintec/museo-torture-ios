using CoreGraphics;
using Facebook.CoreKit;
using Facebook.LoginKit;
using Facebook.ShareKit;
using Foundation;
using Newtonsoft.Json.Linq;
using RestSharp;
using Xamarin.SWRevealViewController;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Json;
using System.Net;
using System.Threading;
using UIKit;

namespace MuseoDelleTorture
{
	partial class GiocaPageController : UIViewController
	{

        int yyDomande;
        int yyContainer;
        int yyResult;

        CGSize sizeDom, sizeRisp1, sizeRisp2, sizeRisp3,sizeYouAre,sizeNome,sizeDescr;

        GiocaResult ListResult;
        public List<GiocaElement> ListGioca = new List<GiocaElement>();

        int indiceDomanda = 0;

        bool somethingSel = false;

        public float WidhtViewDom;

        UILabel NumDomanda, Domanda, Risposta1, Risposta2, Risposta3;

        UIView BorderView, ContainerView;

        UIButton BottoneConfirm;

        UIScrollView scrollViewDomande, scrollViewRisultato;

        UIImageView RispCheck1, RispCheck2, RispCheck3;

        UIImage Checked = UIImage.FromFile("check.png");
        UIImage NoChecked = UIImage.FromFile("no_check.png");

        List<string> readPermissions = new List<string> { "public_profile" };
        ShareDialog dialog;

        bool DoLogIn = false;

        public GiocaPageController (IntPtr handle) : base (handle)
		{
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += (sender, e) => this.RevealViewController().RevealToggleAnimated(true);
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
            }
            UIViewController GiocaHomePage = storyboard.InstantiateViewController("GiocaHome");

            BackItem.Clicked += delegate
            {
                this.RevealViewController().PushFrontViewController(GiocaHomePage, true);
            };

            //*** PRENDO DATI DEL GIOCA DEL MUSEO ***//
            ListGioca = GiocaHomeController.Instance.ListGioca[GiocaHomeController.Instance.selected];
            ListResult = GiocaHomeController.Instance.ListResult[GiocaHomeController.Instance.selected];

            dialog = new MyShareDialog();

            if (ListResult.Risultato != "")
            {
                DoLogIn = false;

                scrollViewRisultato = new UIScrollView(new CGRect(0, 0, View.Frame.Width, ContentViewResult.Frame.Height));
                yyResult = 0;

                UILabel TitResult  = new UILabel(new CGRect(0, yyResult + 15, View.Frame.Width, 25));
                TitResult.Font = UIFont.FromName("DaunPenh", 25);
                TitResult.TextAlignment = UITextAlignment.Center;
                TitResult.TextColor = UIColor.White;
                TitResult.Text = NSBundle.MainBundle.LocalizedString("Gioca_Risultato", "RESULT", null);

                yyResult += 40;

                UIImageView image = new UIImageView(new CGRect(View.Frame.Width/2 - 87.5,yyResult+20,175,175));
                image.Image = UIImage.FromFile("placeholder.png");

                yyResult += 195;

                sizeYouAre = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Gioca_TuSei", "You are", null), UIFont.FromName("MyriadPro-Regular", 16), new CGSize(View.Frame.Width/2, 2000));
                Console.WriteLine("YouAre:" + sizeYouAre.Height + "  " + sizeYouAre.Width);

                sizeNome = UIStringDrawing.StringSize(ListResult.Risultato, UIFont.FromName("MyriadPro-Bold", 18), new CGSize(View.Frame.Width - sizeYouAre.Width -5, 2000));
                Console.WriteLine("Nome:" + sizeNome.Height + "  " + sizeNome.Width);

                float widhtNome = (float)(sizeNome.Width + sizeYouAre.Width + 5);

                UILabel YouAreLabel = new UILabel(new CGRect((View.Frame.Width/2)-(widhtNome/2), yyResult + 15 + (sizeNome.Height -sizeYouAre.Height), sizeYouAre.Width, sizeYouAre.Height));
                YouAreLabel.Font = UIFont.FromName("MyriadPro-Regular", 16);
                YouAreLabel.TextColor = UIColor.White;
                YouAreLabel.Text = NSBundle.MainBundle.LocalizedString("Gioca_TuSei", "You are", null);

                UILabel NomeLabel = new UILabel(new CGRect((View.Frame.Width / 2) - (widhtNome / 2) + sizeYouAre.Width +5 , yyResult + 15, sizeNome.Width, sizeNome.Height));
                NomeLabel.Font = UIFont.FromName("MyriadPro-Bold", 18);
                NomeLabel.TextColor = UIColor.White;
                NomeLabel.Text = ListResult.Risultato;

                yyResult += (int)(sizeNome.Height+15);

                sizeDescr = UIStringDrawing.StringSize(ListResult.Descrizione, UIFont.FromName("MyriadPro-Regular", 16), new CGSize(View.Frame.Width - 70, 2000));
                Console.WriteLine("Descr:" + sizeDescr.Height + "  " + sizeDescr.Width);

                UILabel DescrLabel = new UILabel(new CGRect(35, yyResult + 10, View.Frame.Width -70, sizeDescr.Height));
                DescrLabel.Font = UIFont.FromName("MyriadPro-Regular", 16);
                DescrLabel.TextAlignment = UITextAlignment.Justified;
                DescrLabel.TextColor = UIColor.White;
                DescrLabel.Lines = 0;
                DescrLabel.Text = ListResult.Descrizione;

                yyResult += (int)(sizeDescr.Height + 10);

                UIButton BottoneShare = new UIButton(new CGRect((ContentViewDomande.Frame.Width / 2) - 125, yyResult + 20, 250, 40));
                BottoneShare.SetTitle(NSBundle.MainBundle.LocalizedString("Gioca_Condividi", "Share on Facebook", null), UIControlState.Normal);
                BottoneShare.SetTitleColor(UIColor.White, UIControlState.Normal);
                BottoneShare.BackgroundColor = UIColor.FromRGB(153, 19, 27);
                BottoneShare.Font = UIFont.FromName("MyriadPro-Regular", 18);
                BottoneShare.TouchUpInside += (object sender, EventArgs e) =>
                {
                    PostHello();
                };

                yyResult += 60;

                scrollViewRisultato.Add(TitResult);
                scrollViewRisultato.Add(image);
                scrollViewRisultato.Add(YouAreLabel);
                scrollViewRisultato.Add(NomeLabel);
                scrollViewRisultato.Add(DescrLabel);
                scrollViewRisultato.Add(BottoneShare);

                scrollViewRisultato.ContentSize = new CGSize(View.Frame.Width, yyResult + 10);

                ContentViewResult.Add(scrollViewRisultato);

                SetImageAsync(image, NomeLabel, YouAreLabel, DescrLabel, BottoneShare);

            }
            else
            {
                ContentViewResult.Alpha = 0;
                AttendiPage.Alpha = 0;

                WidhtViewDom = (float)(View.Frame.Width - 64);

                scrollViewDomande = new UIScrollView(new CGRect(0, 0, View.Frame.Width, ContentViewDomande.Frame.Height));

                yyDomande = 0;
                yyContainer = 0;

                NumDomanda = new UILabel(new CGRect(0, yyDomande + 15, View.Frame.Width, 25));
                NumDomanda.Font = UIFont.FromName("DaunPenh", 25);
                NumDomanda.TextAlignment = UITextAlignment.Center;
                NumDomanda.TextColor = UIColor.White;
                NumDomanda.Text = NSBundle.MainBundle.LocalizedString("Gioca_Domanda", "Question", null) + " " + (indiceDomanda + 1) + "/" + ListGioca.Count;

                yyDomande += 40;

                sizeDom = UIStringDrawing.StringSize(ListGioca[indiceDomanda].Domanda, UIFont.FromName("MyriadPro-Regular", 17), new CGSize(WidhtViewDom - 20, 2000));
                Console.WriteLine("D:" + sizeDom.Height + "  " + sizeDom.Width);

                sizeRisp1 = UIStringDrawing.StringSize(ListGioca[indiceDomanda].Risposta1, UIFont.FromName("MyriadPro-Regular", 17), new CGSize(WidhtViewDom - 55, 2000));
                Console.WriteLine("R1:" + sizeRisp1.Height + "  " + sizeRisp1.Width);

                sizeRisp2 = UIStringDrawing.StringSize(ListGioca[indiceDomanda].Risposta2, UIFont.FromName("MyriadPro-Regular", 17), new CGSize(WidhtViewDom - 55, 2000));
                Console.WriteLine("R2:" + sizeRisp2.Height + "  " + sizeRisp2.Width);

                sizeRisp3 = UIStringDrawing.StringSize(ListGioca[indiceDomanda].Risposta3, UIFont.FromName("MyriadPro-Regular", 17), new CGSize(WidhtViewDom - 55, 2000));
                Console.WriteLine("R3:" + sizeRisp3.Height + "  " + sizeRisp3.Width);

                float HeightView = (float)(sizeDom.Height + sizeRisp1.Height + sizeRisp2.Height + sizeRisp3.Height + 105);

                BorderView = new UIView(new CGRect(30, yyDomande + 20, View.Frame.Width - 60, HeightView + 4));
                BorderView.BackgroundColor = UIColor.FromRGB(153, 19, 27);

                ContainerView = new UIView(new CGRect(2, 2, View.Frame.Width - 64, HeightView));
                ContainerView.BackgroundColor = UIColor.White;

                yyDomande += (int)(HeightView + 24);

                BottoneConfirm = new UIButton(new CGRect((ContentViewDomande.Frame.Width / 2) - 125, yyDomande + 20, 250, 40));
                BottoneConfirm.SetTitle(NSBundle.MainBundle.LocalizedString("Gioca_Conferma", "Confirmation", null), UIControlState.Normal);
                BottoneConfirm.SetTitleColor(UIColor.White, UIControlState.Normal);
                BottoneConfirm.BackgroundColor = UIColor.FromRGB(153, 19, 27);
                BottoneConfirm.Font = UIFont.FromName("MyriadPro-Regular", 18);
                BottoneConfirm.TouchUpInside += (object sender, EventArgs e) =>
                {
                    CambiaDomanda();
                };

                yyDomande += 60;

                Domanda = new UILabel(new CGRect(10, yyContainer + 20, WidhtViewDom - 20, sizeDom.Height));
                Domanda.Font = UIFont.FromName("MyriadPro-Regular", 17);
                Domanda.TextAlignment = UITextAlignment.Justified;
                Domanda.TextColor = UIColor.Black;
                Domanda.Lines = 0;
                Domanda.Text = ListGioca[indiceDomanda].Domanda;

                yyContainer += (int)(20 + sizeDom.Height);

                RispCheck1 = new UIImageView(new CGRect(10, (yyContainer + 25 + (sizeRisp1.Height / 2 - 15)), 30, 30));
                RispCheck1.Image = NoChecked;
                UITapGestureRecognizer RispCheck1Tap = new UITapGestureRecognizer(() =>
                {
                    CheckImage(0);
                });
                RispCheck1.UserInteractionEnabled = true;
                RispCheck1.AddGestureRecognizer(RispCheck1Tap);

                Risposta1 = new UILabel(new CGRect(45, yyContainer + 25, WidhtViewDom - 55, sizeRisp1.Height));
                Risposta1.Font = UIFont.FromName("MyriadPro-Regular", 17);
                Risposta1.TextAlignment = UITextAlignment.Justified;
                Risposta1.TextColor = UIColor.Black;
                Risposta1.Lines = 0;
                Risposta1.Text = ListGioca[indiceDomanda].Risposta1;

                yyContainer += (int)(25 + sizeRisp1.Height);

                RispCheck2 = new UIImageView(new CGRect(10, (yyContainer + 20 + (sizeRisp2.Height / 2 - 15)), 30, 30));
                RispCheck2.Image = NoChecked;
                UITapGestureRecognizer RispCheck2Tap = new UITapGestureRecognizer(() =>
                {
                    CheckImage(1);
                });
                RispCheck2.UserInteractionEnabled = true;
                RispCheck2.AddGestureRecognizer(RispCheck2Tap);

                Risposta2 = new UILabel(new CGRect(45, yyContainer + 20, WidhtViewDom - 55, sizeRisp2.Height));
                Risposta2.Font = UIFont.FromName("MyriadPro-Regular", 17);
                Risposta2.TextAlignment = UITextAlignment.Justified;
                Risposta2.TextColor = UIColor.Black;
                Risposta2.Lines = 0;
                Risposta2.Text = ListGioca[indiceDomanda].Risposta2;

                yyContainer += (int)(20 + sizeRisp2.Height);

                RispCheck3 = new UIImageView(new CGRect(10, (yyContainer + 20 + (sizeRisp3.Height / 2 - 15)), 30, 30));
                RispCheck3.Image = NoChecked;
                UITapGestureRecognizer RispCheck3Tap = new UITapGestureRecognizer(() =>
                {
                    CheckImage(2);
                });
                RispCheck3.UserInteractionEnabled = true;
                RispCheck3.AddGestureRecognizer(RispCheck3Tap);

                Risposta3 = new UILabel(new CGRect(45, yyContainer + 20, WidhtViewDom - 55, sizeRisp3.Height));
                Risposta3.Font = UIFont.FromName("MyriadPro-Regular", 17);
                Risposta3.TextAlignment = UITextAlignment.Justified;
                Risposta3.TextColor = UIColor.Black;
                Risposta3.Lines = 0;
                Risposta3.Text = ListGioca[indiceDomanda].Risposta3;

                ContainerView.Add(Domanda);
                ContainerView.Add(RispCheck1);
                ContainerView.Add(Risposta1);
                ContainerView.Add(RispCheck2);
                ContainerView.Add(Risposta2);
                ContainerView.Add(RispCheck3);
                ContainerView.Add(Risposta3);

                BorderView.Add(ContainerView);
                scrollViewDomande.Add(NumDomanda);
                scrollViewDomande.Add(BorderView);
                scrollViewDomande.Add(BottoneConfirm);

                scrollViewDomande.ContentSize = new CGSize(View.Frame.Width, yyDomande + 10);

                ContentViewDomande.Add(scrollViewDomande);

                AttendiLabel.Font = UIFont.FromName("MyriadPro-Regular", 18);
                AttendiLabel.Text = NSBundle.MainBundle.LocalizedString("Gioca_Wait", "Wait for the result", null);
            }

        }

        public void CambiaDomanda()
        {
            if (somethingSel)
            {
                indiceDomanda++;
                if (indiceDomanda < ListGioca.Count)
                {
                    somethingSel = false;
                    NumDomanda.Text = NSBundle.MainBundle.LocalizedString("Gioca_Domanda", "Question", null) + " " + (indiceDomanda + 1) + "/" + ListGioca.Count;

                    sizeDom = UIStringDrawing.StringSize(ListGioca[indiceDomanda].Domanda, UIFont.FromName("MyriadPro-Regular", 17), new CGSize(WidhtViewDom - 20, 2000));
                    Console.WriteLine("D:" + sizeDom.Height + "  " + sizeDom.Width);

                    sizeRisp1 = UIStringDrawing.StringSize(ListGioca[indiceDomanda].Risposta1, UIFont.FromName("MyriadPro-Regular", 17), new CGSize(WidhtViewDom - 55, 2000));
                    Console.WriteLine("R1:" + sizeRisp1.Height + "  " + sizeRisp1.Width);

                    sizeRisp2 = UIStringDrawing.StringSize(ListGioca[indiceDomanda].Risposta2, UIFont.FromName("MyriadPro-Regular", 17), new CGSize(WidhtViewDom - 55, 2000));
                    Console.WriteLine("R2:" + sizeRisp2.Height + "  " + sizeRisp2.Width);

                    sizeRisp3 = UIStringDrawing.StringSize(ListGioca[indiceDomanda].Risposta3, UIFont.FromName("MyriadPro-Regular", 17), new CGSize(WidhtViewDom - 55, 2000));
                    Console.WriteLine("R3:" + sizeRisp3.Height + "  " + sizeRisp3.Width);

                    float HeightView = (float)(sizeDom.Height + sizeRisp1.Height + sizeRisp2.Height + sizeRisp3.Height + 105);

                    CGRect frame = BorderView.Frame;
                    frame.Height = HeightView + 4;
                    BorderView.Frame = frame;

                    frame = ContainerView.Frame;
                    frame.Height = HeightView;
                    ContainerView.Frame = frame;

                    yyDomande = (int)(HeightView + 64);

                    BottoneConfirm.RemoveFromSuperview();

                    BottoneConfirm = new UIButton(new CGRect((ContentViewDomande.Frame.Width / 2) - 125, yyDomande + 20, 250, 40));
                    BottoneConfirm.SetTitle(NSBundle.MainBundle.LocalizedString("Gioca_Conferma", "Confirmation", null), UIControlState.Normal);
                    BottoneConfirm.SetTitleColor(UIColor.White, UIControlState.Normal);
                    BottoneConfirm.BackgroundColor = UIColor.FromRGB(153, 19, 27);
                    BottoneConfirm.Font = UIFont.FromName("MyriadPro-Regular", 18);
                    BottoneConfirm.TouchUpInside += (object sender, EventArgs e) =>
                    {
                        CambiaDomanda();
                    };

                    scrollViewDomande.Add(BottoneConfirm);

                    yyDomande += 60;

                    float lenght = ContainerView.Subviews.Length;

                    for (int p = 0; p < lenght; p++)
                        ContainerView.Subviews[0].RemoveFromSuperview();

                    AddQuestion();

                    scrollViewDomande.ContentSize = new CGSize(View.Frame.Width, yyDomande + 10);
                }
                else
                {
                    AttendiPage.Alpha = 1;

                    var client = new RestClient("http://api.netwintec.com:" + 82 + "/");
                    //client.Authenticator = new HttpBasicAuthenticator(username, password);

                    var requestN4U = new RestRequest("play", Method.POST);
                    requestN4U.AddHeader("content-type", "application/json");
                    requestN4U.AddHeader("Net4U-Company", "museotortura");
                    requestN4U.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenMuseoN4U"));
                    requestN4U.Timeout = 60000;

                    JObject oJsonObject = new JObject();

                    Console.WriteLine("Luogo:" + ListResult.Luogo);

                    oJsonObject.Add("museo", ListResult.Luogo);

                    requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);
                    //Console.WriteLine(requestN4U.Parameters[0].Value+"|"+requestN4U.Parameters[0].Type+"|"+requestN4U.Parameters[0].Name);

                    client.ExecuteAsync(requestN4U, (s, e) =>
                    {

                        //var s = client.Execute(requestN4U);
                        Console.WriteLine("GIOCA2:" + s.StatusCode + "|" + s.Content);
                        //if(status
                        //if(s.StatusCode != 0){
                        if (s.StatusCode == HttpStatusCode.OK)
                        {
                            InvokeOnMainThread(() => {

                                ContentViewResult.Alpha = 1;

                                JsonValue json = JsonValue.Parse(s.Content);

                                JsonValue data = json["elementi"];

                                foreach (JsonValue dataItem in data)
                                {

                                    string museo = dataItem["museo"];
                                    if (museo == ListResult.Luogo)
                                    {

                                        JsonValue data3 = dataItem["risultato"];
                                        
                                        ListResult.ImageUrl = data3["result_Image"];
                                        if (NSLocale.PreferredLanguages[0].Substring(0, 2).CompareTo("it") == 0)
                                        {
                                            ListResult.Risultato = data3["result_it"];
                                            ListResult.Descrizione = data3["result_descrizione_it"];
                                        }
                                        else
                                        {
                                            ListResult.Risultato = data3["result_en"];
                                            ListResult.Descrizione = data3["result_descrizione_en"];
                                        }

                                        GiocaHomeController.Instance.ListResult[GiocaHomeController.Instance.selected] = ListResult;
                                        Console.WriteLine("RIS:"+GiocaHomeController.Instance.ListResult[GiocaHomeController.Instance.selected].Risultato);
                                    }

                                }

                                ShowResult();
                            });
                        }
                        else
                        {
                            InvokeOnMainThread(() => {
                                AttendiPage.Alpha = 0;
                                indiceDomanda--;
                                var adErr = new UIAlertView("Errore di rete", "Errore invio dati riprovare", null, "OK", null);
                                adErr.Show();
                            });
                        }
                    });
                }
            }

        }

        public void AddQuestion()
        {
            yyContainer = 0;

            Domanda = new UILabel(new CGRect(10, yyContainer + 20, WidhtViewDom - 20, sizeDom.Height));
            Domanda.Font = UIFont.FromName("MyriadPro-Regular", 17);
            Domanda.TextAlignment = UITextAlignment.Justified;
            Domanda.TextColor = UIColor.Black;
            Domanda.Lines = 0;
            Domanda.Text = ListGioca[indiceDomanda].Domanda;

            yyContainer += (int)(20 + sizeDom.Height);

            RispCheck1 = new UIImageView(new CGRect(10, (yyContainer + 25 + (sizeRisp1.Height / 2 - 15)), 30, 30));
            RispCheck1.Image = NoChecked;
            UITapGestureRecognizer RispCheck1Tap = new UITapGestureRecognizer(() =>
            {
                CheckImage(0);
            });
            RispCheck1.UserInteractionEnabled = true;
            RispCheck1.AddGestureRecognizer(RispCheck1Tap);

            Risposta1 = new UILabel(new CGRect(45, yyContainer + 25, WidhtViewDom - 55, sizeRisp1.Height));
            Risposta1.Font = UIFont.FromName("MyriadPro-Regular", 17);
            Risposta1.TextAlignment = UITextAlignment.Justified;
            Risposta1.TextColor = UIColor.Black;
            Risposta1.Lines = 0;
            Risposta1.Text = ListGioca[indiceDomanda].Risposta1;

            yyContainer += (int)(25 + sizeRisp1.Height);

            RispCheck2 = new UIImageView(new CGRect(10, (yyContainer + 20 + (sizeRisp2.Height / 2 - 15)), 30, 30));
            RispCheck2.Image = NoChecked;
            UITapGestureRecognizer RispCheck2Tap = new UITapGestureRecognizer(() =>
            {
                CheckImage(1);
            });
            RispCheck2.UserInteractionEnabled = true;
            RispCheck2.AddGestureRecognizer(RispCheck2Tap);

            Risposta2 = new UILabel(new CGRect(45, yyContainer + 20, WidhtViewDom - 55, sizeRisp2.Height));
            Risposta2.Font = UIFont.FromName("MyriadPro-Regular", 17);
            Risposta2.TextAlignment = UITextAlignment.Justified;
            Risposta2.TextColor = UIColor.Black;
            Risposta2.Lines = 0;
            Risposta2.Text = ListGioca[indiceDomanda].Risposta2;

            yyContainer += (int)(20 + sizeRisp2.Height);

            RispCheck3 = new UIImageView(new CGRect(10, (yyContainer + 20 + (sizeRisp3.Height / 2 - 15)), 30, 30));
            RispCheck3.Image = NoChecked;
            UITapGestureRecognizer RispCheck3Tap = new UITapGestureRecognizer(() =>
            {
                CheckImage(2);
            });
            RispCheck3.UserInteractionEnabled = true;
            RispCheck3.AddGestureRecognizer(RispCheck3Tap);

            Risposta3 = new UILabel(new CGRect(45, yyContainer + 20, WidhtViewDom - 55, sizeRisp3.Height));
            Risposta3.Font = UIFont.FromName("MyriadPro-Regular", 17);
            Risposta3.TextAlignment = UITextAlignment.Justified;
            Risposta3.TextColor = UIColor.Black;
            Risposta3.Lines = 0;
            Risposta3.Text = ListGioca[indiceDomanda].Risposta3;

            ContainerView.Add(Domanda);
            ContainerView.Add(RispCheck1);
            ContainerView.Add(Risposta1);
            ContainerView.Add(RispCheck2);
            ContainerView.Add(Risposta2);
            ContainerView.Add(RispCheck3);
            ContainerView.Add(Risposta3);

        }

        public void ShowResult()
        {
            scrollViewRisultato = new UIScrollView(new CGRect(0, 0, View.Frame.Width, ContentViewResult.Frame.Height));
            yyResult = 0;

            UILabel TitResult = new UILabel(new CGRect(0, yyResult + 15, View.Frame.Width, 25));
            TitResult.Font = UIFont.FromName("DaunPenh", 25);
            TitResult.TextAlignment = UITextAlignment.Center;
            TitResult.TextColor = UIColor.White;
            TitResult.Text = NSBundle.MainBundle.LocalizedString("Gioca_Risultato", "RESULT", null);

            yyResult += 40;

            UIImageView image = new UIImageView(new CGRect(View.Frame.Width / 2 - 87.5, yyResult + 20, 175, 175));
            image.Image = UIImage.FromFile("placeholder.png");

            yyResult += 195;

            sizeYouAre = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Gioca_TuSei", "You are", null), UIFont.FromName("MyriadPro-Regular", 16), new CGSize(View.Frame.Width / 2, 2000));
            Console.WriteLine("YouAre:" + sizeYouAre.Height + "  " + sizeYouAre.Width);

            sizeNome = UIStringDrawing.StringSize(ListResult.Risultato, UIFont.FromName("MyriadPro-Bold", 18), new CGSize(View.Frame.Width - sizeYouAre.Width -5 , 2000));
            Console.WriteLine("Nome:" + sizeNome.Height + "  " + sizeNome.Width);

            float widhtNome = (float)(sizeNome.Width + sizeYouAre.Width + 5);

            UILabel YouAreLabel = new UILabel(new CGRect((View.Frame.Width / 2) - (widhtNome / 2), yyResult + 15 + (sizeNome.Height - sizeYouAre.Height), sizeYouAre.Width, sizeYouAre.Height));
            YouAreLabel.Font = UIFont.FromName("MyriadPro-Regular", 16);
            YouAreLabel.TextColor = UIColor.White;
            YouAreLabel.Text = NSBundle.MainBundle.LocalizedString("Gioca_TuSei", "You are", null);

            UILabel NomeLabel = new UILabel(new CGRect((View.Frame.Width / 2) - (widhtNome / 2) + sizeYouAre.Width + 5, yyResult + 15, sizeNome.Width, sizeNome.Height));
            NomeLabel.Font = UIFont.FromName("MyriadPro-Bold", 18);
            NomeLabel.TextColor = UIColor.White;
            NomeLabel.Text = ListResult.Risultato;

            yyResult += (int)(sizeNome.Height + 15);

            sizeDescr = UIStringDrawing.StringSize(ListResult.Descrizione, UIFont.FromName("MyriadPro-Regular", 16), new CGSize(View.Frame.Width - 70, 2000));
            Console.WriteLine("Descr:" + sizeDescr.Height + "  " + sizeDescr.Width);

            UILabel DescrLabel = new UILabel(new CGRect(35, yyResult + 10, View.Frame.Width - 70, sizeDescr.Height));
            DescrLabel.Font = UIFont.FromName("MyriadPro-Regular", 16);
            DescrLabel.TextAlignment = UITextAlignment.Justified;
            DescrLabel.TextColor = UIColor.White;
            DescrLabel.Lines = 0;
            DescrLabel.Text = ListResult.Descrizione;

            yyResult += (int)(sizeDescr.Height + 10);

            UIButton BottoneShare = new UIButton(new CGRect((ContentViewDomande.Frame.Width / 2) - 125, yyResult + 20, 250, 40));
            BottoneShare.SetTitle(NSBundle.MainBundle.LocalizedString("Gioca_Condividi", "Share on Facebook", null), UIControlState.Normal);
            BottoneShare.SetTitleColor(UIColor.White, UIControlState.Normal);
            BottoneShare.BackgroundColor = UIColor.FromRGB(153, 19, 27);
            BottoneShare.Font = UIFont.FromName("MyriadPro-Regular", 18);
            BottoneShare.TouchUpInside += (object sender, EventArgs e) =>
            {
                PostHello();
            };

            yyResult += 60;

            scrollViewRisultato.Add(TitResult);
            scrollViewRisultato.Add(image);
            scrollViewRisultato.Add(YouAreLabel);
            scrollViewRisultato.Add(NomeLabel);
            scrollViewRisultato.Add(DescrLabel);
            scrollViewRisultato.Add(BottoneShare);

            scrollViewRisultato.ContentSize = new CGSize(View.Frame.Width, yyResult + 10);

            ContentViewResult.Add(scrollViewRisultato);

            SetImageAsync(image, NomeLabel, YouAreLabel, DescrLabel, BottoneShare);
        }

        public void CheckImage(int i)
        {
            somethingSel = true;

            if (i == 0)
            {
                RispCheck1.Image = Checked;
                RispCheck2.Image = NoChecked;
                RispCheck3.Image = NoChecked;
            }
            if (i == 1)
            {
                RispCheck1.Image = NoChecked;
                RispCheck2.Image = Checked;
                RispCheck3.Image = NoChecked;
            }
            if (i == 2)
            {
                RispCheck1.Image = NoChecked;
                RispCheck2.Image = NoChecked;
                RispCheck3.Image = Checked;
            }
        }

        public void SetImageAsync(UIImageView image, UILabel nome,UILabel youAre,UILabel descr, UIButton share)
        {
            UIImage Placeholder = UIImage.FromFile("placeholder.png");


            UIImage cachedImage;
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {

                    // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                    //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                    cachedImage = FromUrl(ListResult.ImageUrl) ?? Placeholder;


                    InvokeOnMainThread(() =>
                    {
                        nfloat w = cachedImage.Size.Width;
                        nfloat h = cachedImage.Size.Height;

                        float imageHeight = (float)(h * 175 / w);
                        Console.WriteLine(w + "  " + h+"   175    "+imageHeight);

                        Double yyy = 40;
                        image.Frame = new CGRect(View.Frame.Width / 2 - 87.5, yyy + 20, 175, imageHeight);
                        yyy += imageHeight + 20;


                        float widhtNome = (float)(sizeNome.Width + sizeYouAre.Width + 5);
                        youAre.Frame = new CGRect((View.Frame.Width / 2) - (widhtNome / 2), yyy + 15 + (sizeNome.Height - sizeYouAre.Height), sizeYouAre.Width, sizeYouAre.Height);
                        nome.Frame = new CGRect((View.Frame.Width / 2) - (widhtNome / 2) + sizeYouAre.Width + 5, yyy + 15, sizeNome.Width, sizeNome.Height);

                        yyy += (int)(sizeNome.Height + 15);

                        descr.Frame = new CGRect(35, yyy + 10, View.Frame.Width - 70, sizeDescr.Height);

                        yyy += (int)(sizeDescr.Height + 10);

                        share.Frame = new CGRect((ContentViewDomande.Frame.Width / 2) - 125, yyy + 20, 250, 40);

                        yyy += 60;

                        scrollViewRisultato.ContentSize = new CGSize(View.Frame.Width, yyy + 20);
                        image.Image = cachedImage;

                    });

                });
            }
            catch (Exception e)
            {
                Console.WriteLine("URL LOAD :" + e.StackTrace);
            }

        }

        static UIImage FromUrl(string uri)
        {
            try
            {
                using (var url = new NSUrl(uri))
                using (var data = NSData.FromUrl(url))
                    if (data != null)
                        return UIImage.LoadFromData(data);
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        void PostHello()
        {
            // Verify if you have permissions to post into user wall,
            // if not, ask to user if he/she wants to let your app post things for them
            if (AccessToken.CurrentAccessToken == null) {
                AskPermissions(null, PostHello);
                return;
            }
            else
            {
                /*if (!AccessToken.CurrentAccessToken.HasGranted("publish_actions") && !DoLogIn)
                {
                    AskPermissions(null, PostHello);//new[] { "publish_actions" }, PostHello);
                    return;
                }*/

                ShareLinkContent content = new ShareLinkContent();
                content.ContentTitle = NSBundle.MainBundle.LocalizedString("Gioca_TuSei", "You are", null) + " " + ListResult.Risultato;
                content.SetContentUrl(new NSUrl("http://www.torturemuseum.it/"));
                content.ImageURL = new NSUrl(ListResult.ImageUrl);
                content.ContentDescription = ListResult.Descrizione;

                //ShareDialog dialog = ShareDialog.Show(this,content,null);
                dialog.FromViewController = this;
                dialog.SetShareContent(content);
                dialog.Mode = ShareDialogMode.FeedBrowser;
                dialog.Show();
    /*let dialog = FBSDKShareDialog()
    dialog.fromViewController = viewController
    dialog.shareContent = content
    dialog.mode = FBSDKShareDialogMode.FeedBrowser
    dialog.delegate = delegate
    dialog.show()*/

                // Once you have the permissions, you can publish or delete the post from your wall 
                /* GraphRequest request;

                 NSDictionary message = new NSDictionary(
                     "link", "http://www.torturemuseum.it/",
                     "picture", ListResult.ImageUrl,
                     "name", NSBundle.MainBundle.LocalizedString("Gioca_TuSei", "You are", null) + " " + ListResult.Risultato,
                     "description", ListResult.Descrizione);


                 request = new GraphRequest("me/feed", message, AccessToken.CurrentAccessToken.TokenString, null, "POST");
                 var requestConnection = new GraphRequestConnection();

                 // Handle the result of the request
                 requestConnection.AddRequest(request, (connection, result, error) =>
                 {
                 // Handle if something went wrong
                 if (error != null)
                     {
                         Console.WriteLine("ERRORE:" + error.Description);
                     //ShowMessageBox("Error...", error.Description, "Ok", null, null);
                     return;
                     }

                     Console.WriteLine("POSTATO");
                     new UIAlertView("Successo", "Condivisione su Facebook riuscita", null, "Ok", null).Show();
                 /* Post the Hello! to your wall
                 if (!isHelloPosted)
                 {
                     helloId = (result as NSDictionary)["id"].ToString();
                     isHelloPosted = true;
                     InvokeOnMainThread(() => {
                         strPostHello.Caption = "Delete \"Hello\" from your wall";
                         Root.Reload(strPostHello, UITableViewRowAnimation.Left);
                         ShowMessageBox("Success!!", "Posted Hello in your wall, MsgId: " + helloId, "Ok", null, null);
                     });
                 }
                 else
                 { // Delete the Hello! from your wall
                     helloId = "";

                     isHelloPosted = false;
                     InvokeOnMainThread(() => {
                         strPostHello.Caption = "Post \"Hello\" on your wall";
                         Root.Reload(strPostHello, UITableViewRowAnimation.Left);
                         ShowMessageBox("Success", "Deleted Hello from your wall", "Ok", null, null);
                     });
                 }
                 });
                 requestConnection.Start(); */
            }
        }

        void AskPermissions(string[] permissions, Action successHandler)
        {

            //ShowMessageBox(title, message, "Maybe Later", new[] { "Ok" }, () => {
                // If they let you do things, ask for a new Access Token with the new permission
                var login = new LoginManager();
                login.LogInWithPublishPermissions(permissions, (result, error) => {
                    // Handle if something went wrong with the request
                    if (error != null)
                    {
                        new UIAlertView("Errore di rete", "Impossibile postare al momento", null, "Ok", null).Show();
                        return;
                    }

                    // Handle if the user cancelled the request
                    if (result.IsCancelled)
                    {
                        new UIAlertView("Errore di rete", "Impossibile postare al momento", null, "OK", null).Show();
                        return;
                    }

                    // Do your magic if the request was successful
                    DoLogIn = true;
                    successHandler();
                });
            //});
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }
    }

    public class MyShareDialog : ShareDialog
    {
        public MyShareDialog() : base(NSObjectFlag.Empty)
        {

        }
    }
}
