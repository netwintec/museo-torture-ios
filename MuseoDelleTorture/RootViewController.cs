﻿using System;
using System.Drawing;
using Foundation;
using UIKit;
using CoreGraphics;
using MediaPlayer;
using System.Text;
using RestSharp;
using Facebook.LoginKit;
using CoreBluetooth;
using System.Collections.Generic;
using System.Globalization;

namespace MuseoDelleTorture
{
    public partial class RootViewController : UIViewController
    {

        MPMoviePlayerController player;
        LoginManager loginManager;

        static bool UserInterfaceIdiomIsPhone
        {
            get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
        }

        public RootViewController(IntPtr handle) : base(handle)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        #region View lifecycle

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();


            //MyCentralManagerDelegate cmd = new MyCentralManagerDelegate();
            //CBCentralManager _manager = new CBCentralManager(cmd, null);
            //_manager.Init();


            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
            }

            UIViewController lp = storyboard.InstantiateViewController("LoginPage");
            //UIViewController rp = storyboard.InstantiateViewController("RegistratiPage");

            var lang = NSLocale.PreferredLanguages[0];
            var lang2 = NSLocale.CurrentLocale.LocaleIdentifier;
            var lang3 = NSLocale.AutoUpdatingCurrentLocale.LocaleIdentifier;

            Console.WriteLine("lang:" + lang + "|" + lang2 + "|" + lang3);
            UIButton BottoneAccedi = new UIButton(new CGRect(2, 2, View.Frame.Width - 4, ButtonView.Frame.Height - 4));
            BottoneAccedi.SetTitle(NSBundle.MainBundle.LocalizedString("Root_Start", "Login and visit the Exhibition", null), UIControlState.Normal);
            BottoneAccedi.BackgroundColor = UIColor.Black;
            BottoneAccedi.SetTitleColor(UIColor.White, UIControlState.Normal);
            BottoneAccedi.TouchUpInside += delegate
            {
                //base.NavigationController.PresentModalViewController(lp, true);

                UtilityLoginManager loginManagerNormal = new UtilityLoginManager();
                List<string> result = loginManagerNormal.Login("a@a.it", "a");
                Console.WriteLine(result[0]);

                if (result[0] == "SUCCESS")
                {
                    Console.WriteLine(result[1]);

                    NSUserDefaults.StandardUserDefaults.SetString(result[1], "TokenMuseoN4U");

                    //NSUserDefaults.StandardUserDefaults.SetString(e.Result.Token.TokenString, "TokenLovieFB");

                    UIViewController hp = storyboard.InstantiateViewController("SWController");
                    base.NavigationController.PresentModalViewController(hp, true);

                }

                if (result[0] == "ERROR")
                {
                    var adErr = new UIAlertView("Login Non Riuscito", "E-Mail o Password Errata", null, "OK", null);
                    adErr.Show();

                }

                if (result[0] == "ERRORDATA")
                {
                    var adErr = new UIAlertView("Errore Dati", "E-Mail o Password Non Inseriti o Formato errato", null, "OK", null);
                    adErr.Show();

                }

            };

            /*UIButton BottoneRegistrati = new UIButton(new CGRect(View.Frame.Width / 2 + 1, 2, View.Frame.Width / 2 - 3, ButtonView.Frame.Height - 4));
            BottoneRegistrati.SetTitle(NSBundle.MainBundle.LocalizedString("Root_Registrati", "Register", null), UIControlState.Normal);
            BottoneRegistrati.BackgroundColor = UIColor.Black;
            BottoneRegistrati.SetTitleColor(UIColor.White, UIControlState.Normal);
            BottoneRegistrati.TouchUpInside += delegate
            {
                base.NavigationController.PresentModalViewController(rp, true);
            };*/

            BottoneAccedi.Font = UIFont.FromName("MyriadPro-Regular", 18);
            //BottoneRegistrati.Font = UIFont.FromName("MyriadPro-Regular", 18);

            ButtonView.AddSubview(BottoneAccedi);
            //ButtonView.AddSubview(BottoneRegistrati);

            loginManager = new LoginManager();

            Console.WriteLine("\"" + NSUserDefaults.StandardUserDefaults.StringForKey("TokenMuseoN4U") + "\"");

            //NSUserDefaults.StandardUserDefaults.SetString("", "TokenMuseoN4U");

            if (!string.IsNullOrWhiteSpace(NSUserDefaults.StandardUserDefaults.StringForKey("TokenMuseoN4U")))
            {
                //DEVELOPMENT
                var client = new RestClient("http://api.netwintec.com:82/");
                //DISTRIBUTION
                //var client = new RestClient("http://api.netwintec.com:82"/");


                var requestN4U = new RestRequest("Profile", Method.GET);
                requestN4U.AddHeader("content-type", "application/json");
                requestN4U.AddHeader("Net4U-Company", "museotortura");
                requestN4U.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenMuseoN4U"));

                var response = client.Execute(requestN4U);

                Console.WriteLine(response.Content + "|" + response.StatusCode);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Console.WriteLine("TOKEN:" + NSUserDefaults.StandardUserDefaults.StringForKey("TokenMuseoN4U"));

                    //AUTO DOWNLOAD

                    string MuseumName = NSUserDefaults.StandardUserDefaults.StringForKey("Museum");
                    DateTime EndVisit = Utility.FromUnixTime((long)NSUserDefaults.StandardUserDefaults.DoubleForKey("EndVisit"));
                    DateTime EndVisit2;
                    try
                    {
                        EndVisit2 = DateTime.ParseExact(NSUserDefaults.StandardUserDefaults.StringForKey("EndVisit_v2"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                    }
                    catch (Exception e)
                    {
                        EndVisit2 = DateTime.Now.AddYears(-1);
                    }

                    Console.WriteLine(MuseumName + "|" + EndVisit + "|" + EndVisit.CompareTo(DateTime.Now) + "|" + EndVisit2 + "|" + EndVisit2.CompareTo(DateTime.Now));


                    if (EndVisit2.CompareTo(DateTime.Now) > 0)
                    {
                        AppDelegate.Instance.GetMuseumData(MuseumName, null, null, (long)(EndVisit2 - DateTime.Now).TotalMilliseconds);
                    }

                    UIViewController lp2 = storyboard.InstantiateViewController("SWController");
                    base.NavigationController.PresentModalViewController(lp2, true);
                }
                else
                {
                    loginManager.LogOut();
                    BlackView.Alpha = 0;
                }
            }
            else
            {
                loginManager.LogOut();
                BlackView.Alpha = 0;
            }

            int W = 0;
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                W = (int)(View.Frame.Width - 100);
            }
            else
            {
                W = (int)(View.Frame.Width - 200);
            }

            int H = (int)((W * 51) / 200);

            UIImageView imageView = new UIImageView(new CGRect(View.Frame.Width / 2 - W / 2, VideoView.Frame.Height / 2 - H / 2, W, H));
            imageView.Image = UIImage.FromFile("logo_pagina_iniziale.png");

            VideoView.Add(imageView);

            /*********** FONT APP ************

            MyriadPro-Regular
            MyriadPro-Bold
            DaunPenh

            **********************************/

            /* var fontList = new StringBuilder();
             var familyNames = UIFont.FamilyNames;

             foreach (var familyName in familyNames)
             {
                 fontList.Append(String.Format("Family; {0} \n", familyName));
                 Console.WriteLine("Family: {0}\n", familyName);

                 var fontNames = UIFont.FontNamesForFamilyName(familyName);

                 foreach (var fontName in fontNames)
                 {
                     Console.WriteLine("\tFont: {0}\n", fontName);
                 }

                 // Perform any additional setup after loading the view, typically from a nib.
             }*/
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
    }
}