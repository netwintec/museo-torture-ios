﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace MuseoDelleTorture
{
    class TableSourceMusei : UITableViewSource
    {

        public const string TEXT_TIT_1="SAN GIMIGNANO";
        public const string TEXT_TIT_2="VOLTERRA";
        public const string TEXT_TIT_3="SIENA";
        public const string TEXT_TIT_4="LUCCA";
        public const string TEXT_TIT_5="MONTEPULCIANO";

        public const string TEXT_DESCR_PRICE_IT_1 = "intero € 10,00; ridotto € 7,00; gruppi € 5,00";
        public const string TEXT_DESCR_PRICE_IT_2 = "intero € 8,00; ridotto € 6,00; gruppi € 4,00";
        public const string TEXT_DESCR_OPEN_IT_1 = "Tutti i giorni dalle 10 alle 19";
        public const string TEXT_DESCR_OPEN_IT_2 = "Tutti i giorni dalle 10 alle 20";
        public const string TEXT_DESCR_OPEN_IT_3 = "(Invernale solo sabato e domenica)";

        public const string TEXT_DESCR_PRICE_EN_1 = "full price € 10,00; reduced € 7,00; groups € 5,00";
        public const string TEXT_DESCR_PRICE_EN_2 = "full price € 8,00; reduced € 6,00; groups € 4,00";
        public const string TEXT_DESCR_OPEN_EN_1 = "Everyday from 10 to 19";
        public const string TEXT_DESCR_OPEN_EN_2 = "Everyday from 10 to 20";
        public const string TEXT_DESCR_OPEN_EN_3 = "(During winter open only an Saturdays and Sundays)";

        public const string TXT_VALID_IT = "Biglietto valido per entrambi i musei";
        public const string TXT_VALID_EN = "Ticket valid for both museums";

        int righe;
        bool SelFirst = false;
        bool SelSecond = false;
        bool SelThird = false;
        bool SelFourth = false;
        bool SelFifth = false;
        bool isPhone;

        float widht;

        float TitleHeight1, TitleHeight2, TitleHeight3, TitleHeight4, TitleHeight5;

        MuseiController super;

        CGSize size1, size2, size3, size4;

        float widthOpen, widthPrice, widthInfo;

        string TEXT_PRICE_1, TEXT_PRICE_2, TEXT_OPEN_1, TEXT_OPEN_2, TEXT_OPEN_3,TEXT_VALID;

        public TableSourceMusei(float w, MuseiController s)
        {
            righe = 10;
            widht = w;
            super = s;
            
            if (NSLocale.PreferredLanguages[0].Substring(0, 2).CompareTo("it") == 0)
            {
                TEXT_PRICE_1 = TEXT_DESCR_PRICE_IT_1;
                TEXT_PRICE_2 = TEXT_DESCR_PRICE_IT_2;
                TEXT_OPEN_1 = TEXT_DESCR_OPEN_IT_1;
                TEXT_OPEN_2 = TEXT_DESCR_OPEN_IT_2;
                TEXT_OPEN_3 = TEXT_DESCR_OPEN_IT_3;
                TEXT_VALID = TXT_VALID_IT;
            }
            else
            {
                TEXT_PRICE_1 = TEXT_DESCR_PRICE_EN_1;
                TEXT_PRICE_2 = TEXT_DESCR_PRICE_EN_2;
                TEXT_OPEN_1 = TEXT_DESCR_OPEN_EN_1;
                TEXT_OPEN_2 = TEXT_DESCR_OPEN_EN_2;
                TEXT_OPEN_3 = TEXT_DESCR_OPEN_EN_3;
                TEXT_VALID = TXT_VALID_EN;
            }

            var size = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Musei_Apertura", "Opening Time:", null), UIFont.FromName("MyriadPro-Bold", 14), new CGSize(2000, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);
            widthOpen = (float)size.Width;

            size = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Musei_Prezzo", "Price:", null), UIFont.FromName("MyriadPro-Bold", 14), new CGSize(2000, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);
            widthPrice = (float)size.Width;

            size = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Musei_Info", "Information:", null), UIFont.FromName("MyriadPro-Bold", 14), new CGSize(2000, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);
            widthInfo = (float)size.Width;

        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return righe;
        }



        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {

            if (indexPath.Row == 0)
            {
                if (SelFirst)
                {
                    SelFirst = false;
                    //super.CloseChange((float)size1.Height + 8);
                }
                else
                {
                    SelFirst = true;
                    SelFourth = false;
                    SelThird = false;
                    SelFifth = false;
                    SelSecond = false;
                    super.ChangePosition(0);
                    //super.OpenChange((float)size1.Height + 8);
                }
            }
            if (indexPath.Row == 2)
            {
                if (SelSecond)
                {
                    SelSecond = false;
                    //super.CloseChange((float)size2.Height + 8);
                }
                else
                {
                    SelSecond = true;
                    SelFourth = false;
                    SelThird = false;
                    SelFifth = false;
                    SelFirst = false;
                    super.ChangePosition(1);
                    //super.OpenChange((float)size2.Height + 8);
                }
            }
            if (indexPath.Row == 4)
            {
                if (SelThird)
                {
                    SelThird = false;
                    //super.CloseChange((float)size3.Height + 8);
                }
                else
                {
                    SelThird = true;
                    SelFourth = false;
                    SelFifth = false;
                    SelSecond = false;
                    SelFirst = false;
                    super.ChangePosition(2);
                    //super.OpenChange((float)size3.Height + 8);
                }
            }
            if (indexPath.Row == 6)
            {
                if (SelFourth)
                {
                    SelFourth = false;
                    //super.CloseChange((float)size4.Height + 8);
                }
                else
                {
                    SelFourth = true;
                    SelFifth = false;
                    SelThird = false;
                    SelSecond = false;
                    SelFirst = false;
                    super.ChangePosition(3);
                    //super.OpenChange((float)size4.Height + 8);
                }
            }
            if (indexPath.Row == 8)
            {
                if (SelFifth)
                {
                    SelFifth = false;
                    //super.CloseChange((float)size4.Height + 8);
                }
                else
                {
                    SelFifth = true;
                    SelFourth = false;
                    SelThird = false;
                    SelSecond = false;
                    SelFirst = false;
                    super.ChangePosition(4);
                    //super.OpenChange((float)size4.Height + 8);
                }
            }

            tableView.ReloadData();
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {

            int row = indexPath.Row;
            if (row == 1 && SelFirst)
            {
                float heightDescrRow = 30;
                var size = UIStringDrawing.StringSize(TEXT_OPEN_1, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht-widthOpen-5, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height + 5);

                size = UIStringDrawing.StringSize(TEXT_OPEN_3, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height);

                size = UIStringDrawing.StringSize(TEXT_PRICE_1, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht- widthPrice - 5, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height + 5);

                size = UIStringDrawing.StringSize(TEXT_VALID, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height + 5);

                heightDescrRow += (float)(35);

                return (nfloat)(heightDescrRow);
            }
            if (row == 1 && !SelFirst)
            {
                return 0;
            }
            if (row == 3 && SelSecond)
            {
                float heightDescrRow = 30;
                var size = UIStringDrawing.StringSize(TEXT_OPEN_1, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht - widthOpen - 5, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height + 5);

                size = UIStringDrawing.StringSize(TEXT_OPEN_3, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height);

                size = UIStringDrawing.StringSize(TEXT_PRICE_2, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht - widthPrice - 5, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height + 5);

                heightDescrRow += (float)(35);
                return (nfloat)(150);
            }
            if (row == 3 && !SelSecond)
            {
                return 0;
            }
            if (row == 5 && SelThird)
            {
                float heightDescrRow = 30;
                var size = UIStringDrawing.StringSize(TEXT_OPEN_1, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht - widthOpen - 5, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height + 5);

                size = UIStringDrawing.StringSize(TEXT_OPEN_3, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height);

                size = UIStringDrawing.StringSize(TEXT_PRICE_1, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht - widthPrice - 5, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height + 5);

                heightDescrRow += (float)(35);
                return (nfloat)(150);
            }
            if (row == 5 && !SelThird)
            {
                return 0;
            }
            if (row == 7 && SelFourth)
            {
                float heightDescrRow = 30;
                var size = UIStringDrawing.StringSize(TEXT_OPEN_2, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht - widthOpen - 5, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height + 5);

                size = UIStringDrawing.StringSize(TEXT_OPEN_3, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height);

                size = UIStringDrawing.StringSize(TEXT_PRICE_1, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht - widthPrice - 5, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height + 5);

                heightDescrRow += (float)(35);
                return (nfloat)(150);
            }
            if (row == 7 && !SelFourth)
            {
                return 0;
            }
            if (row == 9 && SelFifth)
            {
                float heightDescrRow = 30;
                var size = UIStringDrawing.StringSize(TEXT_OPEN_1, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht - widthOpen - 5, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height + 5);

                size = UIStringDrawing.StringSize(TEXT_OPEN_3, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height);

                size = UIStringDrawing.StringSize(TEXT_PRICE_2, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht - widthPrice - 5, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);
                heightDescrRow += (float)(size.Height + 5);

                heightDescrRow += (float)(35);
                return (nfloat)(150);
            }
            if (row == 9 && !SelFifth)
            {
                return 0;
            }

            float NormalHeight = 40;

            if (row == 0)
            {
                var sizeTit = UIStringDrawing.StringSize(TEXT_TIT_1, UIFont.FromName("DaunPenh", 20), new CGSize(widht - 25, 2000));
                Console.WriteLine(row + " : " + sizeTit.Width + " " + sizeTit.Height);
                if (sizeTit.Height > 30)
                {
                    NormalHeight += 30;
                }
                TitleHeight1 = NormalHeight;
            }
            if (row == 2)
            {
                var sizeTit = UIStringDrawing.StringSize(TEXT_TIT_2, UIFont.FromName("DaunPenh", 20), new CGSize(widht - 25, 2000));
                Console.WriteLine(row + " : " + sizeTit.Width + " " + sizeTit.Height);
                if (sizeTit.Height > 30)
                {
                    NormalHeight += 30;
                }
                TitleHeight2 = NormalHeight;
            }
            if (row == 4)
            {
                var sizeTit = UIStringDrawing.StringSize(TEXT_TIT_3, UIFont.FromName("DaunPenh", 20), new CGSize(widht - 25, 2000));
                Console.WriteLine(row + " : " + sizeTit.Width + " " + sizeTit.Height);
                if (sizeTit.Height > 30)
                {
                    NormalHeight += 30;
                }
                TitleHeight3 = NormalHeight;
            }
            if (row == 6)
            {
                var sizeTit = UIStringDrawing.StringSize(TEXT_TIT_4, UIFont.FromName("DaunPenh", 20), new CGSize(widht - 25, 2000));
                Console.WriteLine(row + " : " + sizeTit.Width + " " + sizeTit.Height);
                if (sizeTit.Height > 30)
                {
                    NormalHeight += 30;
                    //super.OpenChange(20);
                }
                TitleHeight4 = NormalHeight;
            }
            if (row == 8)
            {
                var sizeTit = UIStringDrawing.StringSize(TEXT_TIT_5, UIFont.FromName("DaunPenh", 20), new CGSize(widht - 25, 2000));
                Console.WriteLine(row + " : " + sizeTit.Width + " " + sizeTit.Height);
                if (sizeTit.Height > 30)
                {
                    NormalHeight += 30;
                    //super.OpenChange(20);
                }
                TitleHeight5 = NormalHeight;
            }

            return (nfloat)NormalHeight;

        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            int row = indexPath.Row;
            Console.WriteLine(row);

            if (row == 0 && SelFirst)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoTitolo cell = new CustomCellMuseoTitolo(cellIdentifier, true, TEXT_TIT_1, TitleHeight1);
                cell.UpdateCell(TEXT_TIT_1
                                , UIImage.FromFile("freccia_giu.png"),UIImage.FromFile("SanGimIcon.png"));
                return cell;
            }
            if (row == 0 && !SelFirst)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoTitolo cell = new CustomCellMuseoTitolo(cellIdentifier, false, TEXT_TIT_1, TitleHeight1);
                cell.UpdateCell(TEXT_TIT_1
                                , UIImage.FromFile("freccia_su.png"), UIImage.FromFile("SanGimIcon.png"));
                return cell;
            }

            if (row == 2 && SelSecond)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoTitolo cell = new CustomCellMuseoTitolo(cellIdentifier, true, TEXT_TIT_2, TitleHeight2);
                cell.UpdateCell(TEXT_TIT_2
                                , UIImage.FromFile("freccia_giu.png"), UIImage.FromFile("VoltIcon.png"));
                return cell;
            }
            if (row == 2 && !SelSecond)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoTitolo cell = new CustomCellMuseoTitolo(cellIdentifier, false, TEXT_TIT_2, TitleHeight2);
                cell.UpdateCell(TEXT_TIT_2
                                , UIImage.FromFile("freccia_su.png"), UIImage.FromFile("VoltIcon.png"));
                return cell;
            }

            if (row == 4 && SelThird)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoTitolo cell = new CustomCellMuseoTitolo(cellIdentifier, true, TEXT_TIT_3, TitleHeight3);
                cell.UpdateCell(TEXT_TIT_3
                                , UIImage.FromFile("freccia_giu.png"), UIImage.FromFile("SienIcon.png"));
                return cell;
            }
            if (row == 4 && !SelThird)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoTitolo cell = new CustomCellMuseoTitolo(cellIdentifier, false, TEXT_TIT_3, TitleHeight3);
                cell.UpdateCell(TEXT_TIT_3
                                , UIImage.FromFile("freccia_su.png"), UIImage.FromFile("SienIcon.png"));
                return cell;
            }
            if (row == 6 && SelFourth)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoTitolo cell = new CustomCellMuseoTitolo(cellIdentifier, true, TEXT_TIT_4, TitleHeight4);
                cell.UpdateCell(TEXT_TIT_4
                                , UIImage.FromFile("freccia_giu.png"), UIImage.FromFile("LuccaIcon.png"));
                return cell;
            }
            if (row == 6 && !SelFourth)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoTitolo cell = new CustomCellMuseoTitolo(cellIdentifier, false, TEXT_TIT_5, TitleHeight5);
                cell.UpdateCell(TEXT_TIT_4
                                , UIImage.FromFile("freccia_su.png"), UIImage.FromFile("LuccaIcon.png"));
                return cell;
            }
            if (row == 8 && SelFifth)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoTitolo cell = new CustomCellMuseoTitolo(cellIdentifier, true, TEXT_TIT_5, TitleHeight5);
                cell.UpdateCell(TEXT_TIT_5
                                , UIImage.FromFile("freccia_giu.png"), UIImage.FromFile("MontepulcianoIcon.png"));
                return cell;
            }
            if (row == 8 && !SelFifth)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoTitolo cell = new CustomCellMuseoTitolo(cellIdentifier, false, TEXT_TIT_4, TitleHeight4);
                cell.UpdateCell(TEXT_TIT_5
                                , UIImage.FromFile("freccia_su.png"), UIImage.FromFile("MontepulcianoIcon.png"));
                return cell;
            }
            
            if (row == 1 && SelFirst)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoDescrizione cell = new CustomCellMuseoDescrizione(cellIdentifier, (float)size1.Height,TEXT_OPEN_1,TEXT_OPEN_3,TEXT_PRICE_1,TEXT_VALID,true);
                cell.UpdateCell("Via San Giovanni, 125 - San Gimignano (SI)",
                    NSBundle.MainBundle.LocalizedString("Musei_Apertura", "Opening Time:", null),
                    NSBundle.MainBundle.LocalizedString("Musei_Prezzo", "Price:", null),
                    NSBundle.MainBundle.LocalizedString("Musei_Info", "Informations:", null),
                    "tel. +39 0577 940526",
                    TEXT_VALID);
                return cell;
            }
            if (row == 1 && !SelFirst)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoDescrizione cell = new CustomCellMuseoDescrizione(cellIdentifier, 0, TEXT_OPEN_1, TEXT_OPEN_3, TEXT_PRICE_1,TEXT_VALID, true);
                return cell;
            }
            if (row == 3 && SelSecond)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoDescrizione cell = new CustomCellMuseoDescrizione(cellIdentifier, (float)size1.Height, TEXT_OPEN_1, TEXT_OPEN_3, TEXT_PRICE_2, TEXT_VALID, false);
                cell.UpdateCell("Piazza XX Settembre, 3 - Volterra (PI)",
                    NSBundle.MainBundle.LocalizedString("Musei_Apertura", "Opening Time:", null),
                    NSBundle.MainBundle.LocalizedString("Musei_Prezzo", "Price:", null),
                    NSBundle.MainBundle.LocalizedString("Musei_Info", "Informations:", null),
                    "tel. + 39 0588 80501");
                return cell;
            }
            if (row == 3 && !SelSecond)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoDescrizione cell = new CustomCellMuseoDescrizione(cellIdentifier, 0, TEXT_OPEN_1, TEXT_OPEN_3, TEXT_PRICE_2, TEXT_VALID, false);
                return cell;
            }

            if (row == 5 && SelThird)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoDescrizione cell = new CustomCellMuseoDescrizione(cellIdentifier, (float)size1.Height, TEXT_OPEN_1, TEXT_OPEN_3, TEXT_PRICE_1, TEXT_VALID, false);
                cell.UpdateCell("Vicolo del Bargello, 6 - Siena (SI)",
                    NSBundle.MainBundle.LocalizedString("Musei_Apertura", "Opening Time:", null),
                    NSBundle.MainBundle.LocalizedString("Musei_Prezzo", "Price:", null),
                    NSBundle.MainBundle.LocalizedString("Musei_Info", "Informations:", null),
                    "tel. +39 0577 41999");
                return cell;
            }
            if (row == 5 && !SelThird)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoDescrizione cell = new CustomCellMuseoDescrizione(cellIdentifier, 0, TEXT_OPEN_1, TEXT_OPEN_3, TEXT_PRICE_2, TEXT_VALID, false);
                return cell;
            }

            if (row == 7 && SelFourth)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoDescrizione cell = new CustomCellMuseoDescrizione(cellIdentifier, (float)size1.Height, TEXT_OPEN_1, TEXT_OPEN_3, TEXT_PRICE_1, TEXT_VALID, false);
                cell.UpdateCell("Via Fillungo, 209 - Lucca (LU)",
                    NSBundle.MainBundle.LocalizedString("Musei_Apertura", "Opening Time:", null),
                    NSBundle.MainBundle.LocalizedString("Musei_Prezzo", "Price:", null),
                    NSBundle.MainBundle.LocalizedString("Musei_Info", "Informations:", null),
                    "tel. +39 0583 955788");
                return cell;
            }
            if (row == 7 && !SelFourth)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoDescrizione cell = new CustomCellMuseoDescrizione(cellIdentifier, 0, TEXT_OPEN_1, TEXT_OPEN_3, TEXT_PRICE_2, TEXT_VALID, false);
                return cell;
            }

            if (row == 9 && SelFifth)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoDescrizione cell = new CustomCellMuseoDescrizione(cellIdentifier, (float)size1.Height, TEXT_OPEN_1, TEXT_OPEN_3, TEXT_PRICE_2, TEXT_VALID, false);
                cell.UpdateCell("Via San Donato, 12 - Montepulciano (SI)",
                    NSBundle.MainBundle.LocalizedString("Musei_Apertura", "Opening Time:", null),
                    NSBundle.MainBundle.LocalizedString("Musei_Prezzo", "Price:", null),
                    NSBundle.MainBundle.LocalizedString("Musei_Info", "Informations:", null),
                    "tel. + 39 0578 757001");
                return cell;
            }
            if (row == 9 && !SelFifth)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMuseoDescrizione cell = new CustomCellMuseoDescrizione(cellIdentifier, 0, TEXT_OPEN_1, TEXT_OPEN_3, TEXT_PRICE_2, TEXT_VALID, false);
                return cell;
            }

            return null;

        }
    }
}
