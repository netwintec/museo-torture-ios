using Xamarin.iOS.iCarouselBinding;
using CoreGraphics;
using Foundation;
using Xamarin.SWRevealViewController;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using UIKit;

namespace MuseoDelleTorture
{
    partial class MostreTempController : UIViewController
    {

        private UIScrollView scrollView;
        public int yy = 0;
        public iCarousel carousel;
        UIImageView pallino1, pallino2, pallino3;
        public MostreTempController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += (sender, e) => this.RevealViewController().RevealToggleAnimated(true);
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
            }
            UIViewController MostraPage = storyboard.InstantiateViewController("MostraPage");
            UIViewController HomePageC = storyboard.InstantiateViewController("HomePage");

            AppDelegate.Instance.ScanBeIn = true;

            BackIcon.Clicked += (object sender, EventArgs e) =>
            {
                if (HomePage.Instance.isMostra)
                    this.RevealViewController().PushFrontViewController(MostraPage, true);
                if (HomePage.Instance.isHome)
                    this.RevealViewController().PushFrontViewController(HomePageC, true);

            };


            scrollView = new UIScrollView(new CGRect(0, 0, View.Frame.Width, ContentView.Frame.Height));

            UIImageView TopImage = new UIImageView(new CGRect(0, 0, View.Frame.Width, 50));
            TopImage.Image = UIImage.FromFile("mostretemp_sfondo.jpg");

            UIView TopView = new UIView(new CGRect(0, 0, View.Frame.Width, 50));
            TopView.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 150);

            UILabel TopLabel = new UILabel(new CGRect(0, 10, View.Frame.Width, 40));
            TopLabel.Font = UIFont.FromName("DaunPenh", 32);
            TopLabel.TextAlignment = UITextAlignment.Center;
            TopLabel.TextColor = UIColor.White;
            //TopLabel.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 150);
            TopLabel.TextRectForBounds(new CGRect(0, 10, View.Frame.Width, 40), 1);
            TopLabel.Text = NSBundle.MainBundle.LocalizedString("Menu_MostreTemp", "TEMPORARY EXHIBITION", null);

            yy += 50;

            UILabel MostraTitolo = new UILabel(new CGRect(0, yy + 10, View.Frame.Width, 40));
            MostraTitolo.Font = UIFont.FromName("DaunPenh", 22);
            MostraTitolo.TextAlignment = UITextAlignment.Center;
            MostraTitolo.TextColor = UIColor.White;
            MostraTitolo.Text = "SAN DIEGO - CALIFORNIA (U.S.A.)";

            yy += 50;

            List<string> app = new List<string>();
            app.Add("gioca_icon.png");
            app.Add("imusei_icon.png");
            app.Add("lamostra_icon.png");

            carousel = new iCarousel();
            carousel.Frame = new CGRect((ContentView.Frame.Width / 2) - 100, yy + 15, 200, 200);
            carousel.Type = iCarouselType.CoverFlow2;
            carousel.DataSource = new CarouselDataSource(app.Count, 200f, 200f, app, 1);
            carousel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
            //carousel.CurrentItemIndexChanged += Carousel_CurrentItemIndexChanged;

            yy += 215;


            pallino1 = new UIImageView(new CGRect((ContentView.Frame.Width / 2) - 12.5, yy + 5, 5, 5));
            pallino1.Image = UIImage.FromFile("pallinorosso.png");

            pallino2 = new UIImageView(new CGRect((ContentView.Frame.Width / 2) - 2.5, yy + 5, 5, 5));
            pallino2.Image = UIImage.FromFile("pallinobianco.png");

            pallino3 = new UIImageView(new CGRect((ContentView.Frame.Width / 2) + 7.5, yy + 5, 5, 5));
            pallino3.Image = UIImage.FromFile("pallinobianco.png");

            yy += 10;

            UIView SeparatorView = new UIView(new CGRect(0, yy + 10, View.Frame.Width, 4));
            SeparatorView.BackgroundColor = UIColor.FromRGB(153, 19, 27);

            yy += 14;

            UILabel MostraNome = new UILabel(new CGRect(40, yy + 10, View.Frame.Width - 80, 60));
            MostraNome.Font = UIFont.FromName("DaunPenh", 22);
            MostraNome.TextAlignment = UITextAlignment.Left;
            MostraNome.TextColor = UIColor.White;
            MostraNome.Lines = 2;
            MostraNome.Text = "MUSEUM OF MAN\n14/07/2012 - 10/01/2016";

            yy += 70;


            var sizeDesr = UIStringDrawing.StringSize("Survivor of Torture International  - Joan B. Kroc  Institute for Peace and Justice - California Western School of Law", UIFont.FromName("MyriadPro-Regular", 16), new CGSize(ContentView.Frame.Width - 80, 2000));
            Console.WriteLine("2:" + sizeDesr.Width + " " + sizeDesr.Height);
            UILabel MostraDescr = new UILabel(new CGRect(40, yy + 10, View.Frame.Width - 80, sizeDesr.Height));
            MostraDescr.Font = UIFont.FromName("MyriadPro-Regular", 16);
            MostraDescr.TextAlignment = UITextAlignment.Left;
            MostraDescr.TextColor = UIColor.White;
            MostraDescr.Lines = 0;
            MostraDescr.Text = "Survivor of Torture International  - Joan B. Kroc  Institute for Peace and Justice - California Western School of Law";

            yy += (int)(sizeDesr.Height + 10);


            var size1 = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("MostraTemp_Indirizzo", "Address:", null), UIFont.FromName("MyriadPro-Bold", 17), new CGSize(2000, 2000));
            Console.WriteLine("3:" + size1.Width + " " + size1.Height);

            UILabel Address = new UILabel(new CGRect(40, yy + 15, size1.Width, size1.Height));
            Address.Font = UIFont.FromName("MyriadPro-Bold", 17);
            Address.TextAlignment = UITextAlignment.Left;
            Address.TextColor = UIColor.White;
            Address.Lines = 1;
            Address.Text = NSBundle.MainBundle.LocalizedString("MostraTemp_Indirizzo", "Address:", null);

            var size2 = UIStringDrawing.StringSize("Prova", UIFont.FromName("MyriadPro-Regular", 17), new CGSize(ContentView.Frame.Width - 90 - size1.Width, 2000));
            Console.WriteLine("4:" + size2.Width + " " + size2.Height);

            UILabel AddressText = new UILabel(new CGRect(size1.Width + 50, yy + 15, size2.Width, size2.Height));
            AddressText.Font = UIFont.FromName("MyriadPro-Regular", 17);
            AddressText.TextAlignment = UITextAlignment.Left;
            AddressText.TextColor = UIColor.White;
            AddressText.Lines = 1;
            AddressText.Text = "Prova";

            if (size1.Height >= size2.Height)
                yy += (int)(size1.Height + 15);
            else
                yy += (int)(size2.Height + 15);

            size1 = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("MostraTemp_Apertura", "Opening Time:", null), UIFont.FromName("MyriadPro-Bold", 17), new CGSize(2000, 2000));
            Console.WriteLine("3:" + size1.Width + " " + size1.Height);

            UILabel Orario = new UILabel(new CGRect(40, yy + 15, size1.Width, size1.Height));
            Orario.Font = UIFont.FromName("MyriadPro-Bold", 17);
            Orario.TextAlignment = UITextAlignment.Left;
            Orario.TextColor = UIColor.White;
            Orario.Lines = 1;
            Orario.Text = NSBundle.MainBundle.LocalizedString("MostraTemp_Apertura", "Opening Time:", null);

            size2 = UIStringDrawing.StringSize("Prova", UIFont.FromName("MyriadPro-Regular", 17), new CGSize(ContentView.Frame.Width - 90 - size1.Width, 2000));
            Console.WriteLine("4:" + size2.Width + " " + size2.Height);

            UILabel OrarioText = new UILabel(new CGRect(size1.Width + 50, yy + 15, size2.Width, size2.Height));
            OrarioText.Font = UIFont.FromName("MyriadPro-Regular", 17);
            OrarioText.TextAlignment = UITextAlignment.Left;
            OrarioText.TextColor = UIColor.White;
            OrarioText.Lines = 1;
            OrarioText.Text = "Prova";

            if (size1.Height >= size2.Height)
                yy += (int)(size1.Height + 15);
            else
                yy += (int)(size2.Height + 15);

            size1 = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("MostraTemp_Prezzo", "Price:", null), UIFont.FromName("MyriadPro-Bold", 17), new CGSize(2000, 2000));
            Console.WriteLine("3:" + size1.Width + " " + size1.Height);

            UILabel Prezzo = new UILabel(new CGRect(40, yy + 15, size1.Width, size1.Height));
            Prezzo.Font = UIFont.FromName("MyriadPro-Bold", 17);
            Prezzo.TextAlignment = UITextAlignment.Left;
            Prezzo.TextColor = UIColor.White;
            Prezzo.Lines = 1;
            Prezzo.Text = NSBundle.MainBundle.LocalizedString("MostraTemp_Prezzo", "Price:", null);

            size2 = UIStringDrawing.StringSize("Prova", UIFont.FromName("MyriadPro-Regular", 17), new CGSize(ContentView.Frame.Width - 90 - size1.Width, 2000));
            Console.WriteLine("4:" + size2.Width + " " + size2.Height);

            UILabel PrezzoText = new UILabel(new CGRect(size1.Width + 50, yy + 15, size2.Width, size2.Height));
            PrezzoText.Font = UIFont.FromName("MyriadPro-Regular", 17);
            PrezzoText.TextAlignment = UITextAlignment.Left;
            PrezzoText.TextColor = UIColor.White;
            PrezzoText.Lines = 1;
            PrezzoText.Text = "Prova";

            if (size1.Height >= size2.Height)
                yy += (int)(size1.Height + 15);
            else
                yy += (int)(size2.Height + 15);

            size1 = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("MostraTemp_Info", "Information:", null), UIFont.FromName("MyriadPro-Bold", 17), new CGSize(2000, 2000));
            Console.WriteLine("3:" + size1.Width + " " + size1.Height);

            UILabel Info = new UILabel(new CGRect(40, yy + 15, size1.Width, size1.Height));
            Info.Font = UIFont.FromName("MyriadPro-Bold", 17);
            Info.TextAlignment = UITextAlignment.Left;
            Info.TextColor = UIColor.White;
            Info.Lines = 1;
            Info.Text = NSBundle.MainBundle.LocalizedString("MostraTemp_Info", "Information:", null);

            size2 = UIStringDrawing.StringSize("Prova", UIFont.FromName("MyriadPro-Regular", 17), new CGSize(ContentView.Frame.Width - 90 - size1.Width, 2000));
            Console.WriteLine("4:" + size2.Width + " " + size2.Height);

            UILabel InfoText = new UILabel(new CGRect(size1.Width + 50, yy + 15, size2.Width, size2.Height));
            InfoText.Font = UIFont.FromName("MyriadPro-Regular", 17);
            InfoText.TextAlignment = UITextAlignment.Left;
            InfoText.TextColor = UIColor.White;
            InfoText.Lines = 1;
            InfoText.Text = "Prova";

            if (size1.Height >= size2.Height)
                yy += (int)(size1.Height + 15);
            else
                yy += (int)(size2.Height + 15);

            scrollView.Add(TopImage);
            scrollView.Add(TopView);
            scrollView.Add(TopLabel);
            scrollView.Add(MostraTitolo);
            scrollView.Add(carousel);
            scrollView.Add(pallino1);
            scrollView.Add(pallino2);
            scrollView.Add(pallino3);
            scrollView.Add(SeparatorView);
            scrollView.Add(MostraNome);
            scrollView.Add(MostraDescr);
            scrollView.Add(Address);
            scrollView.Add(AddressText);
            scrollView.Add(Orario);
            scrollView.Add(OrarioText);
            scrollView.Add(Prezzo);
            scrollView.Add(PrezzoText);
            scrollView.Add(Info);
            scrollView.Add(InfoText);

            scrollView.ContentSize = new CGSize(View.Frame.Width, yy + 15);

            ContentView.Add(scrollView);

        }

        private void Carousel_CurrentItemIndexChanged(object sender, EventArgs e)
        {
            int index = (int)carousel.CurrentItemIndex;
            ChangePallino(index);
        }

        public void ChangePallino(int i)
        {
            if (i == 0)
            {
                pallino1.Image = UIImage.FromFile("pallinorosso.png");
                pallino2.Image = UIImage.FromFile("pallinobianco.png");
                pallino3.Image = UIImage.FromFile("pallinobianco.png");
            }
            if (i == 1)
            {
                pallino2.Image = UIImage.FromFile("pallinorosso.png");
                pallino1.Image = UIImage.FromFile("pallinobianco.png");
                pallino3.Image = UIImage.FromFile("pallinobianco.png");
            }
            if (i == 2)
            {
                pallino3.Image = UIImage.FromFile("pallinorosso.png");
                pallino2.Image = UIImage.FromFile("pallinobianco.png");
                pallino1.Image = UIImage.FromFile("pallinobianco.png");
            }
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }
    }

}
