using Foundation;
using Xamarin.SWRevealViewController;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MuseoDelleTorture
{
	partial class CosaVedereMenuController : UITableViewController
	{
		public CosaVedereMenuController (IntPtr handle) : base (handle)
		{
		}
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            var tableView = View as UITableView;

            if (tableView != null)
            {
                tableView.Source = new TableSourceCitta();
                tableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
                tableView.SeparatorColor = UIColor.Clear;
                tableView.BackgroundColor = UIColor.Black;
            }
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }

            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }
    }
}
