using CoreGraphics;
using CoreLocation;
using Foundation;
using Google.Maps;
using Xamarin.SWRevealViewController;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MuseoDelleTorture
{
	partial class MuseiController : UIViewController
	{

        MapView mapView;
        CLLocationCoordinate2D Coord1 = new CLLocationCoordinate2D(43.464941, 11.042679); //san giminiano 1
        CLLocationCoordinate2D Coord1_2 = new CLLocationCoordinate2D(43.465539, 11.042644); //san giminiano 2
        CLLocationCoordinate2D Coord2 = new CLLocationCoordinate2D(43.401470, 10.863198); //volterra
        CLLocationCoordinate2D Coord3 = new CLLocationCoordinate2D(43.317884, 11.330948); //siena
        CLLocationCoordinate2D Coord4 = new CLLocationCoordinate2D(43.846435, 10.506773); //lucca
        CLLocationCoordinate2D Coord5 = new CLLocationCoordinate2D(43.092201, 11.780358); //montepulciano

        int yy=0;

        UITableView table;
        public MuseiController (IntPtr handle) : base (handle)
		{
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += (sender, e) => this.RevealViewController().RevealToggleAnimated(true);
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            AppDelegate.Instance.ScanBeIn = true;

            UIImageView TopImage = new UIImageView(new CGRect(0, 0, View.Frame.Width, 50));
            TopImage.Image = UIImage.FromFile("musei_sfondo.jpg");

            UIView TopView = new UIView(new CGRect(0, 0, View.Frame.Width, 50));
            TopView.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 150);

            UILabel TopLabel = new UILabel(new CGRect(0, 10, View.Frame.Width, 40));
            TopLabel.Font = UIFont.FromName("DaunPenh", 32);
            TopLabel.TextAlignment = UITextAlignment.Center;
            TopLabel.TextColor = UIColor.White;
            //TopLabel.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 150);
            TopLabel.TextRectForBounds(new CGRect(0, 10, View.Frame.Width, 40), 1);
            TopLabel.Text = NSBundle.MainBundle.LocalizedString("Menu_Musei", "MUSEUM", null);

            yy += 50;

            CameraPosition camera = CameraPosition.FromCamera(43.472449, 11.250806, 7);

            mapView = MapView.FromCamera(new CGRect(0,yy,View.Frame.Width, (ContentView.Frame.Height/2)- 64), camera);
            mapView.MapType = MapViewType.Normal;
            mapView.Settings.ZoomGestures = true;
            mapView.Settings.CompassButton = true;
            mapView.Settings.MyLocationButton = false;
            mapView.MyLocationEnabled = false;

            var MarkerLucca = new Marker()
            {
                Position = Coord4,
                Icon = UIImage.FromFile("marker_lucca.png"),
                Map = mapView

            };

            var MarkerSiena = new Marker()
            {
                Position = Coord3,
                Icon = UIImage.FromFile("marker_siena.png"),
                Map = mapView

            };

            var MarkerVolterra = new Marker()
            {
                Position = Coord2,
                Icon = UIImage.FromFile("marker_volterra.png"),
                Map = mapView

            };

            var MarkerMonteP = new Marker()
            {
                Position = Coord5,
                Icon = UIImage.FromFile("marker_montepulciano.png"),
                Map = mapView

            };
            var MarkerSanG = new Marker()
            {
                Position = Coord1,
                Icon = UIImage.FromFile("marker_san_gimignano.png"),
                Map = mapView

            };
            var MarkerSanG2 = new Marker()
            {
                Position = Coord1_2,
                Icon = UIImage.FromFile("marker_san_gimignano.png"),
                Map = mapView

            };

            yy += (int)((ContentView.Frame.Height / 2) - 64);

            UIView SeparatorView = new UIView(new CGRect(0, yy, View.Frame.Width, 4));
            SeparatorView.BackgroundColor = UIColor.FromRGB(153, 19, 27);

            yy += 4;

            table = new UITableView(new CGRect(30, yy + 15, ContentView.Frame.Width - 60, ContentView.Frame.Height/2 - 25 ));
            table.Source = new TableSourceMusei((float)ContentView.Frame.Width - 60, this);
            table.SeparatorColor = UIColor.Clear;
            table.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            table.BackgroundColor = UIColor.Black;

            
            //mapView.SelectedMarker = xamMarker;
            ContentView.Add(TopImage);
            ContentView.Add(TopView);
            ContentView.Add(TopLabel);
            ContentView.Add(mapView);
            ContentView.Add(SeparatorView);
            ContentView.Add(table);

        }

        public void ChangePosition(int i) {

            if (i == 0)
            {
                CameraPosition camera = CameraPosition.FromCamera(Coord1, 15);
                mapView.Camera = camera;
            }
            if (i == 1)
            {
                CameraPosition camera = CameraPosition.FromCamera(Coord2, 15);
                mapView.Camera = camera;
            }
            if (i == 2)
            {
                CameraPosition camera = CameraPosition.FromCamera(Coord3, 15);
                mapView.Camera = camera;
            }
            if (i == 3)
            {
                CameraPosition camera = CameraPosition.FromCamera(Coord4, 15);
                mapView.Camera = camera;
            }
            if (i == 4)
            {
                CameraPosition camera = CameraPosition.FromCamera(Coord5, 15);
                mapView.Camera = camera;
            }
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }
    }
}
