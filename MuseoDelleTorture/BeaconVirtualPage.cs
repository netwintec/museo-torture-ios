using AVFoundation;
using CoreGraphics;
using CoreMedia;
using Foundation;
using MediaPlayer;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using UIKit;
using ZXing.Mobile;

namespace MuseoDelleTorture
{
    partial class BeaconVirtualPage : UIViewController
    {
        BeaconInfo oggetto;

        MobileBarcodeScanner scanner;

        bool scanOpen = false;
        bool scanFineOpen = false;

        public System.Timers.Timer timer;

        UIViewController RealtaP;

        AVAudioPlayer audioPlayer;
        bool IsPlaying;

        MPMoviePlayerController moviePlayer;
        bool isPlay;

        AVPlayer _player;
        UIView Trailer;

        public BeaconVirtualPage(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            oggetto = BeaconStanzaHome.Instance.ObjectSelected;

            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
            }
            RealtaP = storyboard.InstantiateViewController("realta");

            var BackTrailer = new UIView(new CGRect(0, 0, ContentView.Frame.Width, ContentView.Frame.Height));
            BackTrailer.BackgroundColor = UIColor.Black;

            ContentView.Add(BackTrailer);


            //     RA/Donzella/index.html
            var split = oggetto.folder3D.Split('/');
            //string videostring = "Resources/"+split[0] + "/" + split[1] + "/video.mp4";
            string videostring = "video" + split[1] + ".mp4";

            Trailer = new UIView(new CGRect(ContentView.Frame.Width / 2 - ContentView.Frame.Height / 2, ContentView.Frame.Height / 2 - ContentView.Frame.Width / 2, ContentView.Frame.Height, ContentView.Frame.Width));
            Trailer.BackgroundColor = UIColor.Black;

            ContentView.Add(Trailer);

            var _asset = AVAsset.FromUrl(NSUrl.FromFilename(videostring));
            var _playerItem = new AVPlayerItem(_asset);

            _player = new AVPlayer(_playerItem);

            _player.ActionAtItemEnd = AVPlayerActionAtItemEnd.None;
            //var videoEndNotificationToken = NSNotificationCenter.DefaultCenter.AddObserver(AVPlayerItem.DidPlayToEndTimeNotification, VideoDidFinishPlaying, _player);

            NSNotificationCenter.DefaultCenter.AddObserver(AVPlayerItem.DidPlayToEndTimeNotification, (notify) =>
            {
                this.PresentModalViewController(RealtaP, true);
                Console.WriteLine("realta");
                notify.Dispose();
            });

            var _playerLayer = AVPlayerLayer.FromPlayer(_player);
            _playerLayer.Frame = new CGRect(0, 0, ContentView.Frame.Height - 0, ContentView.Frame.Width - 0);

            Trailer.Layer.AddSublayer(_playerLayer);

            Trailer.Layer.AnchorPoint = new CGPoint(0.5, 0.5);
            Trailer.Layer.AffineTransform = CGAffineTransform.MakeRotation((float)Math.PI / 2);

            Trailer.Alpha = 0;
            //moviePlayer.
            //startVideo();

            scanner = new MobileBarcodeScanner(this);
            startCamera();

        }

        private void VideoDidFinishPlaying(NSNotification obj)
        {
            InvokeOnMainThread(() =>
            {

                //this.DismissModalViewController(true);
                this.PresentModalViewController(RealtaP, true);
                Console.WriteLine("realta");
            });
        }

        public async void startCamera()
        {
            scanner.UseCustomOverlay = false;

            var options = new MobileBarcodeScanningOptions();
            options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
                    ZXing.BarcodeFormat.QR_CODE
                };
            //We can customize the top and bottom text of the default overlay
            //scanner.TopText = "Tieni la camera davanti al barcode da scansionare";
            //scanner.BottomText = "Attendi che il barcode venga scansionato!";

            scanOpen = true;

            //Start scanning

            ZXing.Result result = null;

            new System.Threading.Thread(new System.Threading.ThreadStart(delegate
            {
                while (result == null)
                {
                    Console.WriteLine("AF");
                    scanner.AutoFocus();
                    System.Threading.Thread.Sleep(2000);
                }
            })).Start();

            result = await scanner.Scan(options);
            //result = await scanner.Scan();

            if (result != null)
                HandleScanResult(result, 1);
            else
            {
                scanOpen = false;

                new System.Threading.Thread(new System.Threading.ThreadStart(() =>
                {

                    System.Threading.Thread.Sleep(500);
                    InvokeOnMainThread(() =>
                    {

                        //this.DismissModalViewController(true);
                        this.PresentModalViewController(RealtaP, true);
                        Console.WriteLine("realta");
                    });
                })).Start();

            }

        }

        void HandleScanResult(ZXing.Result result, int Indice)
        {

            scanOpen = false;

            string msg = "";

            if (result != null && !string.IsNullOrEmpty(result.Text))
            {

                InvokeOnMainThread(() =>
                {
                    string txt = result.Text;
                    Console.WriteLine("VALUE:" + txt);
                    if (txt == "MuseoDelleTorture")
                    {
                        startVideo();
                    }
                    else
                    {
                        startCamera();
                    }
                });


            }
            else
            {
                new System.Threading.Thread(new System.Threading.ThreadStart(() =>
                {

                    System.Threading.Thread.Sleep(500);
                    InvokeOnMainThread(() =>
                    {

                        //this.DismissModalViewController(true);
                        this.PresentModalViewController(RealtaP, true);
                        Console.WriteLine("realta");
                    });
                })).Start();
                //this.RunOnUiThread(() => Toast.MakeText(this, msg, ToastLength.Short).Show());
            }


        }

        public void startVideo()
        {

            Trailer.Alpha = 1;
            _player.Seek(CMTime.Zero);
            _player.Play();

        }

        public override void ViewDidDisappear(bool animated)
        {
            /*if (architectView.IsRunning) {
                architectView.Stop();
                architectView.RemoveFromSuperview();
                architectView.Dispose();
                architectView = null;
                //Delete(architectView);
            }*/

            base.ViewDidDisappear(animated);
        }
    }
}
