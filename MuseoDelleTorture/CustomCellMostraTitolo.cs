﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace MuseoDelleTorture
{
    class CustomCellMostraTitolo : UITableViewCell
    {
        UILabel titolo;
        UIView separator;
        UIImageView freccia;

        float HeightCell;
        public CustomCellMostraTitolo(NSString cellId, bool selected,string tit,float h) : base(UITableViewCellStyle.Default, cellId)
        {
            HeightCell = h;

            SelectionStyle = UITableViewCellSelectionStyle.None;
            freccia = new UIImageView();
            separator = new UIView();
            separator.BackgroundColor = UIColor.White;
            titolo = new UILabel()
            {
                Font = UIFont.FromName("DaunPenh", 20f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                //Lines = 2,
                BackgroundColor = UIColor.Clear
            };

            //Console.WriteLine("/***** "+(ContentView.Frame.Width - 95)+"!"+tit+" *****\\");
            if (HeightCell > 45)
            {
                titolo.Lines = 2;
            }
            else
            {
                titolo.Lines = 1;
            }

            /*
            var size1 = UIStringDrawing.StringSize(tit, UIFont.FromName("DaunPenh", 20), new CGSize(ContentView.Frame.Width - 95,2000));
            Console.WriteLine("2:"+ size1.Width+ " "+size1.Height);
            
            var size2 = UIStringDrawing.StringSize(tit, UIFont.FromName("DaunPenh", 18));
            Console.WriteLine("3:"+ size2.Width);

            var size3 = UIStringDrawing.StringSize(tit, UIFont.FromName("DaunPenh", 17));
            Console.WriteLine("4:" + size3.Width);

            var size4 = UIStringDrawing.StringSize(tit, UIFont.FromName("DaunPenh", 16));
            Console.WriteLine("5:" + size4.Width);

            var size5 = UIStringDrawing.StringSize(tit, UIFont.FromName("DaunPenh", 15));
            Console.WriteLine("6:" + size4.Width);

            if (size.Width > ContentView.Frame.Width - 95)
            {
                if (size1.Width > ContentView.Frame.Width - 95)
                {
                    if (size2.Width > ContentView.Frame.Width - 95)
                    {
                        if (size3.Width > ContentView.Frame.Width - 95)
                        {
                            if (size4.Width > ContentView.Frame.Width - 95)
                            {
                                if (size5.Width > ContentView.Frame.Width - 95)
                                {
                                    Console.WriteLine("size:14");
                                    titolo.Font = UIFont.FromName("DaunPenh", 14f);
                                }
                                else
                                {
                                    Console.WriteLine("size:15");
                                    titolo.Font = UIFont.FromName("DaunPenh", 15f);
                                }
                            }
                            else
                            {
                                Console.WriteLine("size:16");
                                titolo.Font = UIFont.FromName("DaunPenh", 16f);
                            }
                        }
                        else
                        {
                            Console.WriteLine("size:17");
                            titolo.Font = UIFont.FromName("DaunPenh", 17f);
                        }
                    }
                    else
                    {
                        Console.WriteLine("size:18");
                        titolo.Font = UIFont.FromName("DaunPenh", 18f);
                    }
                }
                else
                {
                    Console.WriteLine("size:19");
                    titolo.Font = UIFont.FromName("DaunPenh", 19f);
                }
            }
            else
            {
                Console.WriteLine("size:20");
                titolo.Font = UIFont.FromName("DaunPenh", 20f);
            }
            */
            if (selected)
            {
                titolo.TextColor = UIColor.FromRGB(152, 20, 27);
            }
            else
            {
                titolo.TextColor = UIColor.White;
            }

            ContentView.BackgroundColor = UIColor.Black;

            ContentView.AddSubviews(new UIView[] { titolo, freccia, separator });

        }
        public void UpdateCell(string tit, UIImage imm)
        {
            titolo.Text = tit;
            freccia.Image = imm;
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            if (HeightCell > 45)
            {
                titolo.Frame = new CGRect(0, 7, ContentView.Frame.Width - 25, 60);
                freccia.Frame = new CGRect(ContentView.Frame.Width - 21, 23, 20, 13);
                separator.Frame = new CGRect(0, 68, ContentView.Frame.Width, 1);
            }
            else
            {
                titolo.Frame = new CGRect(0, 7, ContentView.Frame.Width - 25, 30);
                freccia.Frame = new CGRect(ContentView.Frame.Width - 21, 13, 20, 13);
                separator.Frame = new CGRect(0, 38, ContentView.Frame.Width, 1);
            }

        }
    }

}
