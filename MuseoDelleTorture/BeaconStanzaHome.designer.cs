// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MuseoDelleTorture
{
    [Register ("BeaconStanzaHome")]
    partial class BeaconStanzaHome
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem BackItem { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ContentView { get; set; }

        [Action ("BackItem_Activated:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void BackItem_Activated (UIKit.UIBarButtonItem sender);

        void ReleaseDesignerOutlets ()
        {
            if (BackItem != null) {
                BackItem.Dispose ();
                BackItem = null;
            }

            if (ContentView != null) {
                ContentView.Dispose ();
                ContentView = null;
            }
        }
    }
}