﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace MuseoDelleTorture
{
    class CustomCellMostraDescrizione : UITableViewCell
    {
        UILabel descr;
        float height;
        public CustomCellMostraDescrizione(NSString cellId, float h) : base(UITableViewCellStyle.Default, cellId)
        {

            height = h;

            SelectionStyle = UITableViewCellSelectionStyle.None;
            descr = new UILabel()
            {
                Font = UIFont.FromName("MyriadPro-Regular", 14f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                TextColor = UIColor.White,
                Lines = 0,
                BackgroundColor = UIColor.Clear
                
            };

            ContentView.BackgroundColor = UIColor.Black;

            ContentView.AddSubviews(new UIView[] { descr});

        }
        public void UpdateCell(string tit)
        {
            descr.Text = tit;
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            descr.Frame = new CGRect(0, 4, ContentView.Frame.Width, height);

        }
    }

}

