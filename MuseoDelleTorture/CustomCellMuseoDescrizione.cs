﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace MuseoDelleTorture
{
    class CustomCellMuseoDescrizione : UITableViewCell
    {
        UILabel Via,Apertura,AperturaText1,AperturaText2,Prezzo,PrezzoText,Info,InfoText,GimVal;
        float height;

        float widthPrice, widthOpen, widthInfo;

        public string textOpen1, textOpen2, textOpen3, textPrice;

        public CustomCellMuseoDescrizione(NSString cellId, float h,string t1,string t2,string t3,string t4,bool isGim) : base(UITableViewCellStyle.Default, cellId)
        {

            height = h;

            textOpen1 = t1;
            textOpen2 = t2;
            textPrice = t3;
            textOpen3 = t4;

            var size = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Musei_Apertura", "Opening Time:", null), UIFont.FromName("MyriadPro-Bold", 14), new CGSize(2000, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);
            widthOpen = (float)size.Width;

            size = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Musei_Prezzo", "Price:", null), UIFont.FromName("MyriadPro-Bold", 14), new CGSize(2000, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);
            widthPrice = (float)size.Width;

            size = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Musei_Info", "Information:", null), UIFont.FromName("MyriadPro-Bold", 14), new CGSize(2000, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);
            widthInfo = (float)size.Width;


            SelectionStyle = UITableViewCellSelectionStyle.None;
            Via = new UILabel()
            {
                Font = UIFont.FromName("MyriadPro-Bold", 13f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                TextColor = UIColor.White,
                Lines = 0,
                BackgroundColor = UIColor.Clear

            };

            Apertura = new UILabel()
            {
                Font = UIFont.FromName("MyriadPro-Bold", 14f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                TextColor = UIColor.White,
                Lines = 0,
                BackgroundColor = UIColor.Clear

            };

            AperturaText1 = new UILabel()
            {
                Font = UIFont.FromName("MyriadPro-Regular", 14f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                TextColor = UIColor.White,
                Lines = 0,
                BackgroundColor = UIColor.Clear

            };

            AperturaText2 = new UILabel()
            {
                Font = UIFont.FromName("MyriadPro-Regular", 14f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                TextColor = UIColor.White,
                Lines = 0,
                BackgroundColor = UIColor.Clear

            };

            Prezzo = new UILabel()
            {
                Font = UIFont.FromName("MyriadPro-Bold", 14f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                TextColor = UIColor.White,
                Lines = 0,
                BackgroundColor = UIColor.Clear

            };

            PrezzoText = new UILabel()
            {
                Font = UIFont.FromName("MyriadPro-Regular", 14f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                TextColor = UIColor.White,
                Lines = 0,
                BackgroundColor = UIColor.Clear

            };

            Info = new UILabel()
            {
                Font = UIFont.FromName("MyriadPro-Bold", 14f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                TextColor = UIColor.White,
                Lines = 0,
                BackgroundColor = UIColor.Clear

            };

            InfoText = new UILabel()
            {
                Font = UIFont.FromName("MyriadPro-Regular", 14f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                TextColor = UIColor.White,
                Lines = 0,
                BackgroundColor = UIColor.Clear

            };

            GimVal = new UILabel()
            {
                Font = UIFont.FromName("MyriadPro-Regular", 14f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                TextColor = UIColor.White,
                Lines = 0,
                TextAlignment = UITextAlignment.Center,
                BackgroundColor = UIColor.Clear
               

            };

            ContentView.BackgroundColor = UIColor.Black;

            if(isGim)
                ContentView.AddSubviews(new UIView[] { Via, Apertura, AperturaText1, AperturaText2, Prezzo, PrezzoText, Info, InfoText, GimVal });
            else
                ContentView.AddSubviews(new UIView[] { Via, Apertura, AperturaText1, AperturaText2, Prezzo, PrezzoText, Info, InfoText });

        }
        public void UpdateCell(string l, string o, string p, string i, string it)
        {
            Via.Text = l;
            Apertura.Text = o;
            Prezzo.Text = p;
            Info.Text = i;
            InfoText.Text = it;

            AperturaText1.Text = textOpen1;
            AperturaText2.Text = textOpen2;
            PrezzoText.Text = textPrice;

        }
        public void UpdateCell(string l, string o, string p, string i, string it,string val)
        {
            Via.Text = l;
            Apertura.Text = o;
            Prezzo.Text = p;
            Info.Text = i;
            InfoText.Text = it;

            AperturaText1.Text = textOpen1;
            AperturaText2.Text = textOpen2;
            PrezzoText.Text = textPrice;
            GimVal.Text = textOpen3;

        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            var size = UIStringDrawing.StringSize(textOpen1, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(ContentView.Frame.Width - widthOpen - 5, 2000));
            //Console.WriteLine(size.Height + "  " + size.Width);
            float heightOpen1 = (float)(size.Height);

            size = UIStringDrawing.StringSize(textOpen2, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(ContentView.Frame.Width, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);
            float heightOpen2 = (float)(size.Height);

            size = UIStringDrawing.StringSize(textPrice, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(ContentView.Frame.Width - widthPrice - 5, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);
            float heightPrice = (float)(size.Height);

            size = UIStringDrawing.StringSize(textOpen3, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(ContentView.Frame.Width, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);
            float heightVal = (float)(size.Height);

            float yy = 0;

            Via.Frame = new CGRect(0, 5, ContentView.Frame.Width, 25);

            yy += 30;

            Apertura.Frame = new CGRect(0, yy+5, widthOpen, 15);
            AperturaText1.Frame = new CGRect(widthOpen+5, yy + 5, ContentView.Frame.Width-widthOpen-5, heightOpen1);

            if (heightOpen1 > 15)
                yy += heightOpen1 + 5;
            else
                yy += 20;

            AperturaText2.Frame = new CGRect(0, yy, ContentView.Frame.Width, heightOpen2);
            yy += heightOpen2;

            Prezzo.Frame = new CGRect(0, yy + 5, widthPrice, 15);
            PrezzoText.Frame = new CGRect(widthPrice+5, yy + 5, ContentView.Frame.Width - widthPrice - 5, heightPrice);

            if (heightPrice > 15)
                yy += heightOpen1 + 5;
            else
                yy += 20;

            Info.Frame = new CGRect(0, yy + 5, widthOpen, 25);
            InfoText.Frame = new CGRect(widthInfo + 5, yy + 5, ContentView.Frame.Width - widthInfo - 5, 25);

            yy += 30;

            GimVal.Frame = new CGRect(0, yy+5, ContentView.Frame.Width, heightVal);

            //yy += 30;

        }
    }

}


