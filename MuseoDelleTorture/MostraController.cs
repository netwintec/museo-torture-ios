using CoreGraphics;
using Foundation;
using Xamarin.SWRevealViewController;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MuseoDelleTorture
{
	partial class MostraController : UIViewController
	{

        private UIScrollView scrollView;
        public int yy = 0;
        public UITableView table;
        public UIButton BottoneMostreTemp;

        public MostraController (IntPtr handle) : base (handle)
		{
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += (sender, e) => this.RevealViewController().RevealToggleAnimated(true);
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
            }
            UIViewController MostreTempPage = storyboard.InstantiateViewController("MostraTempPage");

            HomePage.Instance.isHome = false;
            HomePage.Instance.isMostra = true;
            AppDelegate.Instance.ScanBeIn = true;

            //scroll.ContentSize = new CGSize(View.Frame.Width, 638);
            scrollView = new UIScrollView(new CGRect(0, 0, View.Frame.Width, ContentView.Frame.Height));

            UIImageView TopImage = new UIImageView(new CGRect(0,0,View.Frame.Width,50));
            TopImage.Image = UIImage.FromFile("mostra_sfondo.jpg");

            CGRect initialFrame = new CGRect(0, 0, View.Frame.Width, 50);
            UIEdgeInsets contentInsets = new UIEdgeInsets(0, 10, 0, 0);
            //GRect paddedFrame = UIKit.r UIEdgeInsetsInsetRect(initialFrame, contentInsets);

            UIView TopView = new UIView(new CGRect(0, 0, View.Frame.Width, 50));
            TopView.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 150);

            UILabel TopLabel = new UILabel(new CGRect(0,10,View.Frame.Width,40));
            TopLabel.Font = UIFont.FromName("DaunPenh",32);
            TopLabel.TextAlignment = UITextAlignment.Center;
            TopLabel.TextColor = UIColor.White;
            //TopLabel.BackgroundColor = UIColor.FromRGBA(0,0,0,200);
            TopLabel.TextRectForBounds(new CGRect(0, 10, View.Frame.Width, 40), 1);
            TopLabel.Text = NSBundle.MainBundle.LocalizedString("Menu_Mostra", "THE EXHIBITION", null);

            yy += 50;

            float heightImg = (float)((View.Frame.Width * 448) / 640);
            UIImageView ContainView = new UIImageView(new CGRect(0,yy, View.Frame.Width, heightImg));
            ContainView.Image = UIImage.FromFile("fotoMostra.png");

            yy += (int)heightImg;

            UIView SeparatorView = new UIView(new CGRect(0, yy, View.Frame.Width, 4));
            SeparatorView.BackgroundColor = UIColor.FromRGB(153, 19, 27);

            yy += 4;

            table = new UITableView(new CGRect(30, yy+5, ContentView.Frame.Width - 60, 165)); 
            table.Source = new TableSourceMostra((float)ContentView.Frame.Width - 60,this);
            table.SeparatorColor = UIColor.Clear;
            table.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            table.BackgroundColor = UIColor.Black;
            table.ScrollEnabled = false;

            yy += 170;

            BottoneMostreTemp = new UIButton(new CGRect((ContentView.Frame.Width / 2) - 125, yy + 20, 250, 40));
            BottoneMostreTemp.SetTitle(NSBundle.MainBundle.LocalizedString("Menu_MostreTemp", "TEMPORARY EXHIBITION", null), UIControlState.Normal);
            BottoneMostreTemp.SetTitleColor(UIColor.White, UIControlState.Normal);
            BottoneMostreTemp.BackgroundColor = UIColor.FromRGB(153, 19, 27);
            BottoneMostreTemp.Font = UIFont.FromName("MyriadPro-Regular", 18);
            BottoneMostreTemp.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(MostreTempPage, true);
            };

            yy += 60;

            //** MOSTRE DISATTIVATE **
            BottoneMostreTemp.Alpha = 0;
            yy -= 60;
            //************************

            scrollView.Add(TopImage);
            scrollView.Add(TopView);
            scrollView.Add(TopLabel);
            scrollView.Add(ContainView);
            scrollView.Add(SeparatorView);
            scrollView.Add(table);
            scrollView.Add(BottoneMostreTemp);

            scrollView.ContentSize = new CGSize(View.Frame.Width, yy + 10);

            ContentView.Add(scrollView);

        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }

        public void addHeight(float h) {

            CGRect frame = table.Frame;
            frame.Height += h;
            table.Frame = frame;

        }

        public void OpenChange(float h)
        {

            CGRect frame = table.Frame;
            frame.Height += h;
            table.Frame = frame;

            CGRect frame2 = BottoneMostreTemp.Frame;
            frame2.Y += h;
            BottoneMostreTemp.Frame = frame2;

            yy += (int)h;

            scrollView.ContentSize = new CGSize(View.Frame.Width, yy + 10);

        }
        public void CloseChange(float h)
        {

            CGRect frame = table.Frame;
            frame.Height -= h;
            table.Frame = frame;

            CGRect frame2 = BottoneMostreTemp.Frame;
            frame2.Y -= h;
            BottoneMostreTemp.Frame = frame2;

            yy -= (int)h;

            scrollView.ContentSize = new CGSize(View.Frame.Width, yy + 10);

        }

    }
}
