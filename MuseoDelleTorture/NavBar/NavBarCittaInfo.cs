using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MuseoDelleTorture
{
	partial class NavBarCittaInfo : UINavigationBar
	{
		public NavBarCittaInfo (IntPtr handle) : base (handle)
		{
		}

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            //Console.WriteLine(Frame.Width);

            UIImageView centerlogo = new UIImageView(new CGRect(this.Frame.Width / 2 - 56.5, 8, 113, 28));
            centerlogo.Image = UIImage.FromFile("logo_toolbar_icon.png");

            UIImageView back = new UIImageView(new CGRect(this.Frame.Width - 31, 12, 21, 20));
            back.Image = UIImage.FromFile("back_icon.png");

            AddSubview(centerlogo);
            AddSubview(back);
        }
    }
}
