using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MuseoDelleTorture
{
	partial class HomeNavBar : UINavigationBar
	{
		public HomeNavBar (IntPtr handle) : base (handle)
		{
		}

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            //Console.WriteLine(Frame.Width);

            UIImageView centerlogo = new UIImageView(new CGRect(this.Frame.Width / 2 - 56.5, 8, 113, 28));
            centerlogo.Image = UIImage.FromFile("logo_toolbar_icon.png");

            UIImageView Menu = new UIImageView(new CGRect(10, 12, 27, 20));
            Menu.Image = UIImage.FromFile("menu_icon.png");

            AddSubview(centerlogo);
            AddSubview(Menu);
        }
    }
}
