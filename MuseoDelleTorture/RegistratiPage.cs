using Xamarin.iOS.iCarouselBinding;
using CoreGraphics;
using Facebook.CoreKit;
using Facebook.LoginKit;
using Foundation;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using UIKit;

namespace MuseoDelleTorture
{
    partial class RegistratiPage : UIViewController
    {

        private LoginButton loginFbButton;
        private UIScrollView scrollView;
        UITextField Nome, Cognome, EMail, Password, RepeatPassword, Citta, Paese;
        List<string> readPermissions = new List<string> { "public_profile", "email" };
        int yy = 0;
        nfloat ViewWidht;
        int SystemVersion;
        bool flagNome, flagCognome, flagEMail, flagPassword, flagRP, flagCitta, flagPaese;
        iCarousel carousel;
        UIImageView pallino1, pallino2, pallino3, pallino4;

        public RegistratiPage(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            ViewWidht = View.Frame.Width;

            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
            }

            Profile.Notifications.ObserveDidChange((sender, e) =>
            {

                if (e.NewProfile == null)
                    return;

                //nameLabel.Text = e.NewProfile.Name;
            });

            // Set the Read and Publish permissions you want to get
            loginFbButton = new LoginButton(new CGRect(0, 0, 120, 40))//AccediText.Frame.X+(AccediText.Frame.Width/2) - 60, AccediText.Frame.Y, 120, 40))
            {
                LoginBehavior = LoginBehavior.Native,
                ReadPermissions = readPermissions.ToArray()

            };

            LoginManager loginManager = new LoginManager();


            // Handle actions once the user is logged in
            loginFbButton.Completed += (sender, e) =>
            {
                if (e.Error != null)
                {
                    var adErr = new UIAlertView("Login Non Riuscito", "E-Mail o Password Errata", null, "OK", null);
                    adErr.Show();
                    // Handle if there was an error
                }

                if (e.Result.IsCancelled)
                {
                    // Handle if the user cancelled the login request
                }

                if (e.Result.Token.TokenString != null)
                {

                    Console.WriteLine("Token:" + e.Result.Token.TokenString);

                    UtilityLoginManager loginManagerFB = new UtilityLoginManager();
                    List<string> Response = loginManagerFB.LoginFB(e.Result.Token.TokenString);

                    if (Response[0] == "Errore")
                    {
                        loginManager.LogOut();

                        var adErr = new UIAlertView("Errore di rete", "Login non riuscito provate pi� tardi", null, "OK", null);
                        adErr.Show();
                    }

                    if (Response[0] == "SUCCESS")
                    {
                        Console.WriteLine("SUCCESS");
                        //****** SALVA TOKEN ******

                        Console.WriteLine(Response[1]);

                        NSUserDefaults.StandardUserDefaults.SetString(Response[1], "TokenMuseoN4U");
                        //NSUserDefaults.StandardUserDefaults.SetString(e.Result.Token.TokenString, "TokenLovieFB");

                        UIViewController lp = storyboard.InstantiateViewController("SWController");
                        this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                        this.PresentModalViewController(lp, true);
                        //loginButton.ClearPermissions();
                        //loginButton.SetPublishPermissions("publish_actions");

                    }

                }

                // Handle your successful login
            };




            //RegisterButton.Layer.CornerRadius = 7;

            scrollView = new UIScrollView(new CGRect(0, 0, View.Frame.Width, ContentView.Frame.Height));


            UIImageView imageView = new UIImageView(new CGRect(ViewWidht / 2 - 75, yy + 20, 150, 38));
            imageView.Image = UIImage.FromFile("logo_pagina_iniziale.png");
            yy += 58;

            UILabel Registrati = new UILabel(new CGRect(ViewWidht / 2 - 45, yy + 15, 90, 25));
            Registrati.Text = NSBundle.MainBundle.LocalizedString("LogReg_Accedi", "Login", null);
            Registrati.TextAlignment = UITextAlignment.Center;
            Registrati.TextColor = UIColor.White;
            yy += 40;

            UIView FacebookView = new UIView(new CGRect(ViewWidht / 2 - 60, yy + 10, 120, 40));
            FacebookView.AddSubview(loginFbButton);
            yy += 50;

            UILabel Oppure = new UILabel(new CGRect(ViewWidht / 2 - 30, yy + 10, 60, 25));
            Oppure.Text = NSBundle.MainBundle.LocalizedString("LogReg_Oppure", "or", null);
            Oppure.TextAlignment = UITextAlignment.Center;
            Oppure.TextColor = UIColor.White;
            yy += 35;

            Nome = new UITextField(new CGRect(1, 1, 248, 30));
            Nome.KeyboardType = UIKeyboardType.Default;
            Nome.Text = "";
            Nome.AttributedPlaceholder = new NSAttributedString(
                NSBundle.MainBundle.LocalizedString("Register_Nome", "Name", null),
                font: UIFont.FromName("MyriadPro-Regular", 15),
                foregroundColor: UIColor.White

            );
            Nome.TextColor = UIColor.White;
            Nome.BackgroundColor = UIColor.Black;
            Nome.BorderStyle = UITextBorderStyle.RoundedRect;
            Nome.Font = UIFont.FromName("MyriadPro-Regular", 15);
            Nome.ShouldReturn += (UITextField) =>
            {
                flagNome = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            UIView NomeView = new UIView(new CGRect(ViewWidht / 2 - 125, yy + 15, 250, 32));
            NomeView.Layer.CornerRadius = 7;
            NomeView.BackgroundColor = UIColor.White;
            NomeView.Add(Nome);

            yy += 47;

            Cognome = new UITextField(new CGRect(1, 1, 248, 30));
            Cognome.KeyboardType = UIKeyboardType.Default;
            Cognome.Text = "";
            Cognome.AttributedPlaceholder = new NSAttributedString(
                NSBundle.MainBundle.LocalizedString("Register_Cognome", "Surname", null),
                font: UIFont.FromName("MyriadPro-Regular", 15),
                foregroundColor: UIColor.White
            );
            Cognome.TextColor = UIColor.White;
            Cognome.BackgroundColor = UIColor.Black;
            Cognome.BorderStyle = UITextBorderStyle.RoundedRect;
            Cognome.Font = UIFont.FromName("MyriadPro-Regular", 15);
            Cognome.ShouldReturn += (UITextField) =>
            {
                flagCognome = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            UIView CognomeView = new UIView(new CGRect(ViewWidht / 2 - 125, yy + 5, 250, 32));
            CognomeView.Layer.CornerRadius = 7;
            CognomeView.BackgroundColor = UIColor.White;
            CognomeView.Add(Cognome);
            yy += 37;

            EMail = new UITextField(new CGRect(1, 1, 248, 30));
            EMail.KeyboardType = UIKeyboardType.EmailAddress;
            EMail.Text = "";
            EMail.AttributedPlaceholder = new NSAttributedString(
                "E-Mail",
                font: UIFont.FromName("MyriadPro-Regular", 15),
                foregroundColor: UIColor.White
            );
            EMail.TextColor = UIColor.White;
            EMail.BackgroundColor = UIColor.Black;
            EMail.BorderStyle = UITextBorderStyle.RoundedRect;
            EMail.Font = UIFont.FromName("MyriadPro-Regular", 15);
            EMail.ShouldReturn += (UITextField) =>
            {
                flagEMail = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            UIView EmailView = new UIView(new CGRect(ViewWidht / 2 - 125, yy + 5, 250, 32));
            EmailView.Layer.CornerRadius = 7;
            EmailView.BackgroundColor = UIColor.White;
            EmailView.Add(EMail);
            yy += 37;

            Password = new UITextField(new CGRect(1, 1, 248, 30));
            Password.KeyboardType = UIKeyboardType.Default;
            Password.SecureTextEntry = true;
            Password.Text = "";
            Password.AttributedPlaceholder = new NSAttributedString(
                "Password",
                font: UIFont.FromName("MyriadPro-Regular", 15),
                foregroundColor: UIColor.White
            );
            Password.TextColor = UIColor.White;
            Password.BackgroundColor = UIColor.Black;
            Password.BorderStyle = UITextBorderStyle.RoundedRect;
            Password.Font = UIFont.FromName("MyriadPro-Regular", 15);
            Password.ShouldReturn += (UITextField) =>
            {
                flagPassword = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            UIView PasswordView = new UIView(new CGRect(ViewWidht / 2 - 125, yy + 5, 250, 32));
            PasswordView.Layer.CornerRadius = 7;
            PasswordView.BackgroundColor = UIColor.White;
            PasswordView.Add(Password);
            yy += 37;

            RepeatPassword = new UITextField(new CGRect(1, 1, 248, 30));
            RepeatPassword.KeyboardType = UIKeyboardType.Default;
            RepeatPassword.SecureTextEntry = true;
            RepeatPassword.Text = "";
            RepeatPassword.AttributedPlaceholder = new NSAttributedString(
                NSBundle.MainBundle.LocalizedString("Register_RipetiP", "Repeat Password", null),
                font: UIFont.FromName("MyriadPro-Regular", 15),
                foregroundColor: UIColor.White
            );
            RepeatPassword.TextColor = UIColor.White;
            RepeatPassword.BackgroundColor = UIColor.Black;
            RepeatPassword.BorderStyle = UITextBorderStyle.RoundedRect;
            RepeatPassword.Font = UIFont.FromName("MyriadPro-Regular", 15);
            RepeatPassword.ShouldReturn += (UITextField) =>
            {
                flagRP = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            UIView RepeatPasswordView = new UIView(new CGRect(ViewWidht / 2 - 125, yy + 5, 250, 32));
            RepeatPasswordView.Layer.CornerRadius = 7;
            RepeatPasswordView.BackgroundColor = UIColor.White;
            RepeatPasswordView.Add(RepeatPassword);
            yy += 37;

            Citta = new UITextField(new CGRect(1, 1, 248, 30));
            Citta.KeyboardType = UIKeyboardType.Default;
            Citta.Text = "";
            Citta.AttributedPlaceholder = new NSAttributedString(
                NSBundle.MainBundle.LocalizedString("Register_Citta", "City*", null),
                font: UIFont.FromName("MyriadPro-Regular", 15),
                foregroundColor: UIColor.White
            );
            Citta.TextColor = UIColor.White;
            Citta.BackgroundColor = UIColor.Black;
            Citta.BorderStyle = UITextBorderStyle.RoundedRect;
            Citta.Font = UIFont.FromName("MyriadPro-Regular", 15);
            Citta.ShouldReturn += (UITextField) =>
            {
                flagCitta = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            UIView CittaView = new UIView(new CGRect(ViewWidht / 2 - 125, yy + 5, 250, 32));
            CittaView.Layer.CornerRadius = 7;
            CittaView.BackgroundColor = UIColor.White;
            CittaView.Add(Citta);
            yy += 37;

            Paese = new UITextField(new CGRect(1, 1, 248, 30));
            Paese.KeyboardType = UIKeyboardType.Default;
            Paese.Text = "";
            Paese.AttributedPlaceholder = new NSAttributedString(
                NSBundle.MainBundle.LocalizedString("Register_Paese", "County*", null),
                font: UIFont.FromName("MyriadPro-Regular", 15),
                foregroundColor: UIColor.White
            );
            Paese.TextColor = UIColor.White;
            Paese.BackgroundColor = UIColor.Black;
            Paese.BorderStyle = UITextBorderStyle.RoundedRect;
            Paese.Font = UIFont.FromName("MyriadPro-Regular", 15);
            Paese.ShouldReturn += (UITextField) =>
            {
                flagPaese = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            UIView PaeseView = new UIView(new CGRect(ViewWidht / 2 - 125, yy + 5, 250, 32));
            PaeseView.Layer.CornerRadius = 7;
            PaeseView.BackgroundColor = UIColor.White;
            PaeseView.Add(Paese);
            yy += 37;

            UILabel Opzionale = new UILabel(new CGRect(ViewWidht / 2 - 125, yy + 10, 250, 25));
            Opzionale.Text = NSBundle.MainBundle.LocalizedString("Register_Opzionale", "*Optional Field", null);
            Opzionale.TextAlignment = UITextAlignment.Center;
            Opzionale.TextColor = UIColor.White;
            yy += 35;

            //RegisterButton.Frame = ;
            UIButton BottoneRegistrati = new UIButton(new CGRect(ViewWidht / 2 - 125, yy + 15, 250, 40));
            BottoneRegistrati.SetTitle(NSBundle.MainBundle.LocalizedString("Register_Button", "REGISTER", null), UIControlState.Normal);
            BottoneRegistrati.BackgroundColor = UIColor.FromRGB(153, 19, 27);
            BottoneRegistrati.Font = UIFont.FromName("MyriadPro-Regular", 18);
            BottoneRegistrati.TouchUpInside += delegate
            {

                if (Password.Text.CompareTo(RepeatPassword.Text) == 0)
                {

                    UtilityLoginManager loginManagerNormal = new UtilityLoginManager();
                    List<string> value = loginManagerNormal.ValidationReg(Nome.Text, Cognome.Text, EMail.Text, Password.Text, Paese.Text, Citta.Text);
                    if (value[0] == "ERRORDATA")
                    {
                        int errore = 0;
                        if (value.Contains("N"))
                            errore++;
                        if (value.Contains("C"))
                            errore++;
                        if (value.Contains("P"))
                            errore++;
                        if (value.Contains("E"))
                        {
                            errore++;
                            EMail.Text = "";
                        }
                        if (errore == 1)
                        {
                            var adErr = new UIAlertView("Errore", "Un campo da lei inserito contiene valori errati o non contiene nessun valore, perfavore controlli", null, "OK", null);
                            adErr.Show();
                        }
                        if (errore >= 2)
                        {
                            var adErr = new UIAlertView("Errore", "Due o pi� campi da lei inseriti contengono valori errati o non contengono nessun valore, perfavore controlli", null, "OK", null);
                            adErr.Show();
                        }
                    }
                    if (value[0] == "ERROR")
                    {
                        var adErr = new UIAlertView("Errore di rete", "Registrazione non Riuscito riprovare pi� tardi", null, "OK", null);
                        adErr.Show();

                    }

                    if (value[0] == "SUCCESS")
                    {
                        Console.WriteLine(value[1]);

                        NSUserDefaults.StandardUserDefaults.SetString(value[1], "TokenMuseoN4U");
                        //NSUserDefaults.StandardUserDefaults.SetString(e.Result.Token.TokenString, "TokenLovieFB");

                        UIViewController lp = storyboard.InstantiateViewController("SWController");
                        this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                        this.PresentModalViewController(lp, true);
                        //******* SALVA TOKEN *********

                    }

                }
                else
                {

                    var adErr = new UIAlertView("Errore", "Le password non corrispondono riprovare", null, "OK", null);
                    adErr.Show();
                    Password.Text = "";
                    RepeatPassword.Text = "";


                }

            };


            yy += 55;

            UILabel Info = new UILabel(new CGRect(ViewWidht / 2 - 125, yy + 2, 250, 30));
            Info.Text = NSBundle.MainBundle.LocalizedString("LogReg_Contratto", "By clicking the button over here you accept the terms of use of this app", null);
            Info.TextAlignment = UITextAlignment.Center;
            Info.TextColor = UIColor.White;
            Info.Font = UIFont.SystemFontOfSize(12);
            Info.Lines = 2;
            yy += 32;



            scrollView.Add(imageView);
            scrollView.Add(Registrati);
            scrollView.Add(FacebookView);
            scrollView.Add(Oppure);
            scrollView.Add(NomeView);
            scrollView.Add(CognomeView);
            scrollView.Add(EmailView);
            scrollView.Add(PasswordView);
            scrollView.Add(RepeatPasswordView);
            scrollView.Add(CittaView);
            scrollView.Add(PaeseView);
            scrollView.Add(Opzionale);
            scrollView.Add(BottoneRegistrati);
            scrollView.Add(Info);

            scrollView.ContentSize = new CGSize(View.Frame.Width, yy + 10);

            //base.NavigationController.PushViewController(new LoginPage(Handle),false);
            ContentView.Add(scrollView);


            Nome.ShouldBeginEditing = delegate
            {
                flagNome = true;
                CGRect frame = View.Frame;
                Console.Out.WriteLine("Dimensioni:" + UIScreen.MainScreen.Bounds);
                if (View.Frame.Height == 480)
                {
                    frame.Y = -50;
                    View.Frame = frame;
                }

                return true;
            };

            Cognome.ShouldBeginEditing = delegate
            {
                flagCognome = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -85;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 568)
                {
                    frame.Y = -26;
                    View.Frame = frame;
                }

                return true;
            };

            EMail.ShouldBeginEditing = delegate
            {
                flagEMail = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -125;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 568)
                {
                    frame.Y = -56;
                    View.Frame = frame;
                }

                return true;
            };

            Password.ShouldBeginEditing = delegate
            {
                flagPassword = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -125;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 568)
                {
                    frame.Y = -56;
                    View.Frame = frame;
                }


                return true;
            };

            RepeatPassword.ShouldBeginEditing = delegate
            {
                flagRP = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -161;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 568)
                {
                    frame.Y = -92;
                    View.Frame = frame;
                }
                return true;
            };

            Citta.ShouldBeginEditing = delegate
            {
                flagCitta = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -195;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 568)
                {
                    frame.Y = -128;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 667)
                {
                    frame.Y = -20;
                    View.Frame = frame;
                }
                return true;
            };

            Paese.ShouldBeginEditing = delegate
            {
                flagPaese = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -222;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 568)
                {
                    frame.Y = -164;
                    View.Frame = frame;
                }
                if (View.Frame.Height == 667)
                {
                    frame.Y = -54;
                    View.Frame = frame;
                }
                return true;
            };

            Nome.ShouldEndEditing = delegate
            {

                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagNome == false)
                    View.Frame = frame;
                return true;
            };

            Cognome.ShouldEndEditing = delegate
            {
                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagCognome == false)
                    View.Frame = frame;
                return true;
            };

            EMail.ShouldEndEditing = delegate
            {
                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagEMail == false)
                    View.Frame = frame;
                return true;
            };

            Password.ShouldEndEditing = delegate
            {
                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagPassword == false)
                    View.Frame = frame;
                return true;
            };

            RepeatPassword.ShouldEndEditing = delegate
            {
                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagRP == false)
                    View.Frame = frame;
                return true;
            };

            Citta.ShouldEndEditing = delegate
            {
                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagCitta == false)
                    View.Frame = frame;
                return true;
            };

            Paese.ShouldEndEditing = delegate
            {
                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagPaese == false)
                    View.Frame = frame;
                return true;
            };

            /* VIEW PER CAROUSEL */

            UIView PopUp = new UIView(new CGRect(0, 0, ContentView.Frame.Width, ContentView.Frame.Height));
            PopUp.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 200);

            float w = (float)ContentView.Frame.Width - 100;
            float h = (w * 850) / 695;

            UIView Container = new UIView(new CGRect((ContentView.Frame.Width / 2) - (w / 2), (ContentView.Frame.Height / 2) - (h / 2), w, h));
            Container.BackgroundColor = UIColor.Clear;

            List<string> app = new List<string>();
            app.Add("Help1.png");
            app.Add("Help2.png");
            app.Add("Help3.png");
            app.Add("Help4.png");

            carousel = new iCarousel();
            carousel.Frame = new CGRect(0, 0, Container.Frame.Width, Container.Frame.Height);
            carousel.Type = iCarouselType.CoverFlow2;
            carousel.DataSource = new CarouselDataSource(4, (float)Container.Frame.Width, (float)((Container.Frame.Width * 850) / 695), app, 0);
            carousel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
            //carousel.CurrentItemIndexChanged += Carousel_CurrentItemIndexChanged;

            UIImageView ImageX = new UIImageView(new CGRect((ContentView.Frame.Width / 2) + (w / 2) + 10, (ContentView.Frame.Height / 2) - (h / 2) - 35, 23, 23));
            ImageX.Image = UIImage.FromFile("X_popup_icon.png");

            UITapGestureRecognizer imageXTap = new UITapGestureRecognizer(() =>
            {
                PopUp.RemoveFromSuperview();
            });
            ImageX.UserInteractionEnabled = true;
            ImageX.AddGestureRecognizer(imageXTap);

            pallino1 = new UIImageView(new CGRect((ContentView.Frame.Width / 2) - 16, (ContentView.Frame.Height / 2) + (h / 2) + 10, 5, 5));
            pallino1.Image = UIImage.FromFile("pallinorosso.png");

            pallino2 = new UIImageView(new CGRect((ContentView.Frame.Width / 2) - 7, (ContentView.Frame.Height / 2) + (h / 2) + 10, 5, 5));
            pallino2.Image = UIImage.FromFile("pallinobianco.png");

            pallino3 = new UIImageView(new CGRect((ContentView.Frame.Width / 2) + 2, (ContentView.Frame.Height / 2) + (h / 2) + 10, 5, 5));
            pallino3.Image = UIImage.FromFile("pallinobianco.png");

            pallino4 = new UIImageView(new CGRect((ContentView.Frame.Width / 2) + 11, (ContentView.Frame.Height / 2) + (h / 2) + 10, 5, 5));
            pallino4.Image = UIImage.FromFile("pallinobianco.png");

            Container.Add(carousel);
            PopUp.Add(ImageX);
            PopUp.Add(Container);
            PopUp.Add(pallino1);
            PopUp.Add(pallino2);
            PopUp.Add(pallino3);
            PopUp.Add(pallino4);
            ContentView.Add(PopUp);

        }

        private void Carousel_CurrentItemIndexChanged(object sender, EventArgs e)
        {
            int index = (int)carousel.CurrentItemIndex;
            ChangePallino(index);
        }

        public void ChangePallino(int i)
        {
            if (i == 0)
            {
                pallino1.Image = UIImage.FromFile("pallinorosso.png");
                pallino2.Image = UIImage.FromFile("pallinobianco.png");
                pallino3.Image = UIImage.FromFile("pallinobianco.png");
                pallino4.Image = UIImage.FromFile("pallinobianco.png");
            }
            if (i == 1)
            {
                pallino2.Image = UIImage.FromFile("pallinorosso.png");
                pallino1.Image = UIImage.FromFile("pallinobianco.png");
                pallino3.Image = UIImage.FromFile("pallinobianco.png");
                pallino4.Image = UIImage.FromFile("pallinobianco.png");
            }
            if (i == 2)
            {
                pallino3.Image = UIImage.FromFile("pallinorosso.png");
                pallino2.Image = UIImage.FromFile("pallinobianco.png");
                pallino1.Image = UIImage.FromFile("pallinobianco.png");
                pallino4.Image = UIImage.FromFile("pallinobianco.png");
            }
            if (i == 3)
            {
                pallino4.Image = UIImage.FromFile("pallinorosso.png");
                pallino2.Image = UIImage.FromFile("pallinobianco.png");
                pallino3.Image = UIImage.FromFile("pallinobianco.png");
                pallino1.Image = UIImage.FromFile("pallinobianco.png");
            }
        }
    }
}
