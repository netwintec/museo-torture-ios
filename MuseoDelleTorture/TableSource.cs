﻿using System;
using System.Collections.Generic;
using System.Text;
using Foundation;
using UIKit;

namespace MuseoDelleTorture
{
    class TableSource : UITableViewSource
    {

        MenuController super;
        public TableSource(MenuController s) {

            super = s;

        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return 7;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {

            if(GiocaHomeController.Instance != null && indexPath.Row != 5 )
            {
                GiocaHomeController.Instance.setNull();
            }

            if (indexPath.Row == 1)
            {
                super.ChangePage(indexPath.Row);
            }

            if (indexPath.Row == 4)
            {
                super.NonDisp();
            }
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            string cellIdentifier = @"Cell";

            switch (indexPath.Row)
            {
                case 0:
                    cellIdentifier = @"one";
                    break;

                case 1:
                    cellIdentifier = @"two";
                    break;

                case 2:
                    cellIdentifier = @"three";
                    break;

                case 3:
                    cellIdentifier = @"four";
                    break;

                case 4:
                    cellIdentifier = @"five";
                    break;

                case 5:
                    cellIdentifier = @"six";
                    break;

                case 6:
                    cellIdentifier = @"seven";
                    break;
            }

            UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier);
            return cell;

            //throw new NotImplementedException();
        }
    }
}
