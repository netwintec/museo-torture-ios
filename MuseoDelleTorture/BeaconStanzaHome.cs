using CoreGraphics;
using Foundation;
using Xamarin.SWRevealViewController;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Threading;
using UIKit;

namespace MuseoDelleTorture
{
    partial class BeaconStanzaHome : UIViewController
    {

        public Dictionary<int, BeaconInfo> BeaconDictionary = new Dictionary<int, BeaconInfo>();
        List<UIImageView> listImage = new List<UIImageView>();
        public BeaconInfo ObjectSelected;
        public BeaconStanzaHome(IntPtr handle) : base(handle)
        {
        }

        public static BeaconStanzaHome Instance { private set; get; }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            BeaconStanzaHome.Instance = this;

            //AppDelegate.Instance.ScanBeIn = false;
            HomePage.Instance.isVisita = true;

            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
            }
            UIViewController HomePageC = storyboard.InstantiateViewController("SWController");
            UIViewController ImmagineP = storyboard.InstantiateViewController("immagine");
            UIViewController ImmaginiP = storyboard.InstantiateViewController("immagini");
            UIViewController VideoP = storyboard.InstantiateViewController("video");
            UIViewController RealtaP = storyboard.InstantiateViewController("realta");
            UIViewController Visita = storyboard.InstantiateViewController("VisitaGuidata");
            BackItem.Clicked += (object sender, EventArgs e) =>
            {
                this.DismissModalViewController(true);
            };

            if (!AppDelegate.Instance.Download)
            {
                this.PresentModalViewController(HomePageC, true);
                return;
            }

            UIScrollView scrollView = new UIScrollView(new CGRect(0, 0, ContentView.Frame.Width, ContentView.Frame.Height));

            //JsonUtility ju = new JsonUtility();
            //ju.SpacchettamentoJsonBeacon("lucca", BeaconDictionary, text, NSLocale.PreferredLanguages[0].Substring(0, 2));

            BeaconDictionary = AppDelegate.Instance.StanzeDictionary[AppDelegate.Instance.StanzaFind].BeaconDictionary;

            int yy = 0;

            UIImageView TopImage = new UIImageView(new CGRect(0, 0, View.Frame.Width, 50));
            TopImage.Image = UIImage.FromFile("mostra_sfondo.jpg");

            UIView TopView = new UIView(new CGRect(0, 0, View.Frame.Width, 50));
            TopView.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 150);

            UILabel TopLabel = new UILabel(new CGRect(0, 10, View.Frame.Width, 40));
            TopLabel.Font = UIFont.FromName("DaunPenh", 32);
            TopLabel.Frame.Inset(0, 5); //= new UIEdgeInsets(5, 0, 0, 0);
            TopLabel.TextAlignment = UITextAlignment.Center;
            TopLabel.TextColor = UIColor.White;
            //TopLabel.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 150);
            TopLabel.Text = NSBundle.MainBundle.LocalizedString("BeaconH_Stanza", "ROOM N�", null) + BeaconDictionary[0].stanza;

            yy += 50;

            var size = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("BeaconH_Scopri", "Click to learn more", null), UIFont.FromName("MyriadPro-Regular", 22), new CGSize(ContentView.Frame.Width, 2000));

            UILabel tap = new UILabel(new CGRect(0, yy + 15, ContentView.Frame.Width, size.Height));
            tap.Font = UIFont.FromName("MyriadPro-Regular", 22);
            tap.TextAlignment = UITextAlignment.Center;
            tap.TextColor = UIColor.White;
            tap.Text = NSBundle.MainBundle.LocalizedString("BeaconH_Scopri", "Click to learn more", null);

            yy += (int)(size.Height + 15);

            float x = 30;
            float w = (float)((ContentView.Frame.Width - 90) / 2);
            float h = w;
            yy += 10;

            for (int i = 0; i < BeaconDictionary.Count; i++)
            {
                UIImageView img = new UIImageView();
                if (i % 2 == 0)
                {
                    img = new UIImageView(new CGRect(x, yy + 15, w, h));
                    img.Image = UIImage.FromFile("placeholder.png");
                    yy += (int)(h + 15);
                }
                if (i % 2 == 1)
                {
                    img = new UIImageView(new CGRect(w + 2 * x, yy - h, w, h));
                    img.Image = UIImage.FromFile("placeholder.png");
                }

                UITapGestureRecognizer imgTap = new UITapGestureRecognizer((s) =>
                {
                    int giusto = -1;
                    for (int ii = 0; ii < listImage.Count; ii++)
                    {
                        Console.WriteLine((listImage[ii] == s.View) + "|" + ii);
                        if (listImage[ii] == s.View)
                        {
                            giusto = ii;
                        }
                    }

                    if (BeaconDictionary[giusto].tipo == "immagine")
                    {
                        ObjectSelected = BeaconDictionary[giusto];
                        this.PresentModalViewController(ImmagineP, true);
                        Console.WriteLine("immagine");
                    }
                    if (BeaconDictionary[giusto].tipo == "immagini")
                    {
                        ObjectSelected = BeaconDictionary[giusto];
                        this.PresentModalViewController(ImmaginiP, true);
                        Console.WriteLine("immagini");
                    }
                    if (BeaconDictionary[giusto].tipo == "realta")
                    {
                        ObjectSelected = BeaconDictionary[giusto];
                        this.PresentModalViewController(RealtaP, true);
                        Console.WriteLine("realta");
                    }
                    if (BeaconDictionary[giusto].tipo == "video")
                    {
                        ObjectSelected = BeaconDictionary[giusto];
                        this.PresentModalViewController(VideoP, true);
                        Console.WriteLine("video");
                    }

                });
                img.UserInteractionEnabled = true;
                img.AddGestureRecognizer(imgTap);

                if (BeaconDictionary[i].Thumbnail == null)
                    SetImageAsync(img, BeaconDictionary[i].url_thumbnail, BeaconDictionary[i]);
                else
                    img.Image = BeaconDictionary[i].Thumbnail;

                listImage.Add(img);
                scrollView.Add(img);

            }

            scrollView.Add(TopImage);
            scrollView.Add(TopView);
            scrollView.Add(TopLabel);
            scrollView.Add(tap);

            scrollView.ContentSize = new CGSize(ContentView.Frame.Width, yy + 10);

            ContentView.Add(scrollView);
        }

        public void SetImageAsync(UIImageView image, string url, BeaconInfo b)
        {
            UIImage Placeholder = UIImage.FromFile("placeholder.png");


            UIImage cachedImage;
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {

                    // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                    //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                    cachedImage = FromUrl(url) ?? Placeholder;


                    InvokeOnMainThread(() =>
                    {
                        b.Thumbnail = cachedImage;
                        image.Image = cachedImage;

                    });

                });
            }
            catch (Exception e)
            {
                Console.WriteLine("URL LOAD :" + e.StackTrace);
            }

        }

        static UIImage FromUrl(string uri)
        {
            try
            {
                using (var url = new NSUrl(uri))
                using (var data = NSData.FromUrl(url))
                    if (data != null)
                        return UIImage.LoadFromData(data);
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }


    }
}
