﻿using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Threading;
using UIKit;

namespace MuseoDelleTorture
{
	partial class CosaVedereInfoController : UIViewController
	{

        MarkerInfo Marker;
        List<UIImageView> imageList = new List<UIImageView>();
        bool PopUp = false;

        UIView trasparentView, PopUpView;
        public CosaVedereInfoController (IntPtr handle) : base (handle)
		{
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();


            float TextHeight = (float)(ContentView.Frame.Height - 340); 

            Marker = CosaVedereMappaController.Instance.Selected;

            int yy = 0;

            UIView TopView = new UIView(new CGRect(0, 0, View.Frame.Width, 50));
            TopView.BackgroundColor = UIColor.FromRGB(153, 19, 27);

            UILabel TopLabel = new UILabel(new CGRect(0, 10, View.Frame.Width, 40));
            TopLabel.Font = UIFont.FromName("DaunPenh", 32);
            TopLabel.TextAlignment = UITextAlignment.Center;
            TopLabel.TextColor = UIColor.White;
            TopLabel.BackgroundColor = UIColor.FromRGB(153, 19, 27);
            TopLabel.TextRectForBounds(new CGRect(0, 10, View.Frame.Width, 40), 1);
            TopLabel.Text = NSBundle.MainBundle.LocalizedString("Menu_CosaVedere", "WHAT TO SEE IN TOWN", null);

            yy += 50;

            UIScrollView horizontalScroll = new UIScrollView(new CGRect(0,yy+5,View.Frame.Width,180));

            int xx = 0;
            for(int i = 0; i < Marker.imgurl.Count; i++)
            {
                UIImageView img = new UIImageView(new CGRect(xx+5,0,180,180));
                img.Image = UIImage.FromFile("placeholder.png");

                imageList.Add(img);

                xx += 185;


                UITapGestureRecognizer imageView1Tap = new UITapGestureRecognizer((s) =>
                {
                    int giusto = -1;
                    for (int ii = 0; ii < imageList.Count; ii++)
                    {
                        Console.WriteLine((imageList[ii] == s.View)+"|"+ii);
                        if(imageList[ii] == s.View)
                        {
                            giusto = ii;
                        }
                    }

                    if (!PopUp)
                    {
                        PopUp = true;
                        AppariPopUp(giusto);
                    }

                });

                SetImageAsync(imageList[i], Marker.imgurl[i]);
                imageList[i].UserInteractionEnabled = true;
                imageList[i].AddGestureRecognizer(imageView1Tap);


                horizontalScroll.Add(img);
            }

            horizontalScroll.ContentSize = new CGSize(xx+5,180);

            yy += 185;

            UIView SeparatorView = new UIView(new CGRect(0, yy+5, View.Frame.Width, 4));
            SeparatorView.BackgroundColor = UIColor.FromRGB(153, 19, 27);

            yy += 9;


            var size = UIStringDrawing.StringSize(Marker.titolo, UIFont.FromName("MyriadPro-Regular", 19), new CGSize(View.Frame.Width, 2000));

            UILabel Titolo = new UILabel(new CGRect(0,yy+10,View.Frame.Width, size.Height));
            Titolo.Font = UIFont.FromName("MyriadPro-Regular", 19);
            Titolo.TextAlignment = UITextAlignment.Center;
            Titolo.TextColor = UIColor.White;
            Titolo.Text = Marker.titolo;
            Titolo.Lines = 0;

            yy += (int)size.Height +10;

            if(size.Height > 20)
            {
                TextHeight += 20;
                TextHeight -= (int)size.Height;
            }

            UIScrollView textScroll = new UIScrollView(new CGRect(20, yy + 5, View.Frame.Width-40, TextHeight));

            size = UIStringDrawing.StringSize(Marker.descrizioneC, UIFont.FromName("MyriadPro-Regular", 16), new CGSize(View.Frame.Width - 40, 2000));

            UILabel Descr = new UILabel(new CGRect(0, 0, View.Frame.Width-40, size.Height));
            Descr.Font = UIFont.FromName("MyriadPro-Regular", 16);
            Descr.TextAlignment = UITextAlignment.Left;
            Descr.TextColor = UIColor.White;
            Descr.Lines = 0;
            Descr.Text = Marker.descrizioneC;

            textScroll.Add(Descr);
            textScroll.ContentSize=new CGSize(View.Frame.Width - 40,size.Height);

            yy += (int)(TextHeight + 5);

            UIView buttonNaviga = new UIView(new CGRect(ContentView.Frame.Width/2-100,yy+10,200,50));
            buttonNaviga.BackgroundColor = UIColor.FromRGB(153, 19, 27);
            UITapGestureRecognizer buttonTap = new UITapGestureRecognizer(() =>
            {

                string url = "http://maps.apple.com/?daddr="+Marker.via;
                url = url.Replace(" ", "%20");
                if (UIApplication.SharedApplication.CanOpenUrl(new NSUrl(url)))
                {
                    UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
                }
                else
                {
                    new UIAlertView("Error", "Maps is not supported on this device", null, "Ok").Show();
                }

            });
            buttonNaviga.UserInteractionEnabled = true;
            buttonNaviga.AddGestureRecognizer(buttonTap);

            UIImageView navigaImage = new UIImageView(new CGRect(10,10,30,30));
            navigaImage.Image = UIImage.FromFile("naviga_verso_icon.png");

            size = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Citta_Naviga", "TRAVELS TO", null), UIFont.FromName("DaunPenh", 22), new CGSize(145, 2000));

            UILabel navigaText = new UILabel(new CGRect(55, 28-(size.Height/2), 145, size.Height));
            navigaText.Font = UIFont.FromName("DaunPenh", 22);
            navigaText.TextAlignment = UITextAlignment.Center;
            navigaText.TextColor = UIColor.White;
            navigaText.Lines = 1;
            navigaText.Text = NSBundle.MainBundle.LocalizedString("Citta_Naviga", "GET DIRECTIONS", null);

            buttonNaviga.Add(navigaImage);
            buttonNaviga.Add(navigaText);

            trasparentView = new UIView(new CGRect(0, 0, ContentView.Frame.Width, ContentView.Frame.Height));
            trasparentView.BackgroundColor = UIColor.FromRGBA(0,0,0,150);
            trasparentView.Alpha = 0;

            ContentView.Add(TopView);
            ContentView.Add(TopLabel);
            ContentView.Add(horizontalScroll);
            ContentView.Add(SeparatorView);
            ContentView.Add(Titolo);
            ContentView.Add(textScroll);
            ContentView.Add(buttonNaviga);
            ContentView.Add(trasparentView);


            
        }

        public void AppariPopUp(int i)
        {
            trasparentView.Alpha = 1;

            float widht = (float)(trasparentView.Frame.Width - 80);
            float height = (float)((trasparentView.Frame.Width - 80)+70);

            PopUpView = new UIView(new CGRect(40,(trasparentView.Frame.Height/2)-(height/2),widht,height));
            PopUpView.BackgroundColor = UIColor.FromRGB(45,45,45);

            UIButton EscButton = new UIButton(new CGRect(widht -27, 2, 25, 25));
            UIImageView EscImage = new UIImageView(EscButton.Bounds);
            EscImage.Image = UIImage.FromFile("X_popup_icon.png");
            EscButton.AddSubview(EscImage);
            EscButton.TouchUpInside += (object sender, EventArgs e) => {
                RimuoviPopUp();
            };

            UIImageView image = new UIImageView(new CGRect(0,35,widht,widht));
            image.Image = UIImage.FromFile("placeholder.png");

            SetImageAsyncPopUp(image,Marker.imgurl[i],widht);

            PopUpView.Add(EscButton);
            PopUpView.Add(image);
            trasparentView.Add(PopUpView);

        }

        public void RimuoviPopUp()
        {

            for (int x =0; x<PopUpView.Subviews.Length; x++)
            {
                PopUpView.Subviews[x].RemoveFromSuperview();
            }

            PopUpView.RemoveFromSuperview();
            trasparentView.Alpha = 0;

            PopUp = false;

        }

        public void SetImageAsync(UIImageView image,string url)
        {
            UIImage Placeholder = UIImage.FromFile("placeholder.png");


            UIImage cachedImage;
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {

                    // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                    //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                    cachedImage = FromUrl(url) ?? Placeholder;


                    InvokeOnMainThread(() =>
                    {
                        nfloat w = cachedImage.Size.Width;
                        nfloat h = cachedImage.Size.Height;

                        float newWidht = (float)180;
                        float newHeight = (float)((180 * h) / w);

                        Console.WriteLine(w + "  " + h);
                        image.Frame = new CGRect(image.Frame.X, image.Frame.Y+(90-(newHeight/2)), newWidht, newHeight);

                        image.Image = cachedImage;

                    });

                });
            }
            catch (Exception e)
            {
                Console.WriteLine("URL LOAD :" + e.StackTrace);
            }

        }

        public void SetImageAsyncPopUp(UIImageView image, string url,float widht)
        {
            UIImage Placeholder = UIImage.FromFile("placeholder.png");


            UIImage cachedImage;
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {

                    // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                    //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                    cachedImage = FromUrl(url) ?? Placeholder;


                    InvokeOnMainThread(() =>
                    {
                        nfloat w = cachedImage.Size.Width;
                        nfloat h = cachedImage.Size.Height;

                        float newWidht = (float)widht;
                        float newHeight = (float)((widht * h) / w);

                        Console.WriteLine(w + "  " + h);
                        PopUpView.Frame = new CGRect(40,(trasparentView.Frame.Height/2)-((newHeight+70)/2),widht, newHeight + 70);
                        image.Frame = new CGRect(0, 35 , newWidht, newHeight);

                        image.Image = cachedImage;

                    });

                });
            }
            catch (Exception e)
            {
                Console.WriteLine("URL LOAD :" + e.StackTrace);
            }

        }

        static UIImage FromUrl(string uri)
        {
            try
            {
                using (var url = new NSUrl(uri))
                using (var data = NSData.FromUrl(url))
                    if (data != null)
                        return UIImage.LoadFromData(data);
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

    }
}

