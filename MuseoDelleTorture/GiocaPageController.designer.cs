// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MuseoDelleTorture
{
    [Register ("GiocaPageController")]
    partial class GiocaPageController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel AttendiLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView AttendiPage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem BackItem { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ContentViewDomande { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ContentViewResult { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem MenuItem { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AttendiLabel != null) {
                AttendiLabel.Dispose ();
                AttendiLabel = null;
            }

            if (AttendiPage != null) {
                AttendiPage.Dispose ();
                AttendiPage = null;
            }

            if (BackItem != null) {
                BackItem.Dispose ();
                BackItem = null;
            }

            if (ContentViewDomande != null) {
                ContentViewDomande.Dispose ();
                ContentViewDomande = null;
            }

            if (ContentViewResult != null) {
                ContentViewResult.Dispose ();
                ContentViewResult = null;
            }

            if (MenuItem != null) {
                MenuItem.Dispose ();
                MenuItem = null;
            }
        }
    }
}