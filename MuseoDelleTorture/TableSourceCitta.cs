﻿using System;
using System.Collections.Generic;
using System.Text;
using Foundation;
using UIKit;
using Xamarin.SWRevealViewController;

namespace MuseoDelleTorture
{
    class TableSourceCitta : UITableViewSource
    {

        Dictionary<string, MarkerInfo> MarkerDictionary = new Dictionary<string, MarkerInfo>();
        Dictionary<int, MarkerInfo> MarkerDictionaryComplete = new Dictionary<int, MarkerInfo>();

        public TableSourceCitta()
        {

            MarkerDictionary = HomePage.Instance.MarkerDictionary;
            MarkerDictionaryComplete = HomePage.Instance.MarkerDictionaryComplete;
            
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return MarkerDictionaryComplete.Count;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {

            nfloat height;
            
            if(MarkerDictionaryComplete[indexPath.Row].isHeader)
            {
                height = 80;
            }
            else
            {
                height = 40;
            }

            return height;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {

            if (CosaVedereMappaController.Instance != null)
            {
                CosaVedereMappaController.Instance.selectedMarker((int)indexPath.Row);

            }
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            int row = indexPath.Row;

            if (MarkerDictionaryComplete[row].isHeader)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellCittaHeader cell = new CustomCellCittaHeader(cellIdentifier, MarkerDictionaryComplete[row].nome);
                if (MarkerDictionaryComplete[row].nome == "PALAZZI")
                {
                    cell.UpdateCell(UIImage.FromFile("palazzi_list_icon.png"));
                }
                if (MarkerDictionaryComplete[row].nome == "PARCHEGGI")
                {
                    cell.UpdateCell(UIImage.FromFile("parcheggi_list_icon.png"));
                }
                if (MarkerDictionaryComplete[row].nome == "PIAZZE E MONUMENTI")
                {
                    cell.UpdateCell(UIImage.FromFile("PM_list_icon.png"));
                }
                if (MarkerDictionaryComplete[row].nome == "CHIESE")
                {
                    cell.UpdateCell(UIImage.FromFile("chiese_list_icon.png"));
                }

                return cell;

            }
            else {

                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellCittaRow cell = new CustomCellCittaRow(cellIdentifier, MarkerDictionaryComplete[row].nomeE);
                cell.UpdateCell(MarkerDictionaryComplete[row].nomeE);

                return cell;

            }

        }

    }
}