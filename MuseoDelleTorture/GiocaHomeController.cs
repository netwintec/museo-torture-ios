using CoreGraphics;
using Foundation;
using RestSharp;
using Xamarin.SWRevealViewController;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using UIKit;

namespace MuseoDelleTorture
{
	partial class GiocaHomeController : UIViewController
	{

        UIScrollView scrollView;

        int yy = 0;
        float imageWidth, imageHeight;

        List<UIImageView> GiocaImages = new List<UIImageView>();
        List<UILabel> GiocaTexts = new List<UILabel>();

        public List<List<GiocaElement>> ListGioca = new List<List<GiocaElement>>();
        public List<GiocaResult> ListResult = new List<GiocaResult>();

        public int selected;
        int porta = 82;

        public GiocaHomeController (IntPtr handle) : base (handle)
		{
		}

        public static GiocaHomeController Instance { get; private set; }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += (sender, e) => this.RevealViewController().RevealToggleAnimated(true);
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            BackItem.Clicked += delegate { setNull(); };

            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
            }
            UIViewController HomePageC = storyboard.InstantiateViewController("HomePage");
            UIViewController GiocaPage = storyboard.InstantiateViewController("GiocaPage");

            BackItem.Clicked += delegate {
                setNull();
                this.RevealViewController().PushFrontViewController(HomePageC, true);
            };

            AppDelegate.Instance.ScanBeIn = false;

            if (GiocaHomeController.Instance == null)
            {
                GiocaHomeController.Instance = this;
            }
            else
            {
                ListGioca = GiocaHomeController.Instance.ListGioca;
                ListResult = GiocaHomeController.Instance.ListResult;
            }

            imageWidth = (float)((ContentView.Frame.Width - 150)/ 2);
            imageHeight = imageWidth;

            scrollView = new UIScrollView(new CGRect(0, 0, View.Frame.Width, ContentView.Frame.Height));

            UIImageView TopImage = new UIImageView(new CGRect(0, 0, View.Frame.Width, 50));
            TopImage.Image = UIImage.FromFile("musei_sfondo.jpg");

            UIView TopView = new UIView(new CGRect(0, 0, View.Frame.Width, 50));
            TopView.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 150);

            UILabel TopLabel = new UILabel(new CGRect(0, 10, View.Frame.Width, 40));
            TopLabel.Font = UIFont.FromName("DaunPenh", 32);
            TopLabel.TextAlignment = UITextAlignment.Center;
            TopLabel.TextColor = UIColor.White;
            //TopLabel.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 150);
            //TopLabel.TextRectForBounds(new CGRect(0, 10, View.Frame.Width, 40), 1);
            TopLabel.Text = NSBundle.MainBundle.LocalizedString("Menu_Gioca", "PLAY", null);

            yy += 50;

            UIView SeparatorView = new UIView(new CGRect(0, yy, View.Frame.Width, 4));
            SeparatorView.BackgroundColor = UIColor.White;

            UILabel TitoloLabel = new UILabel(new CGRect(0, yy +18, View.Frame.Width, 27));
            TitoloLabel.Font = UIFont.FromName("DaunPenh", 25);
            TitoloLabel.TextAlignment = UITextAlignment.Center;
            TitoloLabel.TextColor = UIColor.White;
            TitoloLabel.Text = "TORTURE GAME";

            yy += 45;


            UIView imageV1 = new UIView(new CGRect(50, yy + 20, imageWidth, imageHeight));
            UIView imageV2 = new UIView(new CGRect((View.Frame.Width / 2) + 25, yy + 20, imageWidth, imageHeight));

            imageV1.BackgroundColor = UIColor.White;
            imageV2.BackgroundColor = UIColor.White;

            UIImageView imageview1 = new UIImageView(new CGRect(2,2,imageWidth-4,imageHeight-4));
            UIImageView imageview2 = new UIImageView(new CGRect(2, 2, imageWidth-4, imageHeight-4));

            imageview1.BackgroundColor = UIColor.Black;
            imageview2.BackgroundColor = UIColor.Black;

            imageV1.Add(imageview1);
            imageV2.Add(imageview2);

            GiocaImages.Add(imageview1);
            GiocaImages.Add(imageview2);

            yy += (int)(20 + imageHeight);

            UILabel label1 = new UILabel(new CGRect(35, yy + 5, imageWidth+30, 24));
            label1.Font = UIFont.FromName("DaunPenh", 22);
            label1.TextAlignment = UITextAlignment.Center;
            label1.TextColor = UIColor.White;

            UILabel label2 = new UILabel(new CGRect((View.Frame.Width / 2) + 10, yy + 5, imageWidth+30, 24));
            label2.Font = UIFont.FromName("DaunPenh", 22);
            label2.TextAlignment = UITextAlignment.Center;
            label2.TextColor = UIColor.White;

            GiocaTexts.Add(label1);
            GiocaTexts.Add(label2);

            yy += 29;

            UIView imageV3 = new UIView(new CGRect(50, yy + 20, imageWidth, imageHeight));
            UIView imageV4 = new UIView(new CGRect((View.Frame.Width / 2) + 25, yy + 20, imageWidth, imageHeight));

            imageV3.BackgroundColor = UIColor.White;
            imageV4.BackgroundColor = UIColor.White;

            UIImageView imageview3 = new UIImageView(new CGRect(2, 2, imageWidth - 4, imageHeight - 4));
            UIImageView imageview4 = new UIImageView(new CGRect(2, 2, imageWidth - 4, imageHeight - 4));

            imageview3.BackgroundColor = UIColor.Black;
            imageview4.BackgroundColor = UIColor.Black;

            imageV3.Add(imageview3);
            imageV4.Add(imageview4);

            GiocaImages.Add(imageview3);
            GiocaImages.Add(imageview4);

            yy += (int)(20 + imageHeight);

            UILabel label3 = new UILabel(new CGRect(35, yy + 5, imageWidth + 30, 24));
            label3.Font = UIFont.FromName("DaunPenh", 22);
            label3.TextAlignment = UITextAlignment.Center;
            label3.TextColor = UIColor.White;

            UILabel label4 = new UILabel(new CGRect((View.Frame.Width / 2) + 10, yy + 5, imageWidth + 30, 24));
            label4.Font = UIFont.FromName("DaunPenh", 22);
            label4.TextAlignment = UITextAlignment.Center;
            label4.TextColor = UIColor.White;

            GiocaTexts.Add(label3);
            GiocaTexts.Add(label4);

            yy += 29;

            UIView imageV5 = new UIView(new CGRect(50, yy + 20, imageWidth, imageHeight));
            UIView imageV6 = new UIView(new CGRect((View.Frame.Width / 2) + 25, yy + 20, imageWidth, imageHeight));

            imageV5.BackgroundColor = UIColor.White;
            imageV6.BackgroundColor = UIColor.White;

            UIImageView imageview5 = new UIImageView(new CGRect(2, 2, imageWidth - 4, imageHeight - 4));
            UIImageView imageview6 = new UIImageView(new CGRect(2, 2, imageWidth - 4, imageHeight - 4));

            imageview5.BackgroundColor = UIColor.Black;
            imageview6.BackgroundColor = UIColor.Black;

            imageV5.Add(imageview5);
            imageV6.Add(imageview6);

            GiocaImages.Add(imageview5);
            GiocaImages.Add(imageview6);

            yy += (int)(20 + imageHeight);

            UILabel label5 = new UILabel(new CGRect(35, yy + 5, imageWidth + 30, 24));
            label5.Font = UIFont.FromName("DaunPenh", 22);
            label5.TextAlignment = UITextAlignment.Center;
            label5.TextColor = UIColor.White;

            UILabel label6 = new UILabel(new CGRect((View.Frame.Width / 2) + 10, yy + 5, imageWidth + 30, 24));
            label6.Font = UIFont.FromName("DaunPenh", 22);
            label6.TextAlignment = UITextAlignment.Center;
            label6.TextColor = UIColor.White;

            GiocaTexts.Add(label5);
            GiocaTexts.Add(label6);

            yy += 29;

            //*** SPACCHETTAMENTO GIOCA ***//
            if (ListResult.Count == 0)
            {

                var client = new RestClient("http://api.netwintec.com:" + porta + "/");
                //client.Authenticator = new HttpBasicAuthenticator(username, password);

                var requestN4U = new RestRequest("play", Method.GET);
                requestN4U.AddHeader("content-type", "application/json");
                requestN4U.AddHeader("Net4U-Company", "museotortura");
                requestN4U.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenMuseoN4U"));

                requestN4U.Timeout = 60000;

                IRestResponse response = client.Execute(requestN4U);

                Console.WriteLine("GIOCA:" + response.StatusCode + "|" + response.Content);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    JsonUtility ju = new JsonUtility();
                    //StreamReader strm = new StreamReader(Activity.Assets.Open("Gioca.json"));
                    ju.SpacchettamentoJsonGioca(ListGioca, ListResult, response.Content, NSLocale.PreferredLanguages[0].Substring(0, 2));
                }
                else
                {
                    this.RevealViewController().PushFrontViewController(HomePageC, true);
                    //SupportFragmentManager.PopBackStack();
                }
            }

            for (int i = 0; i < ListResult.Count; i++)
            {

                //GiocaTexts [i].Text = ListResult [i].Luogo;
                //GiocaTexts [i].Typeface = tf;
                Console.WriteLine(i + " " + ListResult[i].Luogo + "|" + ListResult[i].IndiceActive);

                if (ListResult[i].Luogo == "lucca")
                {
                    GiocaTexts[i].Text = "Lucca";
                    if (ListResult[i].IndiceActive == 0)
                        GiocaImages[i].Image = UIImage.FromFile("GiocaL.png");
                    else
                        GiocaImages[i].Image = UIImage.FromFile("GiocaN.png");
                }
                if (ListResult[i].Luogo == "siena")
                {
                    GiocaTexts[i].Text = "Siena";
                    if (ListResult[i].IndiceActive == 0)
                        GiocaImages[i].Image = UIImage.FromFile("GiocaS.png");
                    else
                        GiocaImages[i].Image = UIImage.FromFile("GiocaN.png");
                }
                if (ListResult[i].Luogo == "volterra")
                {
                    GiocaTexts[i].Text = "Volterra";
                    if (ListResult[i].IndiceActive == 0)
                        GiocaImages[i].Image = UIImage.FromFile("GiocaV.png");
                    else
                        GiocaImages[i].Image = UIImage.FromFile("GiocaN.png");
                }
                if (ListResult[i].Luogo == "montepulciano")
                {
                    GiocaTexts[i].Text = "Montepulciano";
                    if (ListResult[i].IndiceActive == 0)
                        GiocaImages[i].Image = UIImage.FromFile("GiocaM.png");
                    else
                        GiocaImages[i].Image = UIImage.FromFile("GiocaN.png");
                }
                if (ListResult[i].Luogo == "san giminiano")
                {
                    GiocaTexts[i].Text = "San Giminiano";
                    if (ListResult[i].IndiceActive == 0)
                        GiocaImages[i].Image = UIImage.FromFile("GiocaSG.png");
                    else
                        GiocaImages[i].Image = UIImage.FromFile("GiocaN.png"); ;
                }
                if (ListResult[i].Luogo == "san marino")
                {
                    GiocaTexts[i].Text = "San Marino";
                    if (ListResult[i].IndiceActive == 0)
                        GiocaImages[i].Image = UIImage.FromFile("GiocaSM.png");
                    else
                        GiocaImages[i].Image = UIImage.FromFile("GiocaSMN.png");
                }

            }



            scrollView.Add(TopImage);
            scrollView.Add(TopView);
            scrollView.Add(TopLabel);
            scrollView.Add(SeparatorView);
            scrollView.Add(TitoloLabel);
            scrollView.Add(imageV1);
            scrollView.Add(imageV2);
            scrollView.Add(label1);
            scrollView.Add(label2);
            scrollView.Add(imageV3);
            scrollView.Add(imageV4);
            scrollView.Add(label3);
            scrollView.Add(label4);
            scrollView.Add(imageV5);
            scrollView.Add(imageV6);
            scrollView.Add(label5);
            scrollView.Add(label6);

            scrollView.ContentSize = new CGSize(View.Frame.Width, yy + 10);

            ContentView.Add(scrollView);


            UITapGestureRecognizer imageView1Tap = new UITapGestureRecognizer(() =>
            {
                if (GiocaHomeController.Instance != null)
                    GiocaHomeController.Instance.selected = 0;

                selected = 0;
                if (ListResult[selected].IndiceActive == 0)
                {
                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                    this.RevealViewController().PushFrontViewController(GiocaPage,true);
                }
                if (ListResult[selected].IndiceActive == 1)
                {
                    var adErr = new UIAlertView("Gioca", "Visita il museo per poter giocare", null, "OK", null);
                    adErr.Show();
                }
                if (ListResult[selected].IndiceActive == 2)
                {
                    var adErr = new UIAlertView("Gioca", "Gioca non disponibile per questo museo", null, "OK", null);
                    adErr.Show();
                }

            });
            imageview1.UserInteractionEnabled = true;
            imageview1.AddGestureRecognizer(imageView1Tap);

            UITapGestureRecognizer imageView2Tap = new UITapGestureRecognizer(() =>
            {
                if (GiocaHomeController.Instance != null)
                    GiocaHomeController.Instance.selected = 1;

                selected = 1;
                if (ListResult[selected].IndiceActive == 0)
                {
                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                    this.RevealViewController().PushFrontViewController(GiocaPage, true);
                }
                if (ListResult[selected].IndiceActive == 1)
                {
                    var adErr = new UIAlertView("Gioca", "Visita il museo per poter giocare", null, "OK", null);
                    adErr.Show();
                }
                if (ListResult[selected].IndiceActive == 2)
                {
                    var adErr = new UIAlertView("Gioca", "Gioca non disponibile per questo museo", null, "OK", null);
                    adErr.Show();
                }

            });
            imageview2.UserInteractionEnabled = true;
            imageview2.AddGestureRecognizer(imageView2Tap);

            UITapGestureRecognizer imageView3Tap = new UITapGestureRecognizer(() =>
            {
                if(GiocaHomeController.Instance != null)
                    GiocaHomeController.Instance.selected = 2;

                selected = 2;
                if (ListResult[selected].IndiceActive == 0)
                {
                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                    this.RevealViewController().PushFrontViewController(GiocaPage, true);
                }
                if (ListResult[selected].IndiceActive == 1)
                {
                    var adErr = new UIAlertView("Gioca", "Visita il museo per poter giocare", null, "OK", null);
                    adErr.Show();
                }
                if (ListResult[selected].IndiceActive == 2)
                {
                    var adErr = new UIAlertView("Gioca", "Gioca non disponibile per questo museo", null, "OK", null);
                    adErr.Show();
                }

            });
            imageview3.UserInteractionEnabled = true;
            imageview3.AddGestureRecognizer(imageView3Tap);

            UITapGestureRecognizer imageView4Tap = new UITapGestureRecognizer(() =>
            {
                if (GiocaHomeController.Instance != null)
                    GiocaHomeController.Instance.selected = 3;

                selected = 3;
                if (ListResult[selected].IndiceActive == 0)
                {
                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                    this.RevealViewController().PushFrontViewController(GiocaPage, true);
                }
                if (ListResult[selected].IndiceActive == 1)
                {
                    var adErr = new UIAlertView("Gioca", "Visita il museo per poter giocare", null, "OK", null);
                    adErr.Show();
                }
                if (ListResult[selected].IndiceActive == 2)
                {
                    var adErr = new UIAlertView("Gioca", "Gioca non disponibile per questo museo", null, "OK", null);
                    adErr.Show();
                }

            });
            imageview4.UserInteractionEnabled = true;
            imageview4.AddGestureRecognizer(imageView4Tap);

            UITapGestureRecognizer imageView5Tap = new UITapGestureRecognizer(() =>
            {
                if (GiocaHomeController.Instance != null)
                    GiocaHomeController.Instance.selected = 4;

                selected = 4;
                if (ListResult[selected].IndiceActive == 0)
                {
                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                    this.RevealViewController().PushFrontViewController(GiocaPage, true);
                }
                if (ListResult[selected].IndiceActive == 1)
                {
                    var adErr = new UIAlertView("Gioca", "Visita il museo per poter giocare", null, "OK", null);
                    adErr.Show();
                }
                if (ListResult[selected].IndiceActive == 2)
                {
                    var adErr = new UIAlertView("Gioca", "Gioca non disponibile per questo museo", null, "OK", null);
                    adErr.Show();
                }

            });
            imageview5.UserInteractionEnabled = true;
            imageview5.AddGestureRecognizer(imageView5Tap);

            UITapGestureRecognizer imageView6Tap = new UITapGestureRecognizer(() =>
            {
                if (GiocaHomeController.Instance != null)
                    GiocaHomeController.Instance.selected = 5;

                selected = 5;
                if (ListResult[selected].IndiceActive == 0)
                {
                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                    this.RevealViewController().PushFrontViewController(GiocaPage, true);
                }
                if (ListResult[selected].IndiceActive == 1)
                {
                    var adErr = new UIAlertView("Gioca", "Visita il museo per poter giocare", null, "OK", null);
                    adErr.Show();
                }
                if (ListResult[selected].IndiceActive == 2)
                {
                    var adErr = new UIAlertView("Gioca", "Gioca non disponibile per questo museo", null, "OK", null);
                    adErr.Show();
                }

            });
            imageview6.UserInteractionEnabled = true;
            imageview6.AddGestureRecognizer(imageView6Tap);


        }

        public void setNull()
        {
            GiocaHomeController.Instance = null;
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }
    }
}
