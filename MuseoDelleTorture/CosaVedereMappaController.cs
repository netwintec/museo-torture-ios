﻿using CoreGraphics;
using CoreLocation;
using Foundation;
using Google.Maps;
using Xamarin.SWRevealViewController;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace MuseoDelleTorture
{
	partial class CosaVedereMappaController : UIViewController
	{

        MapView mapView;
        Dictionary<string, MarkerInfo> MarkerDictionary;
        Dictionary<int, MarkerInfo> MarkerDictionaryComplete = new Dictionary<int, MarkerInfo>();
        public MarkerInfo Selected;
        UIStoryboard storyboard;
        List<Marker> MarkerList = new List<Marker>();

        public CosaVedereMappaController (IntPtr handle) : base (handle)
		{
		}

        public static CosaVedereMappaController Instance { get; private set; }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += (sender, e) => this.RevealViewController().RevealToggleAnimated(true);
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
            }

            CosaVedereMappaController.Instance = this;

            MarkerDictionary = HomePage.Instance.MarkerDictionary;
            MarkerDictionaryComplete = HomePage.Instance.MarkerDictionaryComplete;
            CLLocationCoordinate2D myPos = HomePage.Instance.TruePos;

            CameraPosition camera = CameraPosition.FromCamera(myPos, 14);

            mapView = MapView.FromCamera(new CGRect(0, 0, View.Frame.Width, ContentView.Frame.Height), camera);
            mapView.MapType = MapViewType.Normal;
            mapView.Settings.ZoomGestures = true;
            mapView.Settings.CompassButton = true;
            mapView.Settings.MyLocationButton = false;
            mapView.MyLocationEnabled = false;

            for (int i = 0; i < MarkerDictionary.Count; i++)
            {
                MarkerInfo mi = MarkerDictionary["m" + i];
                var Marker = new Marker();        
                Marker.Position = new CLLocationCoordinate2D(mi.lat, mi.lon);
                Marker.Map = mapView;
                if(mi.nome == "PARCHEGGI")
                    Marker.Icon = UIImage.FromFile("marker_mappaP.png");
                else
                    Marker.Icon = UIImage.FromFile("marker_mappa.png");
                Marker.Title = mi.titolo;
                byte[] utf8bytes = UTF8Encoding.UTF8.GetBytes(mi.descrizione);
                string r = UTF8Encoding.UTF8.GetString(utf8bytes, 0, utf8bytes.Length);
                Marker.Snippet = r;
                MarkerList.Add(Marker);
            }

            mapView.InfoTapped += MapView_InfoTapped;
            ContentView.Add(mapView);
        }

        private void MapView_InfoTapped(object sender, GMSMarkerEventEventArgs e)
        {
            Console.WriteLine(e.Marker);
            bool trovato = false;
            for(int i = 0; i < MarkerDictionary.Count; i++)
            {
                MarkerInfo mi = MarkerDictionary["m" + i];
                    if (e.Marker.Position.Latitude == mi.lat && e.Marker.Position.Longitude == mi.lon)
                    {
                        if (mi.nome == "PARCHEGGI")
                        {
                            string url = "http://maps.apple.com/?daddr=" + mi.via;
                            url = url.Replace(" ", "%20");
                            if (UIApplication.SharedApplication.CanOpenUrl(new NSUrl(url)))
                            {
                                UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
                            }
                            else
                            {
                                new UIAlertView("Error", "Maps is not supported on this device", null, "Ok").Show();
                            }
                        }
                        else
                        {
                            Selected = mi;
                            i = MarkerDictionary.Count + 1;
                            trovato = true;
                        }
                    }
            }

            if (trovato)
            {
                UIViewController lp = storyboard.InstantiateViewController("CosaVedereInfo");
                this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                this.PresentModalViewController(lp, true);
            }

        }

        public void selectedMarker(int info)
        {
            MarkerInfo mi = MarkerDictionaryComplete[info];
            if (mi.isHeader == false)
            {
                CLLocationCoordinate2D location = new CLLocationCoordinate2D((double)mi.lat, (double)mi.lon);
                CameraPosition camera = CameraPosition.FromCamera(location, 16);
                mapView.Camera = camera;
                this.RevealViewController().RevealToggleAnimated(true);
                foreach (Marker m in MarkerList) {
                    if (m.Title == mi.nomeE)
                        mapView.SelectedMarker=m;
                }
            }

        }
    }
}
