﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace MuseoDelleTorture
{
    class CustomCellCittaRow: UITableViewCell
    {
        UILabel titolo;
        UIView separator;

        string text;
        public CustomCellCittaRow(NSString cellId,string t) : base(UITableViewCellStyle.Default, cellId)
        {


            text = t;
            SelectionStyle = UITableViewCellSelectionStyle.None;

            separator = new UIView();
            separator.BackgroundColor = UIColor.White;

            titolo = new UILabel()
            {
                Font = UIFont.FromName("MyriadPro-Regular", 14f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                TextColor = UIColor.White,
                Lines = 0,
                BackgroundColor = UIColor.Clear
                
            };

            ContentView.BackgroundColor = UIColor.Black;

            ContentView.AddSubviews(new UIView[] { titolo,separator});

        }
        public void UpdateCell(string tit)
        {
            titolo.Text = tit;
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            
            var size = UIStringDrawing.StringSize(text, UIFont.FromName("MyriadPro-Regular", 14f), new CGSize(HomePage.Instance.RevealWidht - 40, 2000));
            Console.WriteLine(text+"|"+ size.Height + "  " + size.Width);

            titolo.Frame = new CGRect(35, 20-(size.Height/2), HomePage.Instance.RevealWidht - 40, size.Height);
            separator.Frame = new CGRect(35, 38,130,1);

        }
    }

}
