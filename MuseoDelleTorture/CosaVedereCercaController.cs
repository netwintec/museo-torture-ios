using CoreGraphics;
using CoreLocation;
using Foundation;
using RestSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using UIKit;

namespace MuseoDelleTorture
{
    partial class CosaVedereCercaController : UIViewController
    {
        UIStoryboard storyboard;
        CLLocationManager iPhoneLocationManager;

        CLLocationCoordinate2D LuccaPos = new CLLocationCoordinate2D(43.843060, 10.503589);
        CLLocationCoordinate2D SienaPos = new CLLocationCoordinate2D(43.318579, 11.330521);
        CLLocationCoordinate2D SanMarinoPos = new CLLocationCoordinate2D(43.933983, 12.446931);
        //CLLocationCoordinate2D VolterraPos = new CLLocationCoordinate2D(43.401545, 10.860111);
        CLLocationCoordinate2D MontePulcianoPos = new CLLocationCoordinate2D(43.097799, 11.785759);
        CLLocationCoordinate2D SanGiminianoPos = new CLLocationCoordinate2D(43.467471, 11.043171);


        Dictionary<string, MarkerInfo> MarkerDictionary = new Dictionary<string, MarkerInfo>();
        Dictionary<int, MarkerInfo> MarkerDictionaryComplete = new Dictionary<int, MarkerInfo>();

        int i = 0, porta=82;

        public string maps;


        public CosaVedereCercaController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {

            base.ViewDidLoad();

            storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
            }

            Label.Text = NSBundle.MainBundle.LocalizedString("Citta_Cerca", "We\'re checking for your position", null);
            Label.Font = UIFont.FromName("MyriadPro-Regular", 20);

            BackItem.Clicked += delegate {

                iPhoneLocationManager.StopUpdatingLocation();
                iPhoneLocationManager.StopMonitoringSignificantLocationChanges();
                UIViewController lp = storyboard.InstantiateViewController("SWController");
                this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                this.PresentModalViewController(lp, true);

            };

            iPhoneLocationManager = new CLLocationManager();

            iPhoneLocationManager.DesiredAccuracy = 20;
            iPhoneLocationManager.DistanceFilter = 0;
            Console.WriteLine(iPhoneLocationManager.Location);

            iPhoneLocationManager.UpdatedLocation += (object sender, CLLocationUpdatedEventArgs e) => {
                if (i == 1)
                {
                    UpdateLocation(e.NewLocation);
                }
                i++;
            };

            iPhoneLocationManager.LocationsUpdated += IPhoneLocationManager_LocationsUpdated;

            iPhoneLocationManager.Failed += IPhoneLocationManager_Failed1;
            iPhoneLocationManager.PausesLocationUpdatesAutomatically = false;

            //iOS 8 requires you to manually request authorization now - Note the Info.plist file has a new key called requestWhenInUseAuthorization added to.
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                iPhoneLocationManager.RequestWhenInUseAuthorization();

            }

            if (CLLocationManager.LocationServicesEnabled)
            {
                Console.WriteLine("Entro");
                iPhoneLocationManager.StartUpdatingLocation();
                iPhoneLocationManager.StartMonitoringSignificantLocationChanges();
            }
            else
            {
                UIViewController lp = storyboard.InstantiateViewController("SWController");
                this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                this.PresentModalViewController(lp, true);
            }

            AppDelegate.Instance.ScanBeIn = false;

            /*
            var text = System.IO.File.ReadAllText("Json/Marker.json");
            Console.WriteLine(text);

            JsonUtility ju = new JsonUtility();
            ju.SpacchettamentoJsonMarker("lucca", MarkerDictionary, MarkerDictionaryComplete, text, NSLocale.PreferredLanguages[0].Substring(0, 2));

            HomePage.Instance.MarkerDictionary = MarkerDictionary;
            HomePage.Instance.MarkerDictionaryComplete = MarkerDictionaryComplete;
            HomePage.Instance.TruePos = LuccaPos;

            UIButton go = new UIButton(new CGRect(30,30,50,50));
            go.SetTitle("go",UIControlState.Normal);
            go.TouchUpInside += delegate
            {
                
                UIViewController lp = storyboard.InstantiateViewController("CosaVedereReveal");
                this.PresentModalViewController(lp,false);

            };

            ContentView.Add(go);*/

            var client = new RestClient("http://api.netwintec.com:" + porta + "/");
            //client.Authenticator = new HttpBasicAuthenticator(username, password);

            var requestN4U = new RestRequest("play/maps", Method.GET);
            requestN4U.AddHeader("content-type", "application/json");
            requestN4U.AddHeader("Net4U-Company", "museotortura");
            requestN4U.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenMuseoN4U"));

            IRestResponse response = client.Execute(requestN4U);

            Console.WriteLine("Result:" + response.StatusCode + "|" + response.Content);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {

                maps = response.Content;
                
            }
            else
            {

                UIViewController lp = storyboard.InstantiateViewController("SWController");
                this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                this.PresentModalViewController(lp, true);

            }


        }

        private void IPhoneLocationManager_LocationsUpdated(object sender, CLLocationsUpdatedEventArgs e)
        {
            Console.WriteLine("Location change received:"+ e.Locations.Length);
            if (i == 1) {
                UpdateLocation(e.Locations[0]);
            }
            i++;
        }

        private void IPhoneLocationManager_Failed1(object sender, NSErrorEventArgs e)
        {
            Console.WriteLine(e.Error.ToString()); 
        }

        public void UpdateLocation(CLLocation location)
        {
            Console.WriteLine(location.Coordinate.Latitude + " " + location.Coordinate.Longitude);

            List<CLLocationCoordinate2D> listPos = new List<CLLocationCoordinate2D>();
            listPos.Add(LuccaPos);
            listPos.Add(SienaPos);
            listPos.Add(SanMarinoPos);
            listPos.Add(MontePulcianoPos);
            listPos.Add(SanGiminianoPos);
            //listPos.Add(VolterraPos);

            double distance = Double.MaxValue;
            string position = "";
            //Console.WriteLine("Mypos:" + mypos.Latitude + "," + mypos.Longitude);
            for (int ii = 0; ii < listPos.Count; ii++)
            {

                double Finalvalue = DistanceInMetres(location.Coordinate.Latitude, location.Coordinate.Longitude, listPos[ii].Latitude, listPos[ii].Longitude);
                Console.WriteLine(ii + "|" + Finalvalue);

                if (Finalvalue < distance)
                {

                    distance = Finalvalue;
                    switch (ii)
                    {
                        case 0:
                            position = "lucca";
                            break;
                        case 1:
                            position = "siena";
                            break;
                        case 2:
                            position = "san_marino";
                            break;
                        case 3:
                            position = "montepulciano";
                            break;
                        case 4:
                            position = "san_giminiano";
                            break;
                        case 5:
                            position = "volterra";
                            break;

                    }
                    //truePos = listPos[ii];
                    Console.WriteLine(position);
                }
            }

            //new UIAlertView("RISULTATO", "posizione trovata:" + position + "\nDistanza:" + distance + " m", null,"ok", null).Show();
            iPhoneLocationManager.StopUpdatingLocation();
            iPhoneLocationManager.StopMonitoringSignificantLocationChanges();

            //var text = System.IO.File.ReadAllText("Json/Marker.json",System.Text.Encoding.UTF8);
            //Console.WriteLine(text);

            JsonUtility ju = new JsonUtility();
            ju.SpacchettamentoJsonMarker(position, MarkerDictionary, MarkerDictionaryComplete, maps, NSLocale.PreferredLanguages[0].Substring(0, 2));

            HomePage.Instance.MarkerDictionary = MarkerDictionary;
            HomePage.Instance.MarkerDictionaryComplete = MarkerDictionaryComplete;
            if(position == "lucca")
            {
                HomePage.Instance.TruePos = LuccaPos;
            }
            if (position == "siena")
            {
                HomePage.Instance.TruePos = SienaPos;
            }
            if (position == "san_marino")
            {
                HomePage.Instance.TruePos = SanMarinoPos;
            }
            if (position == "san_giminiano")
            {
                HomePage.Instance.TruePos = SanGiminianoPos;
            }
            if (position == "montepulciano")
            {
                HomePage.Instance.TruePos = MontePulcianoPos;
            }

            UIViewController lp = storyboard.InstantiateViewController("CosaVedereReveal");
            this.PresentModalViewController(lp, false);
        }

        public static Double DistanceInMetres(double lat1, double lon1, double lat2, double lon2)
        {

            if (lat1 == lat2 && lon1 == lon2)
                return 0.0;

            var theta = lon1 - lon2;

            var distance = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) +
                           Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) *
                           Math.Cos(deg2rad(theta));

            distance = Math.Acos(distance);
            if (double.IsNaN(distance))
                return 0.0;

            distance = rad2deg(distance);
            distance = distance * 60.0 * 1.1515 * 1609.344;

            return (distance);
        }

        private static double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        private static double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }
    }
}


