using CoreGraphics;
using Foundation;
using Xamarin.SWRevealViewController;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using UIKit;

namespace MuseoDelleTorture
{
    partial class VisitaGuidataPage : UIViewController
    {

        public Dictionary<int, Stanza> StanzeDictionary = new Dictionary<int, Stanza>();
        List<UIImageView> listImage = new List<UIImageView>();

        public string MuseoID = null;

        public VisitaGuidataPage(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += (sender, e) => this.RevealViewController().RevealToggleAnimated(true);
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            HomePage.Instance.isHome = false;
            HomePage.Instance.isMostra = false;
            HomePage.Instance.isVisita = true;

            AppDelegate.Instance.SetCorrectStanzaDictionary(MuseoID);
            StanzeDictionary = AppDelegate.Instance.StanzeDictionary;

            UIScrollView scrollView = new UIScrollView(new CGRect(0, 0, ContentView.Frame.Width, ContentView.Frame.Height));

            int yy = 0;

            for (int i = 0; i < StanzeDictionary.Count; i++)
            {

                Stanza st = StanzeDictionary[i + 1];

                float ImgH = (float)((ContentView.Frame.Width * 1200) / 1900);
                float SfocH = (float)((ContentView.Frame.Width * 360) / 1900);

                UIImageView img = new UIImageView();
                img = new UIImageView(new CGRect(0, yy, ContentView.Frame.Width, ImgH));

                UIImageView sfoc = new UIImageView();
                sfoc = new UIImageView(new CGRect(0, (yy + ImgH) - SfocH, ContentView.Frame.Width, SfocH));
                sfoc.Image = UIImage.FromFile("Sfocatura.png");

                string stanzaTxt = NSBundle.MainBundle.LocalizedString("StanzaH", "ROOM", null) + " " + st.stanza.ToString();
                var size = UIStringDrawing.StringSize(stanzaTxt, UIFont.FromName("DaunPenh", 32), new CGSize(ContentView.Frame.Width - 60, 2000));

                UILabel text = new UILabel();
                text = new UILabel(new CGRect(30, (yy + ImgH) - size.Height - 10, ContentView.Frame.Width - 60, size.Height));
                text.Font = UIFont.FromName("DaunPenh", 32);
                text.Text = stanzaTxt;
                text.TextColor = UIColor.White;
                text.BackgroundColor = UIColor.Clear;
                text.TextAlignment = UITextAlignment.Center;


                if (st.img_file == "Lucca1")
                    img.Image = UIImage.FromFile("stanzaLucca1.jpg");
                if (st.img_file == "Lucca2")
                    img.Image = UIImage.FromFile("stanzaLucca2.jpg");
                if (st.img_file == "Lucca3")
                    img.Image = UIImage.FromFile("stanzaLucca3.jpg");
                if (st.img_file == "Lucca4")
                    img.Image = UIImage.FromFile("stanzaLucca4.jpg");
                if (st.img_file == "Lucca5")
                    img.Image = UIImage.FromFile("stanzaLucca5.jpg");

                if (st.img_file == "SanMarino1")
                    img.Image = UIImage.FromFile("stanzaSanMarino2.jpg");
                if (st.img_file == "SanMarino2")
                    img.Image = UIImage.FromFile("stanzaSanMarino3.jpg");
                if (st.img_file == "SanMarino3")
                    img.Image = UIImage.FromFile("stanzaSanMarino5.jpg");
                if (st.img_file == "SanMarino4")
                    img.Image = UIImage.FromFile("stanzaSanMarino4.jpg");

                if (st.img_file == "Siena1")
                    img.Image = UIImage.FromFile("stanzaSiena1.jpg");
                if (st.img_file == "Siena2")
                    img.Image = UIImage.FromFile("stanzaSiena2.jpg");
                if (st.img_file == "Siena3")
                    img.Image = UIImage.FromFile("stanzaSiena8.jpg");
                if (st.img_file == "Siena4")
                    img.Image = UIImage.FromFile("stanzaSiena9.jpg");
                if (st.img_file == "Siena5")
                    img.Image = UIImage.FromFile("stanzaSiena3.jpg");
                if (st.img_file == "Siena6")
                    img.Image = UIImage.FromFile("stanzaSiena6.jpg");
                if (st.img_file == "Siena7")
                    img.Image = UIImage.FromFile("stanzaSiena4.jpg");
                if (st.img_file == "Siena8")
                    img.Image = UIImage.FromFile("stanzaSiena7.jpg");


                if (st.img_file == "SanGimignanoStr1")
                    img.Image = UIImage.FromFile("SGStrege1.png");
                if (st.img_file == "SanGimignanoStr2")
                    img.Image = UIImage.FromFile("SGStrege2.png");
                if (st.img_file == "SanGimignanoStr3")
                {
                    img.Image = UIImage.FromFile("SGStrege3.png");
                }
                if (st.img_file == "SanGimignanoStr4")
                {
                    img.Image = UIImage.FromFile("SGStrege4.png");
                }


                if (st.img_file == "SanGimignanoPM1")
                    img.Image = UIImage.FromFile("SGMorte1.png");
                if (st.img_file == "SanGimignanoPM2")
                    img.Image = UIImage.FromFile("SGMorte2.png");
                if (st.img_file == "SanGimignanoPM3")
                    img.Image = UIImage.FromFile("SGMorte3.png");
                if (st.img_file == "SanGimignanoPM4")
                    img.Image = UIImage.FromFile("SGMorte4.png");
                if (st.img_file == "SanGimignanoPM5")
                    img.Image = UIImage.FromFile("SGMorte5.png");
                if (st.img_file == "SanGimignanoPM6")
                    img.Image = UIImage.FromFile("SGMorte6.png");

                if (st.img_file == "Volterra1")
                    img.Image = UIImage.FromFile("Volterra1.png");
                if (st.img_file == "Volterra2")
                    img.Image = UIImage.FromFile("Volterra2.png");
                if (st.img_file == "Volterra3")
                    img.Image = UIImage.FromFile("Volterra3.png");
                if (st.img_file == "Volterra4")
                    img.Image = UIImage.FromFile("Volterra4.png");
                if (st.img_file == "Volterra5")
                    img.Image = UIImage.FromFile("Volterra5.png");

                if (st.img_file == "Montepulciano1")
                    img.Image = UIImage.FromFile("Montepulciano1.png");
                if (st.img_file == "Montepulciano2")
                    img.Image = UIImage.FromFile("Montepulciano2.png");
                if (st.img_file == "Montepulciano3")
                    img.Image = UIImage.FromFile("Montepulciano3.png");
                if (st.img_file == "Montepulciano4")
                    img.Image = UIImage.FromFile("Montepulciano4.png");
                if (st.img_file == "Montepulciano5")
                    img.Image = UIImage.FromFile("Montepulciano5.png");

                UITapGestureRecognizer imgTap = new UITapGestureRecognizer((s) =>
                {
                    int giusto = -1;
                    for (int ii = 0; ii < listImage.Count; ii++)
                    {
                        Console.WriteLine((listImage[ii] == s.View) + "|" + ii);
                        if (listImage[ii] == s.View)
                        {
                            giusto = ii;
                        }
                    }

                    AppDelegate.Instance.StanzaFind = giusto + 1;

                    UIStoryboard storyboard = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                    }
                    else
                    {
                        storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
                    }

                    UIViewController BeaconStanza = storyboard.InstantiateViewController("BeaconStanzaHome");

                    HomePage.Instance.isVisita = true;
                    PresentModalViewController(BeaconStanza, true);

                });
                img.UserInteractionEnabled = true;
                img.AddGestureRecognizer(imgTap);


                listImage.Add(img);
                scrollView.Add(img);
                scrollView.Add(sfoc);
                scrollView.Add(text);

                yy += (int)ImgH;

            }

            scrollView.ContentSize = new CGSize(ContentView.Frame.Width, yy + 10);

            ContentView.Add(scrollView);

        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }
    }
}
