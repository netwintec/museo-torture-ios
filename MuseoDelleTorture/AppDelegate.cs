﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;
using Facebook.CoreKit;
using Google.Maps;
using RestSharp;
using Newtonsoft.Json.Linq;
using Xamarin.SWRevealViewController;
using System.Threading;
using Timer = System.Timers.Timer;
using CoreLocation;
using System.Json;
using CoreBluetooth;
using CoreFoundation;
using ZXing.Mobile;

namespace MuseoDelleTorture
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to
    // application events from iOS.

    /* 
            CameraPosition camera = CameraPosition.FromCamera (37.797865, -122.402526, 6);

			mapView = MapView.FromCamera (CGRect.Empty, camera);
			mapView.MyLocationEnabled = true;

			var xamMarker = new Marker () {
				Title = "Xamarin HQ",
				Snippet = "Where the magic happens.",
				Position = new CLLocationCoordinate2D (37.797865, -122.402526),
				Map = mapView
			};

			mapView.SelectedMarker = xamMarker;
    */
    [Register("AppDelegate")]
    public partial class AppDelegate : UIApplicationDelegate
    {

        const string MapsApiKey = "AIzaSyBF9gWvFFCxURjpjZnDTwZgOV2kpb2k5fI";
        bool doIt = false;
        public bool a = false;

        Timer Timer;


        CLLocationManager locationmanager;
        NSUuid beaconUUID;
        CLBeaconRegion beaconRegion;
        const string beaconId = "BlueBeacon";
        const string uuid = "ACFD065E-C3C0-11E3-9BBE-1A514932AC01";

        public bool FirstScan = false;
        public bool LockFirstScan = false;
        public bool TokenGood = false;
        public bool ScanBeIn = false;
        public int StanzaFind = 0;
        public bool Download = false;
        public bool ErrorDownload = false;
        public string PushToken;

        int porta = 82, countSleep = 51, countBeIn = 0;

        bool notification;
        string desc, img;

        Dictionary<int, string> BeaconMusei = new Dictionary<int, string>();
        public Dictionary<int, Stanza> StanzeDictionary = new Dictionary<int, Stanza>();
        public Dictionary<int, Stanza> StanzeDictionaryComplete = new Dictionary<int, Stanza>();
        Dictionary<int, bool> StanzeFlagDictionary = new Dictionary<int, bool>();
        private static Dictionary<string, List<int>> ListBeaconBeIn = new Dictionary<string, List<int>>();
        public static List<string> BeaconNetworks = new List<string>();

        // class-level declarations
        public override UIWindow Window
        {
            get;
            set;
        }

        public override void FinishedLaunching(UIApplication application)
        {
            //base.FinishedLaunching(application);
            MapServices.ProvideAPIKey(MapsApiKey);
            UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.LightContent, false);

            /* CODICE PER RISCHIESTA OTIFICHE ATTUALMENTE INUTILIZZATO
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
                    UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                    new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            }
            else
            {
                UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
            }

            */

            AppDelegate.Instance = this;

            Settings.AppID = "498603803649832";
            Settings.DisplayName = "MuseoDelleTortura";

            BeaconMusei.Add(102, "lucca");
            BeaconMusei.Add(104, "san_marino");
            BeaconMusei.Add(105, "siena");
            BeaconMusei.Add(106, "SanGimignano");
            BeaconMusei.Add(1, "SanGimignanoTorture");
            BeaconMusei.Add(107, "volterra");
            BeaconMusei.Add(108, "montepulciano");



        }

        public static AppDelegate Instance { get; private set; }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {

            //Get current device token
            var DeviceToken = deviceToken.Description;
            Console.WriteLine("PUSH TOKEN1:" + DeviceToken);
            if (!string.IsNullOrWhiteSpace(DeviceToken))
            {
                DeviceToken = DeviceToken.Replace("<", "").Replace(">", "").Replace(" ", "");
            }

            Console.WriteLine("PUSH TOKEN2:" + DeviceToken);

            /* CODICE NOTIFICHE DA RIVEDERE ATTUALMENTE INUTILIZZATO 
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {
                    int i = 6;

                    while (i > 5)
                    {
                        //Console.WriteLine(i);
                        if (!string.IsNullOrWhiteSpace(NSUserDefaults.StandardUserDefaults.StringForKey("TokenMuseoN4U")))
                        {
                            Console.WriteLine("DOIT");
                            doIt = true;
                            i = 0;
                        }

                        i++;
                    }
                    if (doIt)
                    {

                        //********** SEND NOTIFICATION TOKEN ******************
                        var client = new RestClient("http://api.netwintec.com:82/");
                        //client.Authenticator = new HttpBasicAuthenticator(username, password);

                        var requestN4U = new RestRequest("notification", Method.POST);
                        requestN4U.AddHeader("content-type", "application/json");
                        requestN4U.AddHeader("Net4U-Company", "museotortura");
                        requestN4U.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenMuseoN4U"));
                        requestN4U.Timeout = 60000;

                        JObject oJsonObject = new JObject();

                        oJsonObject.Add("uuid", DeviceToken);
                        oJsonObject.Add("type", "ios");

                        requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

                        IRestResponse response = client.Execute(requestN4U);

                        Console.WriteLine("RESPONSE:" + response.StatusCode + "|" + response.Content);
                        //Console.WriteLine("Result:" + response.Content);

                        //************************************************

                    }
                    

                });
            }
            catch (Exception e)
            {
                Console.WriteLine("URL LOAD :" + e.StackTrace);
            }

            */


        }


        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            new UIAlertView("Error registering push notifications", error.LocalizedDescription, null, "OK", null).Show();
        }

        // This method is invoked when the application is about to move from active to inactive state.
        // OpenGL applications should use this method to pause.
        public override void OnResignActivation(UIApplication application)
        {
        }
        // This method should be used to release shared resources and it should store the application state.
        // If your application supports background exection this method is called instead of WillTerminate
        // when the user quits.
        public override void DidEnterBackground(UIApplication application)
        {
        }
        // This method is called as part of the transiton from background to active state.
        public override void WillEnterForeground(UIApplication application)
        {
        }
        // This method is called when the application is about to terminate. Save data, if needed.
        public override void WillTerminate(UIApplication application)
        {
        }
        public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
        {
            // We need to handle URLs by passing them to their own OpenUrl in order to make the SSO authentication works.
            return ApplicationDelegate.SharedInstance.OpenUrl(application, url, sourceApplication, annotation);
        }


        public void StartTimer(long intervalMS)
        {
            Timer = new Timer();
            Timer.Start();
            Timer.Interval = intervalMS;
            Timer.Enabled = true;
            Timer.Elapsed += (object sender, System.Timers.ElapsedEventArgs e) =>
            {
                Timer.Stop();
                InvokeOnMainThread(() =>
                {
                    TimerEnd();
                });
                //Delete time since it will no longer be used.
                Timer.Dispose();
            };
        }

        private void TimerEnd()
        {
            try
            {
                HomePage.Instance.DownloadBeacon(false);

                if (HomePage.Instance.isVisita)
                {
                    HomePage.Instance.isVisita = false;

                    UIStoryboard storyboard2 = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard2 = UIStoryboard.FromName("MainStoryboard_iPhone", null);
                    }
                    else
                    {
                        storyboard2 = UIStoryboard.FromName("MainStoryboard_iPad", null);
                    }

                    UIViewController Home = storyboard2.InstantiateViewController("SWController");


                    TopController().PresentModalViewController(Home, true);

                }

            }
            catch (Exception e2) { }

            Download = false;
            ErrorDownload = false;
            LockFirstScan = false;
        }

        public void GetMuseumData(string museumName, EventHandler onSuccess = null, EventHandler onError = null, long timerTime = -1)
        {
            if (!LockFirstScan)
            {
                LockFirstScan = true;
                Console.WriteLine("CHIEDERE DATI PER IL MUSEO :" + museumName);

                //******** CHIAMATA SALVARE DATI E POI METTERE UNA VARIABILE BOOL A TRUE PER INIZIARE SCAN DATI
                //102 --> lucca
                //104 --> san marino

                var client = new RestClient("http://api.netwintec.com:" + porta + "/");
                //client.Authenticator = new HttpBasicAuthenticator(username, password);

                var requestN4U = new RestRequest("active-messages", Method.GET);
                requestN4U.AddHeader("content-type", "application/json");
                requestN4U.AddHeader("Net4U-Company", "museotortura");
                requestN4U.AddHeader("Net4U-Token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenMuseoN4U"));

                client.ExecuteAsync(requestN4U, (response, arg2) =>
                {

                    InvokeOnMainThread(() =>
                    {
                        Console.WriteLine("Result:" + response.StatusCode + "|" + response.Content);

                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {

                            var lang = NSLocale.PreferredLanguages[0].Substring(0, 2);

                            Console.WriteLine("Lang:" + lang);

                            StanzeDictionary.Clear();
                            StanzeDictionaryComplete.Clear();
                            StanzeFlagDictionary.Clear();

                            try
                            {
                                JsonUtility ju = new JsonUtility();
                                ju.SpacchettamentoJsonBeacon(museumName, StanzeDictionaryComplete, StanzeFlagDictionary, response.Content, lang.ToUpper());
                            }
                            catch (Exception) { }

                            if (StanzeDictionaryComplete.Count != 0)
                            {

                                InvokeOnMainThread(() =>
                                {
                                    if (timerTime != -1)
                                    {
                                        StartTimer(timerTime);
                                    }
                                    else
                                    {
                                        StartTimer(60 * 60 * 1000);
                                    }

                                    HomePage.Instance.DownloadBeacon(false);

                                    if (onSuccess != null)
                                    {
                                        onSuccess(this, new EventArgs());
                                    }

                                });
                            }
                            else
                            {

                                ErrorDownload = true;

                                if (onError != null)
                                {
                                    onError(this, new EventArgs());
                                }

                                Console.WriteLine("DIZIONARIO VUOTOOOOOO");
                            }

                            Download = true;
                        }
                        else
                        {

                            if (onError != null)
                            {
                                onError(this, new EventArgs());
                            }

                            Download = false;

                        }

                        LockFirstScan = false;
                    });

                });

            }
        }

        public void SetCorrectStanzaDictionary(string id)
        {
            StanzeDictionary.Clear();
            if (id != null)
            {
                foreach (var pair in StanzeDictionaryComplete)
                {
                    if (pair.Value.museo == id)
                    {
                        if (pair.Value.museo == "SanGimignanoTorture")
                        {
                            StanzeDictionary.Add(pair.Key - 100, pair.Value);
                        }
                        else
                        {
                            StanzeDictionary.Add(pair.Value.stanza, pair.Value);
                        }

                    }
                }
            }
            else
            {

                StanzeDictionary = new Dictionary<int, Stanza>(StanzeDictionaryComplete);
            }
        }


        public UIViewController TopController()
        {
            UIViewController topController = (UIApplication.SharedApplication).KeyWindow.RootViewController;
            while (topController.PresentedViewController != null)
            {
                topController = topController.PresentedViewController;
            }

            return topController;
        }
    }
}
