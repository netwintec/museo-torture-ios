using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using Xamarin.SWRevealViewController;
using System.Collections.Generic;
using CoreLocation;
using ZXing.Mobile;

namespace MuseoDelleTorture
{
    partial class HomePage : UIViewController
    {
        int yy = 0;
        nfloat ViewWidht;
        double Widht, Height, PaddingX, PaddingY;

        public bool isHome, isMostra, isVisita;
        public float RevealWidht;

        public Dictionary<string, MarkerInfo> MarkerDictionary = new Dictionary<string, MarkerInfo>();
        public Dictionary<int, MarkerInfo> MarkerDictionaryComplete = new Dictionary<int, MarkerInfo>();
        public CLLocationCoordinate2D TruePos;

        UIImageView MostraTempImage;
        VisitaGuidataPage Visita;

        UIView Loading;

        MobileBarcodeScanner scanner;

        UIAlertView alertSanGimi = null;

        bool scanOpen = false;
        bool scanFineOpen = false;

        public HomePage(IntPtr handle) : base(handle)
        {
        }
        public static HomePage Instance { get; private set; }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            scanner = new MobileBarcodeScanner(this.NavigationController);

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                Widht = (View.Frame.Width - 60) / 2;
                PaddingX = 20;
            }
            else
            {
                Widht = (View.Frame.Width - 300) / 2;
                PaddingX = 100;
            }


            Height = Widht;
            PaddingY = (ContentView.Frame.Height - (Height * 3)) / 4;

            Console.WriteLine(View.Frame.Width + "|" + ContentView.Frame.Height + "|" + Widht + "|" + Height + "|" + PaddingX + "|" + PaddingY + "|");

            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            RevealWidht = (float)this.RevealViewController().RightViewRevealWidth;
            MenuItem.Clicked += (sender, e) => this.RevealViewController().RevealToggleAnimated(true);
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
            }
            UIViewController MostraPage = storyboard.InstantiateViewController("MostraPage");
            UIViewController MuseiPage = storyboard.InstantiateViewController("MuseiPage");
            UIViewController GiocaHomePage = storyboard.InstantiateViewController("GiocaHome");
            UIViewController CosaVederePage = storyboard.InstantiateViewController("CosaVedereCercaPage");
            Visita = storyboard.InstantiateViewController("VisitaGuidata") as VisitaGuidataPage;

            //if (HomePage.Instance != null)
            //    if (HomePage.Instance.isVisita)
            //{
            //this.RevealViewController().PushFrontViewController(Visita, true);
            //return;
            //}

            HomePage.Instance = this;
            isHome = true;
            isMostra = false;
            isVisita = false;

            AppDelegate.Instance.TokenGood = true;
            AppDelegate.Instance.ScanBeIn = true;

            UIButton MostraButton = new UIButton(new CGRect(PaddingX, Height + (2 * PaddingY), Widht, Height));
            UIImageView MostraImage = new UIImageView(MostraButton.Bounds);
            MostraImage.Image = UIImage.FromFile("lamostra_icon.png");
            MostraButton.AddSubview(MostraImage);
            MostraButton.TouchUpInside += (object sender, EventArgs e) =>
            {
                this.RevealViewController().PushFrontViewController(MostraPage, true);
            };

            UIButton MuseiButton = new UIButton(new CGRect(Widht + (2 * PaddingX), Height + (2 * PaddingY), Widht, Height));
            UIImageView MuseiImage = new UIImageView(MuseiButton.Bounds);
            MuseiImage.Image = UIImage.FromFile("imusei_icon.png");
            MuseiButton.AddSubview(MuseiImage);
            MuseiButton.TouchUpInside += (object sender, EventArgs e) =>
            {
                this.RevealViewController().PushFrontViewController(MuseiPage, true);
            };

            UIButton MostraTempButton = new UIButton(new CGRect(PaddingX, PaddingY, Widht, Height));
            MostraTempImage = new UIImageView(MostraButton.Bounds);
            MostraTempImage.Image = UIImage.FromFile("visita_on_icon.png");
            MostraTempButton.AddSubview(MostraTempImage);
            MostraTempButton.TouchUpInside += (object sender, EventArgs e) =>
            {

                if (AppDelegate.Instance.Download && !AppDelegate.Instance.ErrorDownload)
                {
                    string MuseumName = "";
                    try
                    {
                        MuseumName = NSUserDefaults.StandardUserDefaults.StringForKey("Museum");
                        if (MuseumName.Contains("SanGimignano"))
                        {
                            alertSanGimi.Show();
                        }
                        else
                        {
                            Visita.MuseoID = null;
                            this.RevealViewController().PushFrontViewController(Visita, true);
                        }
                    }
                    catch (Exception exc) { }
                }
                else
                {
                    if (!AppDelegate.Instance.LockFirstScan)
                    {
                        StartQrCodeReader();
                    }
                }

            };

            UIButton GiocaButton = new UIButton(new CGRect(Widht + (2 * PaddingX), PaddingY, Widht, Height));
            UIImageView GiocaImage = new UIImageView(MuseiButton.Bounds);
            GiocaImage.Image = UIImage.FromFile("gioca_icon.png");
            GiocaButton.AddSubview(GiocaImage);
            GiocaButton.TouchUpInside += (object sender, EventArgs e) =>
            {
                this.RevealViewController().PushFrontViewController(GiocaHomePage, true);
            };


            UIButton CittaButton;

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                CittaButton = new UIButton(new CGRect(PaddingX, (2 * Height) + (3 * PaddingY), (2 * Widht) + PaddingX, Height));
            }
            else
            {
                float w = (float)((Height * 535) / 244);

                CittaButton = new UIButton(new CGRect(View.Frame.Width / 2 - w / 2, (2 * Height) + (3 * PaddingY), w, Height));
            }


            UIImageView CittaImage = new UIImageView(CittaButton.Bounds);
            CittaImage.Image = UIImage.FromFile("cosavedere_icon.png");
            CittaButton.AddSubview(CittaImage);
            CittaButton.TouchUpInside += (object sender, EventArgs e) =>
            {
                this.PresentModalViewController(CosaVederePage, true);
            };


            Loading = new UIView(new CGRect(0, 0, ContentView.Frame.Width, ContentView.Frame.Height));
            Loading.BackgroundColor = new UIColor(0, 0, 0, 100);
            Loading.Alpha = 0;


            UIActivityIndicatorView load = new UIActivityIndicatorView();
            load.Color = UIColor.White;
            load.StartAnimating();
            load.Frame = new CGRect((Loading.Frame.Width / 2) - 10, (Loading.Frame.Height / 2) - 10, 20, 20);
            Loading.AddSubview(load);

            ContentView.AddSubview(MostraButton);
            ContentView.AddSubview(MuseiButton);
            ContentView.AddSubview(MostraTempButton);
            ContentView.AddSubview(GiocaButton);
            ContentView.AddSubview(CittaButton);
            ContentView.AddSubview(Loading);

            string title = "Info";
            string message = NSBundle.MainBundle.LocalizedString("ModalSanGimTitle", "Which museum you want to explore?");

            alertSanGimi = new UIAlertView(title,
                message,
                null,
                NSBundle.MainBundle.LocalizedString("Annulla", "Cancel"),
                "SAN GIMINIANO PENA DI MORTE",
                "SAN GIMINIANO TORTURE E STREGHE");


            alertSanGimi.Clicked += (sender, buttonArgs) =>
            {
                if (buttonArgs.ButtonIndex == 1)
                {
                    Visita.MuseoID = "SanGimignano";
                    this.RevealViewController().PushFrontViewController(Visita, true);
                }
                else if (buttonArgs.ButtonIndex == 2)
                {
                    Visita.MuseoID = "SanGimignanoTorture";
                    this.RevealViewController().PushFrontViewController(Visita, true);
                }

            };

        }

        public async void StartQrCodeReader()
        {
            scanner.UseCustomOverlay = false;

            var options = new MobileBarcodeScanningOptions();
            options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
                    ZXing.BarcodeFormat.QR_CODE
                };

            scanOpen = true;

            ZXing.Result result = null;

            new System.Threading.Thread(new System.Threading.ThreadStart(delegate
            {
                while (result == null)
                {
                    Console.WriteLine("AF");
                    scanner.AutoFocus();
                    System.Threading.Thread.Sleep(2000);
                }
            })).Start();

            result = await scanner.Scan(options);

            if (result != null)
                HandleScanResult(result, 1);
            else
                scanOpen = false;
        }

        void HandleScanResult(ZXing.Result result, int Indice)
        {

            scanOpen = false;

            string msg = "Wrong QRCode scan";

            if (result != null && !string.IsNullOrEmpty(result.Text))
            {

                Loading.Alpha = 1;
                string id = result.Text;
                InvokeOnMainThread(() =>
                {

                    AppDelegate.Instance.GetMuseumData(id,
                     (object sender, EventArgs e) =>
                     {
                         Loading.Alpha = 0;

                         NSUserDefaults.StandardUserDefaults.SetString(id, "Museum");
                         NSUserDefaults.StandardUserDefaults.SetDouble(Utility.FromDateTime(DateTime.Now.AddHours(1)), "EndVisit");
                         NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.AddHours(1).ToString("yyyy-MM-dd HH:mm:ss"), "EndVisit_v2");

                         if (id.Contains("SanGimignano"))
                         {
                             alertSanGimi.Show();
                         }
                         else
                         {
                             Visita.MuseoID = null;
                             this.RevealViewController().PushFrontViewController(Visita, true);
                         }
                     },
                     (object sender, EventArgs e) =>
                     {
                         Loading.Alpha = 0;
                     });



                });
            }
            else
            {
                InvokeOnMainThread(() => { new UIAlertView("Error", msg, null, "Ok", null).Show(); });
            }

        }

        public void DownloadBeacon(bool enter)
        {
            //if (enter)
            MostraTempImage.Image = UIImage.FromFile("visita_on_icon.png");
            //else
            //MostraTempImage.Image = UIImage.FromFile("visita_off_icon.png");
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }
    }
}
