using AVFoundation;
using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Net;
using System.Threading;
using UIKit;

namespace MuseoDelleTorture
{
	partial class BeaconImagesPage : UIViewController, IAVAudioPlayerDelegate
    {
        BeaconInfo oggetto;

        AVAudioPlayer audioPlayer;

        UIImage play_NA = UIImage.FromFile("play.png");
        UIImage play_A = UIImage.FromFile("play_attivo.png");
        UIImage pause_NA = UIImage.FromFile("pause.png");
        UIImage pause_A = UIImage.FromFile("pause_attivo.png");
        UIImage stopImm = UIImage.FromFile("stop.png");

        UIImageView play, pause, stop;
        UIView loadView;

        HttpWebRequest request;

        bool IsPlaying = false;

        public BeaconImagesPage (IntPtr handle) : base (handle)
		{
		}
        public override void ViewDidLoad()
        {

            base.ViewDidLoad();

            oggetto = BeaconStanzaHome.Instance.ObjectSelected;

            int yy = 0;

            var size = UIStringDrawing.StringSize(oggetto.titolo, UIFont.FromName("DaunPenh", 25), new CGSize(ContentView.Frame.Width, 2000));

            UILabel TitoloLabel = new UILabel(new CGRect(0, 12, ContentView.Frame.Width, size.Height));
            TitoloLabel.Font = UIFont.FromName("DaunPenh", 25);
            TitoloLabel.TextAlignment = UITextAlignment.Center;
            TitoloLabel.TextColor = UIColor.White;
            TitoloLabel.Text = oggetto.titolo;
            TitoloLabel.Lines = 0;

            yy += (int)(size.Height + 15);

            float carouselHeight = (float)(((ContentView.Frame.Height - (size.Height + 90)) / 2) - 30);
            float ImageLato = (float)(carouselHeight-2);
            float ScrollHeight = (float)(((ContentView.Frame.Height - (size.Height + 90)) / 2) + 30);

            UIScrollView horizontalScroll = new UIScrollView(new CGRect(0, yy + 5, View.Frame.Width, carouselHeight));

            int xx = 0;
            for (int i = 0; i < oggetto.ListUrlImage.Count; i++)
            {
                UIImageView img = new UIImageView(new CGRect(xx + 5, 0, ImageLato, ImageLato));
                img.Image = UIImage.FromFile("placeholder.png");

                //imageList.Add(img);

                xx += (int)(ImageLato+5);

                SetImageAsync(img, oggetto.ListUrlImage[i]);


                horizontalScroll.Add(img);
            }

            horizontalScroll.ContentSize = new CGSize(xx + 5, carouselHeight);
            //UIImageView image = new UIImageView(new CGRect((ContentView.Frame.Width / 2) - (ImageLato / 2), yy + 10, ImageLato, ImageLato));
            //image.Image = UIImage.FromFile("placeholder.png");

            //SetImageAsync(image, oggetto.urlImage);

            yy += (int)(carouselHeight + 10);

            UIView SeparatorView = new UIView(new CGRect(0, yy, View.Frame.Width, 4));
            SeparatorView.BackgroundColor = UIColor.FromRGB(153, 19, 27);

            yy += 4;

            play = new UIImageView(new CGRect(ContentView.Frame.Width / 2 - 70, yy + 11, 35, 35));
            play.Image = play_NA;

            UITapGestureRecognizer playTap = new UITapGestureRecognizer((s) =>
            {
                if (isHeadsetPluggedIn())
                {

                    if (!IsPlaying && audioPlayer != null)
                    {

                        audioPlayer.Play();
                        play.Image = play_A;
                        pause.Image = pause_NA;
                        IsPlaying = true;

                    }

                }
                else
                {
                    var alert = new UIAlertView("Errore", "Attaccare le cuffie per usufruire dell'audioguida", null, "Ok");
                    alert.Show();
                }
            });
            play.UserInteractionEnabled = true;
            play.AddGestureRecognizer(playTap);

            pause = new UIImageView(new CGRect(ContentView.Frame.Width / 2 - 17.5, yy + 11, 35, 35));
            pause.Image = pause_NA;
            UITapGestureRecognizer pauseTap = new UITapGestureRecognizer((s) =>
            {
                if (IsPlaying && audioPlayer != null)
                {

                    audioPlayer.Pause();
                    play.Image = play_NA;
                    pause.Image = pause_A;
                    IsPlaying = false;

                }
            });
            pause.UserInteractionEnabled = true;
            pause.AddGestureRecognizer(pauseTap);

            stop = new UIImageView(new CGRect(ContentView.Frame.Width / 2 + 35, yy + 11, 35, 35));
            stop.Image = stopImm;

            UITapGestureRecognizer stopTap = new UITapGestureRecognizer((s) =>
            {
                if (audioPlayer != null)
                {

                    audioPlayer.Pause();
                    audioPlayer.CurrentTime = 0.0;
                    play.Image = play_NA;
                    pause.Image = pause_NA;
                    IsPlaying = false;

                }
            });
            stop.UserInteractionEnabled = true;
            stop.AddGestureRecognizer(stopTap);


            loadView = new UIView(new CGRect(0, yy + 5, ContentView.Frame.Width, 43));
            loadView.BackgroundColor = UIColor.Black;

            UIActivityIndicatorView load = new UIActivityIndicatorView(new CGRect(ContentView.Frame.Width / 2 - 17.5, 6, 35, 35));
            load.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.White;
            load.StartAnimating();

            loadView.Add(load);

            yy += 46;

            UIScrollView TextScroll = new UIScrollView(new CGRect(30, yy + 10, ContentView.Frame.Width - 60, ScrollHeight));

            string a = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            NSAttributedStringDocumentAttributes opz = new NSAttributedStringDocumentAttributes();
            NSError err = new NSError();
            opz.DocumentType = NSDocumentType.HTML;
            opz.StringEncoding = NSStringEncoding.UTF8;

            NSAttributedString text = new NSAttributedString("<span style='color:white;font-size:15px'>" + oggetto.descrizione + "</span>", opz, ref err);

            size = UIStringDrawing.StringSize(oggetto.descrizione, UIFont.FromName("MyriadPro-Regular", 17), new CGSize(ContentView.Frame.Width - 60, 2000));

            UILabel Text = new UILabel(new CGRect(0, 0, ContentView.Frame.Width - 60, size.Height));
            Text.Font = UIFont.FromName("MyriadPro-Regular", 17);
            Text.TextAlignment = UITextAlignment.Justified;
            Text.TextColor = UIColor.White;
            Text.AttributedText = text;
            Text.Lines = 0;

            TextScroll.ContentSize = new CGSize(ContentView.Frame.Width - 60, size.Height);

            TextScroll.Add(Text);

            ContentView.Add(TitoloLabel);
            ContentView.Add(horizontalScroll);
            ContentView.Add(SeparatorView);
            ContentView.Add(play);
            ContentView.Add(pause);
            ContentView.Add(stop);
            ContentView.Add(loadView);
            ContentView.Add(TextScroll);

            Console.WriteLine("Headset attached:" + isHeadsetPluggedIn());


            new System.Threading.Thread(new System.Threading.ThreadStart(() => {

                try
                {
                    string url = oggetto.urlaudio;
                    NSUrl fileURL = new NSUrl(url);
                    NSData soundData = NSData.FromUrl(fileURL);
                    NSError oe = new NSError();

                    try
                    {
                        audioPlayer = new AVAudioPlayer(data: soundData, fileTypeHint: "mp3", outError: out oe);
                    }
                    catch (Exception ee)
                    {
                        InvokeOnMainThread(() =>
                        {
                            new UIAlertView("Errore", "Errore nella riproduzione dell'audioguida", null, "Ok", null).Show();
                            Console.WriteLine("Error getting the audio file\n" + ee.StackTrace + "\n" + ee.Message);

                        });
                    }

                    if (audioPlayer != null)
                    {
                        InvokeOnMainThread(() => {

                            audioPlayer.PrepareToPlay();
                            audioPlayer.Volume = 1.0f;
                            audioPlayer.Delegate = this;
                            audioPlayer.Play();
                            audioPlayer.Pause();
                            IsPlaying = false;
                            loadView.Alpha = 0;

                            audioPlayer.FinishedPlaying += delegate
                            {
                                play.Image = play_NA;
                                pause.Image = pause_NA;
                                IsPlaying = false;
                            };

                        });
                    }
                }
                catch (Exception e)
                {
                    InvokeOnMainThread(() =>
                    {
                        new UIAlertView("Errore", "Errore nella riproduzione dell'audioguida", null, "Ok", null).Show();
                        Console.WriteLine("Error getting the audio file\n" + e.StackTrace + "\n" + e.Message);
                    });
                }

            })).Start();

        }
        

        public void SetImageAsync(UIImageView image, string url)
        {
            UIImage Placeholder = UIImage.FromFile("placeholder.png");


            UIImage cachedImage;
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {

                    // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                    //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                    cachedImage = FromUrl(url) ?? Placeholder;


                    InvokeOnMainThread(() =>
                    {
                        image.Image = cachedImage;

                    });

                });
            }
            catch (Exception e)
            {
                Console.WriteLine("URL LOAD :" + e.StackTrace);
            }

        }

        public bool isHeadsetPluggedIn()
        {
            AVAudioSessionRouteDescription route = AVAudioSession.SharedInstance().CurrentRoute;
            foreach (AVAudioSessionPortDescription descr in route.Outputs)
            {
                Console.WriteLine(descr.PortType);
                if (descr.PortType.IsEqual(new NSString("Headphones")))
                    return true;
            }
            return true;
        }

        static UIImage FromUrl(string uri)
        {
            try
            {
                using (var url = new NSUrl(uri))
                using (var data = NSData.FromUrl(url))
                    if (data != null)
                        return UIImage.LoadFromData(data);
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            if (audioPlayer != null)
            {
                audioPlayer.Dispose();
                audioPlayer = null;
            }
        }
    }
}

