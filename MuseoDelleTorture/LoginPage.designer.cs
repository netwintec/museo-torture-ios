﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MuseoDelleTorture
{
    [Register ("LoginPage")]
    partial class LoginPage
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Accedi { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton AccediButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ContenView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Contratto { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView LoginFBView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Oppure { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PasswordBorder { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PasswordText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView UsernameBorder { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField UsernameText { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Accedi != null) {
                Accedi.Dispose ();
                Accedi = null;
            }

            if (AccediButton != null) {
                AccediButton.Dispose ();
                AccediButton = null;
            }

            if (ContenView != null) {
                ContenView.Dispose ();
                ContenView = null;
            }

            if (Contratto != null) {
                Contratto.Dispose ();
                Contratto = null;
            }

            if (LoginFBView != null) {
                LoginFBView.Dispose ();
                LoginFBView = null;
            }

            if (Oppure != null) {
                Oppure.Dispose ();
                Oppure = null;
            }

            if (PasswordBorder != null) {
                PasswordBorder.Dispose ();
                PasswordBorder = null;
            }

            if (PasswordText != null) {
                PasswordText.Dispose ();
                PasswordText = null;
            }

            if (UsernameBorder != null) {
                UsernameBorder.Dispose ();
                UsernameBorder = null;
            }

            if (UsernameText != null) {
                UsernameText.Dispose ();
                UsernameText = null;
            }
        }
    }
}