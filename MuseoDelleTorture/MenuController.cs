using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using Xamarin.SWRevealViewController;

namespace MuseoDelleTorture
{
	partial class MenuController : UITableViewController
	{
        public int selected;

        UIViewController HP;
        UIStoryboard storyboard;
        public MenuController (IntPtr handle) : base (handle)
		{
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            var tableView = View as UITableView;
            
            if (tableView != null)
            {
                tableView.Source = new TableSource(this);
            }

            storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
            }

            HP = storyboard.InstantiateViewController("HomePage");

        }

        public void NonDisp()
        {
            this.RevealViewController().RevealToggleAnimated(true);

            var adErr = new UIAlertView("Non Disponibile", "Pagina non al momento disponibile", null, "OK", null);
            adErr.Show();

        }
        public void ChangePage(int i)
        {

            if (i == 1)
                this.RevealViewController().PushFrontViewController(HP, true);

        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {

            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }


            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }
    }
}
