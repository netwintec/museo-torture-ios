﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace MuseoDelleTorture
{
    class CustomCellCittaHeader : UITableViewCell
    {
        UIImageView image;
        UILabel title;

        string text;
        public CustomCellCittaHeader(NSString cellId, string t) : base(UITableViewCellStyle.Default, cellId)
        {

            text = t;

            SelectionStyle = UITableViewCellSelectionStyle.None;

            image = new UIImageView();

            title = new UILabel()
            {
                Font = UIFont.FromName("DaunPenh", 17f),
                //Font = UIFont.BoldSystemFontOfSize(16),
                TextColor = UIColor.White,
                Lines = 0,
                BackgroundColor = UIColor.Clear

            };

            ContentView.BackgroundColor = UIColor.Black;

            ContentView.AddSubviews(new UIView[] { image,title });

        }
        public void UpdateCell(UIImage imm)
        {
            title.Text = text;
            image.Image = imm;
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            var size = UIStringDrawing.StringSize(text, UIFont.FromName("DaunPenh", 17), new CGSize(ContentView.Frame.Width - 90, 2000));
            Console.WriteLine(size.Height + "  " + size.Width);

            image.Frame = new CGRect(20, 10, 60, 60);
            title.Frame = new CGRect(85, 40-(size.Height/2), ContentView.Frame.Width - 90, size.Height);

        }
    }

}
