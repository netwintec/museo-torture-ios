using CoreGraphics;
using Facebook.CoreKit;
using Facebook.LoginKit;
using Foundation;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using Xamarin.iOS.iCarouselBinding;

namespace MuseoDelleTorture
{
    partial class LoginPage : UIViewController
    {
        bool flagUser, flagPassword;
        LoginButton loginFbButton;
        private UIScrollView scrollView;
        UITextField EMail, Password;
        List<string> readPermissions = new List<string> { "public_profile", "email" };
        int yy = 0;
        nfloat ViewWidht;
        iCarousel carousel;
        UIImageView pallino1, pallino2, pallino3, pallino4;
        LoginManager loginManager;

        public LoginPage(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            ViewWidht = View.Frame.Width;

            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPhone", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("MainStoryboard_iPad", null);
            }

            Profile.Notifications.ObserveDidChange((sender, e) =>
            {

                if (e.NewProfile == null)
                    return;

                //nameLabel.Text = e.NewProfile.Name;
            });

            // Set the Read and Publish permissions you want to get
            loginFbButton = new LoginButton(new CGRect(0, 0, 150, 40))//AccediText.Frame.X+(AccediText.Frame.Width/2) - 60, AccediText.Frame.Y, 120, 40))
            {
                LoginBehavior = LoginBehavior.Native,
                ReadPermissions = readPermissions.ToArray()
            };

            loginManager = new LoginManager();

            // Handle actions once the user is logged in
            loginFbButton.Completed += (sender, e) =>
            {
                bool error = false;

                if (e.Error != null)
                {
                    error = true;
                    // Handle if there was an error
                }

                if (e.Result.IsCancelled)
                {
                    error = true;
                    // Handle if the user cancelled the login request
                }

                if (!error)
                {
                    if (e.Result.Token.TokenString != null)
                    {
                        Console.WriteLine("Token:" + e.Result.Token.TokenString);

                        UtilityLoginManager loginManagerFB = new UtilityLoginManager();
                        List<string> Response = loginManagerFB.LoginFB(e.Result.Token.TokenString);

                        if (Response[0] == "Errore")
                        {
                            loginManager.LogOut();

                            var adErr = new UIAlertView("Errore di rete", "Login non riuscito provate pi� tardi", null, "OK", null);
                            adErr.Show();
                        }

                        if (Response[0] == "SUCCESS")
                        {
                            Console.WriteLine("SUCCESS");
                            //****** SALVA TOKEN ******

                            Console.WriteLine(Response[1]);

                            NSUserDefaults.StandardUserDefaults.SetString(Response[1], "TokenMuseoN4U");
                            //NSUserDefaults.StandardUserDefaults.SetString(e.Result.Token.TokenString, "TokenLovieFB");

                            UIViewController lp = storyboard.InstantiateViewController("SWController");
                            this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                            this.PresentModalViewController(lp, true);
                            //loginButton.ClearPermissions();
                            //loginButton.SetPublishPermissions("publish_actions");

                        }
                    }
                }
                // Handle your successful login
            };

            // Handle actions once the user is logged out
            loginFbButton.LoggedOut += (sender, e) =>
            {
                // Handle your logout
                // nameLabel.Text = "";
            };



            /* If you have been logged into the app before, ask for the your profile name
            if (AccessToken.CurrentAccessToken != null)
            {
                var request = new GraphRequest("/me?fields=name", null, AccessToken.CurrentAccessToken.TokenString, null, "GET");
                request.Start((connection, result, error) => {
                    // Handle if something went wrong with the request
                    if (error != null)
                    {
                        new UIAlertView("Error...", error.Description, null, "Ok", null).Show();
                        return;
                    }

                    // Get your profile name
                    var userInfo = result as NSDictionary;
                    //nameLabel.Text = userInfo["name"].ToString();
                });
            }*/

            /**** V2 ******/

            scrollView = new UIScrollView(new CGRect(0, 0, View.Frame.Width, ContenView.Frame.Height));


            UIImageView imageView = new UIImageView(new CGRect(ViewWidht / 2 - 75, yy + 20, 150, 38));
            imageView.Image = UIImage.FromFile("logo_pagina_iniziale.png");
            yy += 58;

            UILabel Accedi = new UILabel(new CGRect(ViewWidht / 2 - 45, yy + 15, 90, 25));
            Accedi.Text = NSBundle.MainBundle.LocalizedString("LogReg_Accedi", "Login", null);
            Accedi.TextAlignment = UITextAlignment.Center;
            Accedi.TextColor = UIColor.White;
            yy += 40;

            UIView FacebookView = new UIView(new CGRect(ViewWidht / 2 - 60, yy + 10, 120, 40));
            FacebookView.AddSubview(loginFbButton);
            yy += 50;

            UILabel Oppure = new UILabel(new CGRect(ViewWidht / 2 - 30, yy + 10, 60, 25));
            Oppure.Text = NSBundle.MainBundle.LocalizedString("LogReg_Oppure", "or", null);
            Oppure.TextAlignment = UITextAlignment.Center;
            Oppure.TextColor = UIColor.White;
            yy += 35;


            EMail = new UITextField(new CGRect(1, 1, 248, 30));
            EMail.KeyboardType = UIKeyboardType.EmailAddress;
            EMail.Text = "";
            EMail.AttributedPlaceholder = new NSAttributedString(
                "E-Mail",
                font: UIFont.FromName("MyriadPro-Regular", 15),
                foregroundColor: UIColor.White
            );
            EMail.TextColor = UIColor.White;
            EMail.BackgroundColor = UIColor.Black;
            EMail.BorderStyle = UITextBorderStyle.RoundedRect;
            EMail.Font = UIFont.FromName("MyriadPro-Regular", 15);
            EMail.ShouldReturn += (UITextField) =>
            {
                flagUser = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            UIView EmailView = new UIView(new CGRect(ViewWidht / 2 - 125, yy + 5, 250, 32));
            EmailView.Layer.CornerRadius = 7;
            EmailView.BackgroundColor = UIColor.White;
            EmailView.Add(EMail);
            yy += 37;

            Password = new UITextField(new CGRect(1, 1, 248, 30));
            Password.KeyboardType = UIKeyboardType.Default;
            Password.SecureTextEntry = true;
            Password.Text = "";
            Password.AttributedPlaceholder = new NSAttributedString(
                "Password",
                font: UIFont.FromName("MyriadPro-Regular", 15),
                foregroundColor: UIColor.White
            );
            Password.TextColor = UIColor.White;
            Password.BackgroundColor = UIColor.Black;
            Password.BorderStyle = UITextBorderStyle.RoundedRect;
            Password.Font = UIFont.FromName("MyriadPro-Regular", 15);
            Password.ShouldReturn += (UITextField) =>
            {
                flagPassword = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            UIView PasswordView = new UIView(new CGRect(ViewWidht / 2 - 125, yy + 5, 250, 32));
            PasswordView.Layer.CornerRadius = 7;
            PasswordView.BackgroundColor = UIColor.White;
            PasswordView.Add(Password);
            yy += 37;


            //RegisterButton.Frame = ;
            UIButton AccediButton = new UIButton(new CGRect(ViewWidht / 2 - 125, yy + 15, 250, 40));
            AccediButton.SetTitle(NSBundle.MainBundle.LocalizedString("Login_Button", "LOGIN", null), UIControlState.Normal);
            AccediButton.BackgroundColor = UIColor.FromRGB(153, 19, 27);
            AccediButton.Font = UIFont.FromName("MyriadPro-Regular", 18);
            AccediButton.TouchUpInside += delegate
            {

                UtilityLoginManager loginManagerNormal = new UtilityLoginManager();
                List<string> result = loginManagerNormal.Login(EMail.Text, Password.Text);
                Console.WriteLine(result[0]);

                if (result[0] == "SUCCESS")
                {
                    Console.WriteLine(result[1]);

                    NSUserDefaults.StandardUserDefaults.SetString(result[1], "TokenMuseoN4U");

                    //NSUserDefaults.StandardUserDefaults.SetString(e.Result.Token.TokenString, "TokenLovieFB");

                    UIViewController lp = storyboard.InstantiateViewController("SWController");
                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                    this.PresentModalViewController(lp, true);

                }

                if (result[0] == "ERROR")
                {


                    UsernameText.Text = "";
                    PasswordText.Text = "";
                    var adErr = new UIAlertView("Login Non Riuscito", "E-Mail o Password Errata", null, "OK", null);
                    adErr.Show();

                }

                if (result[0] == "ERRORDATA")
                {

                    UsernameText.Text = "";
                    PasswordText.Text = "";
                    var adErr = new UIAlertView("Errore Dati", "E-Mail o Password Non Inseriti o Formato errato", null, "OK", null);
                    adErr.Show();

                }

                //UIViewController lp = storyboard.InstantiateViewController("SWController");
                //this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                //this.PresentModalViewController(lp, true);

            };


            yy += 55;

            UILabel Info = new UILabel(new CGRect(ViewWidht / 2 - 125, yy + 2, 250, 30));
            Info.Text = NSBundle.MainBundle.LocalizedString("LogReg_Contratto", "By clicking the button over here you accept the terms of use of this app", null);
            Info.TextAlignment = UITextAlignment.Center;
            Info.TextColor = UIColor.White;
            Info.Font = UIFont.SystemFontOfSize(12);
            Info.Lines = 2;
            yy += 32;

            UILabel RegisterText = new UILabel(new CGRect(20, yy + 20, ViewWidht - 40, 25));
            RegisterText.Text = "Non hai un account?";
            RegisterText.TextAlignment = UITextAlignment.Center;
            RegisterText.TextColor = UIColor.White;
            yy += 45;

            UIButton BottoneRegistrati = new UIButton(new CGRect(ViewWidht / 2 - 125, yy + 15, 250, 40));
            BottoneRegistrati.SetTitle(NSBundle.MainBundle.LocalizedString("Register_Button", "REGISTER", null), UIControlState.Normal);
            BottoneRegistrati.BackgroundColor = UIColor.FromRGB(153, 19, 27);
            BottoneRegistrati.Font = UIFont.FromName("MyriadPro-Regular", 18);
            BottoneRegistrati.TouchUpInside += delegate
            {

                UIViewController lp = storyboard.InstantiateViewController("RegistratiPage");
                this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                this.PresentModalViewController(lp, true);

            };

            yy += 55;

            scrollView.Add(imageView);
            scrollView.Add(Accedi);
            scrollView.Add(FacebookView);
            scrollView.Add(Oppure);
            scrollView.Add(EmailView);
            scrollView.Add(PasswordView);
            scrollView.Add(AccediButton);
            scrollView.Add(Info);
            scrollView.Add(AccediButton);
            scrollView.Add(RegisterText);
            scrollView.Add(BottoneRegistrati);

            scrollView.ContentSize = new CGSize(View.Frame.Width, yy + 10);

            //base.NavigationController.PushViewController(new LoginPage(Handle),false);
            ContenView.Add(scrollView);


            EMail.ShouldBeginEditing = delegate
            {
                flagUser = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -85;
                    View.Frame = frame;
                }

                return true;
            };

            Password.ShouldBeginEditing = delegate
            {
                flagPassword = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -85;
                    View.Frame = frame;
                }

                return true;
            };

            EMail.ShouldEndEditing = delegate
            {

                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagUser == false)
                    View.Frame = frame;
                return true;
            };

            Password.ShouldEndEditing = delegate
            {
                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagPassword == false)
                    View.Frame = frame;
                return true;
            };


            /**************
            // Add views to main view
            LoginFBView.AddSubview(loginFbButton);

            Accedi.Text = NSBundle.MainBundle.LocalizedString("LogReg_Accedi", "Login", null);
            AccediButton.SetTitle(NSBundle.MainBundle.LocalizedString("Login_Button", "LOGIN", null), UIControlState.Normal);
            AccediButton.Font = UIFont.FromName("MyriadPro-Regular", 18);
            Oppure.Text = NSBundle.MainBundle.LocalizedString("LogReg_Oppure", "or", null);
            Contratto.Text = NSBundle.MainBundle.LocalizedString("LogReg_Contratto", "By clicking the button over here you accept the terms of use of this app", null);

            AccediButton.TouchUpInside += delegate {

                
                UtilityLoginManager loginManagerNormal = new UtilityLoginManager();
                List<string> result = loginManagerNormal.Login(UsernameText.Text, PasswordText.Text);
                Console.WriteLine(result[0]);

                if (result[0] == "SUCCESS")
                {
                    Console.WriteLine(result[1]);

                    NSUserDefaults.StandardUserDefaults.SetString(result[1], "TokenMuseoN4U");

                    //NSUserDefaults.StandardUserDefaults.SetString(e.Result.Token.TokenString, "TokenLovieFB");

                    UIViewController lp = storyboard.InstantiateViewController("SWController");
                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                    this.PresentModalViewController(lp, true);

                }

                if (result[0] == "ERROR")
                {


                    UsernameText.Text = "";
                    PasswordText.Text = "";
                    var adErr = new UIAlertView("Login Non Riuscito", "E-Mail o Password Errata", null, "OK", null);
                    adErr.Show();

                }

                if (result[0] == "ERRORDATA")
                {

                    UsernameText.Text = "";
                    PasswordText.Text = "";
                    var adErr = new UIAlertView("Errore Dati", "E-Mail o Password Non Inseriti o Formato errato", null, "OK", null);
                    adErr.Show();

                }

                //UIViewController lp = storyboard.InstantiateViewController("SWController");
                //this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                //this.PresentModalViewController(lp, true);

            };


            UsernameBorder.Layer.CornerRadius = 7;

            UsernameText.Font = UIFont.FromName("MyriadPro-Regular", 15);
            UsernameText.AttributedPlaceholder = new NSAttributedString(
                "E-Mail",
                font: UIFont.FromName("MyriadPro-Regular", 15),
                foregroundColor: UIColor.White
            //strokeWidth: 4
            );
            UsernameText.ShouldReturn += (UITextField) => {
                flagUser = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            PasswordBorder.Layer.CornerRadius = 7;

            PasswordText.Font = UIFont.FromName("MyriadPro-Regular", 15);
            PasswordText.AttributedPlaceholder = new NSAttributedString(
                "Password",
                font: UIFont.FromName("MyriadPro-Regular", 15),
                foregroundColor: UIColor.White
            //strokeWidth: 4
            );
            PasswordText.ShouldReturn += (UITextField) => {
                flagPassword = false;
                UITextField.ResignFirstResponder();
                return true;
            };

            UsernameText.ShouldBeginEditing = delegate {
                flagUser = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -85;
                    View.Frame = frame;
                }

                return true;
            };

            PasswordText.ShouldBeginEditing = delegate {
                flagPassword = true;
                CGRect frame = View.Frame;
                if (View.Frame.Height == 480)
                {
                    frame.Y = -85;
                    View.Frame = frame;
                }

                return true;
            };

            UsernameText.ShouldEndEditing = delegate {

                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagUser == false)
                    View.Frame = frame;
                return true;
            };

            PasswordText.ShouldEndEditing = delegate {
                CGRect frame = View.Frame;
                frame.Y = 0;
                if (flagPassword == false)
                    View.Frame = frame;
                return true;
            };




            /* VIEW PER CAROUSEL */

            UIView PopUp = new UIView(new CGRect(0, 0, ContenView.Frame.Width, ContenView.Frame.Height));
            PopUp.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 200);

            float w = (float)ContenView.Frame.Width - 100;
            float h = (w * 850) / 695;

            UIView Container = new UIView(new CGRect((ContenView.Frame.Width / 2) - (w / 2), (ContenView.Frame.Height / 2) - (h / 2), w, h));
            Container.BackgroundColor = UIColor.Clear;


            List<string> app = new List<string>();
            app.Add("Help1.png");
            app.Add("Help2.png");
            app.Add("Help3.png");
            app.Add("Help4.png");

            carousel = new iCarousel();
            carousel.Frame = new CGRect(0, 0, Container.Frame.Width, Container.Frame.Height);
            carousel.Type = iCarouselType.CoverFlow2;
            carousel.DataSource = new CarouselDataSource(4, (float)Container.Frame.Width, (float)((Container.Frame.Width * 850) / 695), app, 0);
            carousel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
            //carousel. += Carousel_CurrentItemIndexChanged;

            UIImageView ImageX = new UIImageView(new CGRect((ContenView.Frame.Width / 2) + (w / 2) + 10, (ContenView.Frame.Height / 2) - (h / 2) - 35, 23, 23));
            ImageX.Image = UIImage.FromFile("X_popup_icon.png");

            UITapGestureRecognizer imageXTap = new UITapGestureRecognizer(() =>
            {
                PopUp.RemoveFromSuperview();
            });
            ImageX.UserInteractionEnabled = true;
            ImageX.AddGestureRecognizer(imageXTap);

            pallino1 = new UIImageView(new CGRect((ContenView.Frame.Width / 2) - 16, (ContenView.Frame.Height / 2) + (h / 2) + 10, 5, 5));
            pallino1.Image = UIImage.FromFile("pallinorosso.png");

            pallino2 = new UIImageView(new CGRect((ContenView.Frame.Width / 2) - 7, (ContenView.Frame.Height / 2) + (h / 2) + 10, 5, 5));
            pallino2.Image = UIImage.FromFile("pallinobianco.png");

            pallino3 = new UIImageView(new CGRect((ContenView.Frame.Width / 2) + 2, (ContenView.Frame.Height / 2) + (h / 2) + 10, 5, 5));
            pallino3.Image = UIImage.FromFile("pallinobianco.png");

            pallino4 = new UIImageView(new CGRect((ContenView.Frame.Width / 2) + 11, (ContenView.Frame.Height / 2) + (h / 2) + 10, 5, 5));
            pallino4.Image = UIImage.FromFile("pallinobianco.png");

            Container.Add(carousel);
            PopUp.Add(ImageX);
            PopUp.Add(Container);
            PopUp.Add(pallino1);
            PopUp.Add(pallino2);
            PopUp.Add(pallino3);
            PopUp.Add(pallino4);
            ContenView.Add(PopUp);

            // Perform any additional setup after loading the view, typically from a nib.
        }

        private void Carousel_CurrentItemIndexChanged(object sender, EventArgs e)
        {
            int index = (int)carousel.CurrentItemIndex;
            ChangePallino(index);
        }

        public void ChangePallino(int i)
        {
            if (i == 0)
            {
                pallino1.Image = UIImage.FromFile("pallinorosso.png");
                pallino2.Image = UIImage.FromFile("pallinobianco.png");
                pallino3.Image = UIImage.FromFile("pallinobianco.png");
                pallino4.Image = UIImage.FromFile("pallinobianco.png");
            }
            if (i == 1)
            {
                pallino2.Image = UIImage.FromFile("pallinorosso.png");
                pallino1.Image = UIImage.FromFile("pallinobianco.png");
                pallino3.Image = UIImage.FromFile("pallinobianco.png");
                pallino4.Image = UIImage.FromFile("pallinobianco.png");
            }
            if (i == 2)
            {
                pallino3.Image = UIImage.FromFile("pallinorosso.png");
                pallino2.Image = UIImage.FromFile("pallinobianco.png");
                pallino1.Image = UIImage.FromFile("pallinobianco.png");
                pallino4.Image = UIImage.FromFile("pallinobianco.png");
            }
            if (i == 3)
            {
                pallino4.Image = UIImage.FromFile("pallinorosso.png");
                pallino2.Image = UIImage.FromFile("pallinobianco.png");
                pallino3.Image = UIImage.FromFile("pallinobianco.png");
                pallino1.Image = UIImage.FromFile("pallinobianco.png");
            }
        }
    }

    public class CarouselDataSource : iCarouselDataSource
    {
        int[] items;
        float Widht, Height;
        List<string> listImg;
        int indice;
        public CarouselDataSource(int number, float w, float h, List<string> lI, int i)
        {
            // create our amazing data source
            items = Enumerable.Range(number, number).ToArray();
            Widht = w;
            Height = h;
            listImg = lI;
            indice = i;//(w * 850) / 695;
        }

        public override nint NumberOfItemsInCarousel(iCarousel carousel)
        {
            return items.Length;
        }

        public override UIView ViewForItemAtIndex(iCarousel carousel, nint index, UIView view)
        {
            UIImageView imageView = null;
            UIView View = null;

            if (view == null)
            {

                View = new UIView(new CGRect(0, 0, Widht, Height));
                if (indice == 0)
                    View.BackgroundColor = UIColor.FromRGB(153, 19, 27);
                if (indice == 1)
                    View.BackgroundColor = UIColor.FromRGB(255, 255, 255);

                imageView = new UIImageView(new CGRect(2, 2, Widht - 4, Height - 4));
                imageView.Tag = 2;

                UIImage cachedImage = null;

                cachedImage = UIImage.FromFile(listImg[(int)index]);

                /*if (index == 0) 
                    cachedImage = UIImage.FromFile("Help1.png");

                if (index == 1)
                    cachedImage = UIImage.FromFile("Help2.png");

                if (index == 2)
                    cachedImage = UIImage.FromFile("Help3.png");

                if (index == 3)
                    cachedImage = UIImage.FromFile("Help4.png");*/

                imageView.Image = cachedImage;



                View.AddSubview(imageView);
            }
            else
            {
                // get a reference to the label in the recycled view
                View = view;
                imageView = (UIImageView)view.ViewWithTag(2);


                //label = (UILabel)view.ViewWithTag(1);
            }

            // set the values of the view

            return View;
        }
    }

}
