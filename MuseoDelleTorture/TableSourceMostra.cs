﻿using System;
using System.Collections.Generic;
using System.Text;
using Foundation;
using UIKit;
using CoreGraphics;

namespace MuseoDelleTorture
{
    class TableSourceMostra : UITableViewSource
    {

        public const string TEXT_EN_1 = "The exhibit continues to achieve resounding success thanks to its strong and clear historical value and includes unique instruments."+
            " An exhibit which doesn’t need to emphasize its message through bloody representation and horror scenes: instruments speaks for themselves."+
            " It’s an unique exhibit because of its interpretation and its strong effect on visitors. The horror aroused in our visitors viewing the instruments allows us to make them our allies against torture."+
            " The exhibit wants to be a great testimony and doesn’t linger in the past but becomes a living matter."+
            " It lays bare the worst side of human nature: every man hides and holds back a potential butcher."+
            " If displayed with great accuracy and propriety, the exhibit helps to fuel a sympathetic awareness on the matter, and to respect the opinions and beliefs different from ours, that is the elementary foundation of modern democracy.";
        public const string TEXT_EN_2 = "This exhibit includes images and words, a unique world-wide collection: 100 instruments produced for causing pain and death."+
            "\n\n"+
            "It shows exceptionally rare pieces dated back to the XVI, XVII and XVIII century and more recent philological reconstructions of ancient and lost instruments." + 
            "\n\n"+
            "Very well known instruments such as the Iron Maiden, the Guillotine, the Rack, the Interrogation Chair and the Chastity Belt" +
            "\n\n"+
            "But the peculiarity of the exhibit is showing to the public for the very first time less known but very refined instruments. From the Heretic’s Fork to the Noise-maker’s fifes, to the Gatta da Scorticamento to the Spanish Spiders, these instruments prove how human imagination and sophisticated intelligence had no limit in finding new ways to inflict the most atrocious and cruel tortures.";
        public const string TEXT_EN_3 = "Few phenomenons kept their essence intact through time as the subject of this exhibit. From the Jock’s Mare or Skull Splitter to the shock treatment or psychiatric drugs, there is always been a long series of new instruments, so it is hard to talk about progress or evolution." +
            "\n\n"+
            "Through this appalling journey into the machines used to cause death, public mockery and pain, the exhibit shows horrors that our conscience has repressed but that had been part of human coexistence for many centuries instead. Famous tortures that make us shiver but demonstrate how men applied as much creativity into the field of technology and into finding new ways to inflict pain as into arts and culture." +
            "\n\n"+
            "The purpose of the exhibit is practicing our memory, documenting the aberrations of intolerance and zealotry which men achieved in their clear headed delirium to intentionally provoke harm and death.";
        public const string TEXT_EN_4 = "Already used since ancient times in all cultures, torture is a physical and psychological method of coercion, inflicted to punish or extort information or confessions." +
            "\n\n"+
            "Torture has been condemned, repudiated and contested for its theoretical justifications, and it reappears under many shapes and motivations forcing us to think about the dynamics which generate it. Condemned by everyone but yet still used, why?" +
            "\n\n"+
            "Indeed, it’s wrong to imagine torture as an historical fact, as an ancient or delimited custom, as a procedure overcome by social, political and moral evolution. Actually torture knows no era, no framework and no power, both secular or religious. Causing pain seems to be an irrepressible human desire. Human cruelty, the pleasure derived from other people’s pain, the desire to impose our criteria without respecting the freedom of others, these are not behaviors delimited to a certain era but to human history.";

        public const string TEXT_IT_1 = "Una mostra che continua a riscuotere grandi consensi da parte del pubblico per la sua forte e chiara valenza storica e che annovera strumenti unici al mondo."+
            "Una mostra che non ha bisogno di enfatizzare, attraverso rappresentazioni di sangue o scene di orrore, il suo messaggio: gli strumenti parlano da soli."+
            "Una mostra unica per la sua chiave di lettura e con un contenuto di forte impatto sui visitatori.Una mostra in cui l’orrore suscitato negli spettatori alla visione degli strumenti ci permette di renderli nostri alleati nella lotta contro la tortura."+
            "Una mostra che vuole essere una forte testimonianza e che non rimane circoscritta nella storia ma che diventa di grande attualità.Una mostra che mette a nudo il lato peggiore della natura umana: in ogni uomo si nasconde e latita un potenziale carnefice."+
            "Una mostra a conferma del fatto che, se presentata con grande rigore e correttezza, aiuta a fomentare una coscienza solidale e a rispettare l’opinione e il credo di chi la pensa diversamente da noi, base primordiale dei sistemi democratici dell’epoca moderna.";

        public const string TEXT_IT_2 = "Questa Esposizione racchiude in immagini e parole una collezione unica al mondo: 100 strumenti disegnati per torturare ed uccidere." + 
            "\n\n" +
            "La raccolta comprende pezzi d’eccezionale rarità, risalenti al XVI, XVII e XVIII secolo, e ricostruzioni filologiche, dell’Otto e Novecento, di originali antichi e introvabili." + 
            "\n\n" +
            "Strumenti alcuni molto conosciuti quali la Vergine di Norimberga, la Ghigliottina, il Banco di Stiramento, la Sedia Inquisitoria e la Cintura di Castità." + 
            "\n\n" +
            "Ma la singolarità di questa esposizione sta nel presentare per la prima volta al pubblico strumenti sicuramente meno famosi, ma incredibilmente sofisticati.Strumenti questi che, dalla Forcella dell’Eretico al Piffero del Baccanaro, dalla Gatta da Scorticamento ai Ragni Spagnoli,dimostrano quanto la fantasia umana ed il suo raffinato ingegno non abbia conosciuto limiti nella ricerca di sistemi atti ad infliggere le più atroci e crudeli torture.";

        public const string TEXT_IT_3 = "Pochi fenomeni come quello trattato in questa Mostra, hanno mantenuto la propria essenza inalterata nel tempo.Dal cavalletto o dallo schiacciatesta alle scariche elettriche o alla somministrazione di psicofarmaci che alterano il dominio del corpo, c’è stata una lunga serie di novità, è difficile parlare di evoluzione o progresso." + 
            "\n\n" +
            "Attraverso un agghiacciante viaggio tra gli strumenti di esecuzione capitale,tortura e pubblico ludibrio questa insolita esposizione racconta una storia di orrori che la nostra coscienza ha rimosso e che invece per molti secoli furono parte integrante dell’umana convivenza.Tutti supplizi famigerati che a vederli oggi fanno rabbrividire ma che dimostrano come l’uomo abbia impiegato nel campo delle tecniche e delle pratiche atte ad infliggere dolore un’ inventiva che in nulla é minore a quella che ha saputo porre nel pensiero e nelle arti." + 
            "\n\n" +
            "La finalità della mostra è proprio l’esercizio della memoria, allo scopo di documentare le aberrazioni dell’intolleranza e del fanatismo di cui l’uomo è capace quando,nel suo lucido delirio, vuole provocare intenzionalmente sofferenza e morte ad altri esseri umani.";

        public const string TEXT_IT_4 = "Pochi fenomeni come quello trattato in questa Mostra, hanno mantenuto la propria essenza inalterata nel tempo.Dal cavalletto o dallo schiacciatesta alle scariche elettriche o alla somministrazione di psicofarmaci che alterano il dominio del corpo, c’è stata una lunga serie di novità, è difficile parlare di evoluzione o progresso." + 
            "\n\n" +
            "Attraverso un agghiacciante viaggio tra gli strumenti di esecuzione capitale, tortura e pubblico ludibrio questa insolita esposizione racconta una storia di orrori che la nostra coscienza ha rimosso e che invece per molti secoli furono parte integrante dell’umana convivenza.Tutti supplizi famigerati che a vederli oggi fanno rabbrividire ma che dimostrano come l’uomo abbia impiegato nel campo delle tecniche e delle pratiche atte ad infliggere dolore un’ inventiva che in nulla é minore a quella che ha saputo porre nel pensiero e nelle arti." + 
            "\n\n" +
            "La finalità della mostra è proprio l’esercizio della memoria, allo scopo di documentare le aberrazioni dell’intolleranza e del fanatismo di cui l’uomo è capace quando, nel suo lucido delirio, vuole provocare intenzionalmente sofferenza e morte ad altri esseri umani.";



        int righe;
        bool SelFirst = false;
        bool SelSecond = false;
        bool SelThird = false;
        bool SelFourth = false;
        bool isPhone;

        float widht;

        float TitleHeight1, TitleHeight2, TitleHeight3, TitleHeight4;

        MostraController  super;

        CGSize size1, size2, size3, size4;

        public string TEXT_1, TEXT_2, TEXT_3, TEXT_4;
        public TableSourceMostra(float w,MostraController s)
        {
            righe = 8;
            widht = w;
            super = s;

            if(NSLocale.PreferredLanguages[0].Substring(0,2).CompareTo("it") == 0)
            {
                TEXT_1 = TEXT_IT_1;
                TEXT_2 = TEXT_IT_2;
                TEXT_3 = TEXT_IT_3;
                TEXT_4 = TEXT_IT_4;
            }
            else
            {
                TEXT_1 = TEXT_EN_1;
                TEXT_2 = TEXT_EN_2;
                TEXT_3 = TEXT_EN_3;
                TEXT_4 = TEXT_EN_4;
            }

            size1 = UIStringDrawing.StringSize(TEXT_1, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht, 2000));
            //Console.WriteLine(size1.Height + "  " + size1.Width);

            size2 = UIStringDrawing.StringSize(TEXT_2, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht, 2000));
            //Console.WriteLine(size1.Height + "  " + size1.Width);

            size3 = UIStringDrawing.StringSize(TEXT_3, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht, 2000));
            //Console.WriteLine(size1.Height + "  " + size1.Width);

            size4 = UIStringDrawing.StringSize(TEXT_4, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht, 2000));
            //Console.WriteLine(size1.Height + "  " + size1.Width);

            var sizeTit = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Mostra_Titolo1", "THE EXIBITH", null), UIFont.FromName("DaunPenh", 20), new CGSize(widht - 25, 2000));
            if (sizeTit.Height > 30)
            {
                super.yy += 30;
                super.addHeight(30);
            }

            sizeTit = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Mostra_Titolo2", "ANCIENT INSTRUMENT OF TORTURE", null), UIFont.FromName("DaunPenh", 20), new CGSize(widht - 25, 2000));
            if (sizeTit.Height > 30)
            {
                super.yy += 30;
                super.addHeight(30);
            }
            
            sizeTit = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Mostra_Titolo3", "A JOURNEY THROUGH HUMAN CRUELTY", null), UIFont.FromName("DaunPenh", 20), new CGSize(widht - 25, 2000));
            if (sizeTit.Height > 30)
            {
                super.yy += 30;
                super.addHeight(30);
            }

            sizeTit = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Mostra_Titolo4", "THE WORST SIDE OF MANKIND", null), UIFont.FromName("DaunPenh", 20), new CGSize(widht - 25, 2000));
            if (sizeTit.Height > 30)
            {
                super.yy += 30;
                super.addHeight(30);
            }
            
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return righe;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {

            if (indexPath.Row == 0)
            {
                if (SelFirst)
                {
                    SelFirst = false;
                    super.CloseChange((float)size1.Height + 8);
                }
                else
                {
                    SelFirst = true;
                    super.OpenChange((float)size1.Height+8);
                }
            }
            if (indexPath.Row == 2)
            {
                if (SelSecond)
                {
                    SelSecond = false;
                    super.CloseChange((float)size2.Height + 8);
                }
                else
                {
                    SelSecond = true;
                    super.OpenChange((float)size2.Height + 8);
                }
            }
            if (indexPath.Row == 4)
            {
                if (SelThird)
                {
                    SelThird = false;
                    super.CloseChange((float)size3.Height + 8);
                }
                else
                {
                    SelThird = true;
                    super.OpenChange((float)size3.Height + 8);
                }
            }
            if (indexPath.Row == 6)
            {
                if (SelFourth)
                {
                    SelFourth = false;
                    super.CloseChange((float)size4.Height + 8);
                }
                else
                {
                    SelFourth = true;
                    super.OpenChange((float)size4.Height + 8);
                }
            }

            tableView.ReloadData();
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            
            int row = indexPath.Row;
            if (row == 1 && SelFirst) {
                var size = UIStringDrawing.StringSize(TEXT_1, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht, 2000));
                //Console.WriteLine(size.Height + "  " + size.Width);
                return (nfloat)(size.Height + 8);
            }
            if (row == 1 && !SelFirst)
            {
                return 0;
            }
            if (row == 3 && SelSecond)
            {
                var size = UIStringDrawing.StringSize(TEXT_2, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht, 2000));
                //Console.WriteLine(size.Height + "  " + size.Width);
                return (nfloat)(size.Height + 8);
            }
            if (row == 3 && !SelSecond)
            {
                return 0;
            }
            if (row == 5 && SelThird)
            {
                var size = UIStringDrawing.StringSize(TEXT_3, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht, 2000));
                //Console.WriteLine(size.Height + "  " + size.Width);
                return (nfloat)(size.Height + 8);
            }
            if (row == 5 && !SelThird)
            {
                return 0;
            }
            if (row == 7 && SelFourth)
            {
                var size = UIStringDrawing.StringSize(TEXT_4, UIFont.FromName("MyriadPro-Regular", 14), new CGSize(widht, 2000));
                //Console.WriteLine(size.Height + "  " + size.Width);
                return (nfloat)(size.Height + 8);
            }
            if (row == 7 && !SelFourth)
            {
                return 0;
            }

            float NormalHeight = 40;

            if(row == 0)
            {
                var sizeTit =UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Mostra_Titolo1", "THE EXIBITH", null), UIFont.FromName("DaunPenh", 20), new CGSize(widht -25, 2000));
                Console.WriteLine(row + " : " + sizeTit.Width + " " + sizeTit.Height);
                if (sizeTit.Height > 30)
                {
                    NormalHeight += 30;
                }
                TitleHeight1 = NormalHeight;
            }
            if (row == 2)
            {
                var sizeTit = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Mostra_Titolo2", "ANCIENT INSTRUMENT OF TORTURE", null), UIFont.FromName("DaunPenh", 20), new CGSize(widht - 25, 2000));
                Console.WriteLine(row + " : " + sizeTit.Width + " " + sizeTit.Height);
                if (sizeTit.Height > 30)
                {
                    NormalHeight += 30;
                }
                TitleHeight2 = NormalHeight;
            }
            if (row == 4)
            {
                var sizeTit = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Mostra_Titolo3", "A JOURNEY THROUGH HUMAN CRUELTY", null), UIFont.FromName("DaunPenh", 20), new CGSize(widht - 25, 2000));
                Console.WriteLine(row + " : " + sizeTit.Width + " " + sizeTit.Height);
                if (sizeTit.Height > 30)
                {
                    NormalHeight += 30;
                }
                TitleHeight3 = NormalHeight;
            }
            if (row == 6)
            {
                var sizeTit = UIStringDrawing.StringSize(NSBundle.MainBundle.LocalizedString("Mostra_Titolo4", "THE WORST SIDE OF MANKIND", null), UIFont.FromName("DaunPenh", 20), new CGSize(widht - 25, 2000));
                Console.WriteLine(row+" : "+sizeTit.Width + " " + sizeTit.Height);
                if (sizeTit.Height > 30)
                {
                    NormalHeight += 30;
                    //super.OpenChange(20);
                }
                TitleHeight4 = NormalHeight;
            }

            return (nfloat)NormalHeight;

        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            int row = indexPath.Row;

            if(row == 0 && SelFirst)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraTitolo cell = new CustomCellMostraTitolo(cellIdentifier, true, NSBundle.MainBundle.LocalizedString("Mostra_Titolo1", "THE EXIBITH", null),TitleHeight1);
                cell.UpdateCell(NSBundle.MainBundle.LocalizedString("Mostra_Titolo1", "THE EXIBITH", null)
                                , UIImage.FromFile("freccia_giu.png"));
                return cell;
            }
            if (row == 0 && !SelFirst)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraTitolo cell = new CustomCellMostraTitolo(cellIdentifier, false, NSBundle.MainBundle.LocalizedString("Mostra_Titolo1", "THE EXIBITH", null), TitleHeight1);
                cell.UpdateCell(NSBundle.MainBundle.LocalizedString("Mostra_Titolo1", "THE EXIBITH", null)
                                , UIImage.FromFile("freccia_su.png"));
                return cell;
            }

            if (row == 2 && SelSecond)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraTitolo cell = new CustomCellMostraTitolo(cellIdentifier, true, NSBundle.MainBundle.LocalizedString("Mostra_Titolo2", "ANCIENT INSTRUMENT OF TORTURE", null), TitleHeight2);
                cell.UpdateCell(NSBundle.MainBundle.LocalizedString("Mostra_Titolo2", "ANCIENT INSTRUMENT OF TORTURE", null)
                                , UIImage.FromFile("freccia_giu.png"));
                return cell;
            }
            if (row == 2 && !SelSecond)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraTitolo cell = new CustomCellMostraTitolo(cellIdentifier, false, NSBundle.MainBundle.LocalizedString("Mostra_Titolo2", "ANCIENT INSTRUMENT OF TORTURE", null), TitleHeight2);
                cell.UpdateCell(NSBundle.MainBundle.LocalizedString("Mostra_Titolo2", "ANCIENT INSTRUMENT OF TORTURE", null)
                                , UIImage.FromFile("freccia_su.png"));
                return cell;
            }

            if (row == 4 && SelThird)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraTitolo cell = new CustomCellMostraTitolo(cellIdentifier, true, NSBundle.MainBundle.LocalizedString("Mostra_Titolo3", "A JOURNEY THROUGH HUMAN CRUELTY", null), TitleHeight3);
                cell.UpdateCell(NSBundle.MainBundle.LocalizedString("Mostra_Titolo3", "A JOURNEY THROUGH HUMAN CRUELTY", null)
                                , UIImage.FromFile("freccia_giu.png"));
                return cell;
            }
            if (row == 4 && !SelThird)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraTitolo cell = new CustomCellMostraTitolo(cellIdentifier, false, NSBundle.MainBundle.LocalizedString("Mostra_Titolo3", "A JOURNEY THROUGH HUMAN CRUELTY", null), TitleHeight3);
                cell.UpdateCell(NSBundle.MainBundle.LocalizedString("Mostra_Titolo3", "A JOURNEY THROUGH HUMAN CRUELTY", null)
                                , UIImage.FromFile("freccia_su.png"));
                return cell;
            }
            if (row == 6 && SelFourth)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraTitolo cell = new CustomCellMostraTitolo(cellIdentifier, true, NSBundle.MainBundle.LocalizedString("Mostra_Titolo4", "THE WORST SIDE OF MANKIND", null), TitleHeight4);
                cell.UpdateCell(NSBundle.MainBundle.LocalizedString("Mostra_Titolo4", "THE WORST SIDE OF MANKIND", null)
                                , UIImage.FromFile("freccia_giu.png"));
                return cell;
            }
            if (row == 6 && !SelFourth)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraTitolo cell = new CustomCellMostraTitolo(cellIdentifier, false, NSBundle.MainBundle.LocalizedString("Mostra_Titolo4", "THE WORST SIDE OF MANKIND", null), TitleHeight4);
                cell.UpdateCell(NSBundle.MainBundle.LocalizedString("Mostra_Titolo4", "THE WORST SIDE OF MANKIND", null)
                                , UIImage.FromFile("freccia_su.png"));
                return cell;
            }

            if (row == 1 && SelFirst)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraDescrizione cell = new CustomCellMostraDescrizione(cellIdentifier, (float)size1.Height);
                cell.UpdateCell(TEXT_1);
                return cell;
            }
            if (row == 1 && !SelFirst)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraDescrizione cell = new CustomCellMostraDescrizione(cellIdentifier, 0);
                return cell;
            }

            if (row == 3 && SelSecond)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraDescrizione cell = new CustomCellMostraDescrizione(cellIdentifier, (float)size2.Height);
                cell.UpdateCell(TEXT_2);
                return cell;
            }
            if (row == 3 && !SelSecond)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraDescrizione cell = new CustomCellMostraDescrizione(cellIdentifier, 0);
                return cell;
            }

            if (row == 5 && SelThird)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraDescrizione cell = new CustomCellMostraDescrizione(cellIdentifier, (float)size3.Height);
                cell.UpdateCell(TEXT_3);
                return cell;
            }
            if (row == 5 && !SelThird)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraDescrizione cell = new CustomCellMostraDescrizione(cellIdentifier, 0);
                return cell;
            }

            if (row == 7 && SelFourth)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraDescrizione cell = new CustomCellMostraDescrizione(cellIdentifier, (float)size4.Height);
                cell.UpdateCell(TEXT_4);
                return cell;
            }
            if (row == 7 && !SelFourth)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellMostraDescrizione cell = new CustomCellMostraDescrizione(cellIdentifier, 0);
                return cell;
            }

            return null;

        }
    }
}
